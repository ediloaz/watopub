import { StyleSheet , Dimensions } from 'react-native';

const HEIGHT = Dimensions.get('window').height

export const STYLES = StyleSheet.create({
    containerLogo : {
        position:'absolute',
        left:0,
        top:0,
        marginTop:'auto',
        marginBottom:'auto',
        marginLeft:'auto',
        marginRight:'auto',
        width:'100%',
        height:'100%',
    },
    textWatopub : {
        color:'white',
        fontSize:40,
        fontStyle:'italic',
        textAlign:'center',
        position:'absolute',
        left:0,
        top:HEIGHT-60,
        marginTop:'auto',
        marginBottom:'auto',
        width:'100%',
        height:'100%',
    },
})

