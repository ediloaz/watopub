// React
import React , { useEffect } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { Text, Alert } from 'react-native';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Logo from '../../components/image/logo'

// Firebase auth
import {
    CheckAccountIsLogged,
    GetDataOfCurrentAccount,
  } from '../../firebase/firestore/accounts'

// Utilities
import {
    ResetNavigation
} from '../../utils/utilities'

// Redux actions
import { LoginAccount } from '../../redux/actions/account'

// Styles
import { STYLES } from './styles'


function Screen(props){

    const { navigation } = props

    useEffect( _ => {
        
        const  NavegationToAuth = _ => ResetNavigation(navigation , 'Auth')
        const  NavegationToHomeUser = _ => ResetNavigation(navigation , 'HomeUser')
        const  NavegationToHomeBusiness = _ => ResetNavigation(navigation , 'HomeBusiness')

        function WriteInReduxAccountData(dataAccount){
            props.LoginAccount({
                dataAccount
            })
            dataAccount.accountType==='user'
            ?
            NavegationToHomeUser()
            :
            NavegationToHomeBusiness()

            // setTimeout( _ => {
            // },1000)
        }
        async function UserConnected(){
            console.log("inicio 2")
            GetDataOfCurrentAccount()
            .then(
                r => {
                    console.log("inicio 2")
                    WriteInReduxAccountData(r)
                }
                )
            .catch(
                e => {
                    console.log("inicio 3")
                    // Alert.alert(
                    //     "Se cerró su cuenta",
                    //     "Vuelva a introducir sus datos de usuario",
                    //     [
                    //         {
                    //             text:"Aceptar",
                    //             onPress:NavegationToAuth()
                    //         }
                    //     ]
                    // )
                }
            )
            
        }
        (async _ => {
            console.log("inicio 1")
            await CheckAccountIsLogged().then(r => {
                r
                ? UserConnected()
                : NavegationToAuth()
                // setAccount(r)
            })
        })()
    },[])

    return (
        <BackgroundScreen
        red
        >
            <Logo
            styleContainer={STYLES.containerLogo} 
            size={180}
            />
            <Text
            style={STYLES.textWatopub} 
            >
                Watopub
            </Text>
            
        </BackgroundScreen>
    )
}




const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
}
  
const mapDispatchToProps = { 
    LoginAccount
}
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Screen)
export default component