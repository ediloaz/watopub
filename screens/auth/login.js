// React
import React , { Fragment } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import FormLogin from '../../components/form/formLogin'
import Button from '../../components/button/button'
import CircleButton from '../../components/button/circle'
import Logo from '../../components/image/logo'

// Styles
import { STYLES } from './styles'

const IMAGE_GO_TO_LOGIN_BUSINESS = require('../../assets/icons/auth/goToLoginBusiness.png')
const IMAGE_GO_TO_BACK = require('../../assets/icons/auth/goToBack.png')

function Screen(props){

    const { typeAccount , navigation } = props

    const GoToRegisterUser = _ => {
        navigation.navigate("RegisterUser")
    }

    const GoToWelcome = _ => {
        navigation.navigate('Welcome');
    }

    function ButtonRegister(){
        return (
            <View
            style={STYLES.viewButtonRegister} 
            >
                <Button 
                text="¡Regístrate!"
                styleContainer={STYLES.containerButtonRegister} 
                styleButton={STYLES.buttonRegister} 
                styleText={STYLES.textButtonRegister} 
                onPress={GoToRegisterUser}
                />
            </View>
        )
    }
    

    function LogoTop(){
        return (
            <Logo
            styleContainer={STYLES.containerLogo} 
            size={140}
            />
        )
    }

    function TopImageAndButton(){
        return (
            <View
            style={STYLES.containerTopImageAndButton}
            >
                {/* {
                    typeAccount==='user' 
                    && <CircleButton 
                    source={IMAGE_GO_TO_LOGIN_BUSINESS}
                    size={45}
                    styleContainer={STYLES.containerCircleButtonGoToBusiness} 
                    onPress={GoToLoginBusiness}
                    />
                }
                {
                    typeAccount==='business' 
                    && <CircleButton 
                    source={IMAGE_GO_TO_BACK}
                    size={45}
                    styleContainer={STYLES.containerCircleButtonGoToBack} 
                    onPress={GoToBack}
                    />
                } */}
                <LogoTop />
            </View>
        )
    }

    return (
        <Fragment>
            <BackgroundScreen
            red
            scroll
            >
                <TopImageAndButton />

                <FormLogin
                styleContainer={STYLES.containerFormLogin} 
                accountType={typeAccount}
                // accountTypeMessage={typeAccount==='user' ? 'User Watopub' : typeAccount==='business' ? 'Business Watopub' : 'Tipo de cuenta'}
                accountTypeMessage="Iniciar sesión"
                navigation={props.navigation}
                GoToWelcome={GoToWelcome}
                />
                
                <ButtonRegister />
                
            </BackgroundScreen>
        </Fragment>
    )
}

export default Screen