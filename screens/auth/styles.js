import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import { COLORS } from '../../consts/colors'
import { STYLES_SHARED } from '../../consts/styles'

const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
    },
    textButtonLogin : {
        color:'#FFF',
    },
    viewButtonRegister: {
        // bottom:0,
        // position:'absolute',
        // marginBottom:0,
        // justifyContent:'flex-end',
        // top:Dimensions.get('window').height-400,
        // display:'flex',
        // height:10,
    },
    containerButtonRegister : {
        width:'70%',
        marginHorizontal:'15%',
        marginTop:150
        // bottom:30,
        // position:'absolute'
    },
    buttonRegister : {
        backgroundColor:'#FFF',
        marginTop:22,
    },
    textButtonRegister : {
        color:COLORS.blue.default,
    },
    containerTopImageAndButton : {
        width:'100%',
        // top:50,
        // position:'absolute',
    },
    containerCircleButtonGoToBusiness : { 
        alignSelf:'flex-end',
        marginRight:17,
    },
    containerCircleButtonGoToBack : { 
        alignSelf:'flex-start',
        marginLeft:17,
    },
    circleButton : {
        backgroundColor:'#FFF'
    },
    containerLogo : {
        alignSelf:'center',
        marginTop:80,
    },
    containerFormLogin : {
        marginTop:20,
        width:'80%',
        marginHorizontal:'10%',
    },
    // For use in pointsInformation screen
    textsGrouped : {
        marginHorizontal:20,
    },
    firstText : {
        textAlign:'center',
        fontWeight:"normal",
        marginTop:30,
        marginBottom:45,
        fontSize:FONT_SIZE_4,
    },
    secondText : {
        textAlign:'center',
        marginBottom:30,
        fontSize:FONT_SIZE_4,
    },
    thirdText : {
        marginBottom:30,
        fontSize:FONT_SIZE_5,
    },
    containerButtonNext : STYLES_SHARED.buttonBottomPosition,
    buttonNext : {
        backgroundColor:COLORS.yellow.default,
        marginTop:22,
        width:200
    },

    // For use in Header
    containerCircleButtonGoToBack : { 
        alignSelf:'flex-start',
        marginLeft:17,
    },
})

