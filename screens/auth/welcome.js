// React
import React , { useEffect } from 'react';

// React native
import { Platform, Share, Text, View, BackHandler,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import RocketForWelcome from '../../components/animations/rocketForWelcome'
import Header from '../../components/header/header'
import Button from '../../components/button/button'
import CircleButton from '../../components/button/circle'

// Utilities
import { 
    ShareApp,
    ResetNavigation,
 } from '../../utils/utilities';

// Styles
import { STYLES } from './styles'

const LOGO_GO_TO_SHARE = require('../../assets/icons/share.png')

function Screen(props){

    const { navigation } = props

    
    useEffect( _ => {
        const ControllBackButton = _ => { 
            return true 
        }
        // Mount
        BackHandler.addEventListener('hardwareBackPress',ControllBackButton())
        // UnMount
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',ControllBackButton())
        }
    },[])

    const GoToHome = _ => {
        NavegationToHomeUser()
    }

    const  NavegationToHomeUser = _ => ResetNavigation(navigation , 'HomeUser')

    function HeaderScreen(){
        return (
            <Header 
            title="Bienvenida"
            />
        )
    }

    function TextsGrouped(){
        return (
            <View  style={STYLES.textsGrouped} >
                <Text style={STYLES.firstText} >
                    <Text style={{fontWeight:'bold'}} >Comparte</Text> la aplicación para impulsar proyectos increíbles
                </Text>
                {/* <Text style={STYLES.secondText} >
                    Pronto podrás utilizar los Watopub Points
                </Text>
                <Text style={STYLES.thirdText} >
                    Comparte el enlace para invitar a tus amigos a descargar la aplicación para que puedas ganar los Watopub Points y tus amigos también.
                </Text> */}
            </View>
        )
    }

    function ButtonNext(){
        return (
            <Button 
            text="Siguiente"
            styleContainer={STYLES.containerButtonNext}
            styleButton={STYLES.buttonNext} 
            onPress={GoToHome}
            />
        )
    }

    function ButtonShare(){
        return (
            <CircleButton 
            text="Compartir"
            source={LOGO_GO_TO_SHARE}
            size={45}
            styleContainer={STYLES.containerCircleButton} 
            styleButton={STYLES.circleButton} 
            onPress={ShareApp}
            />
        )
    }



    return (
        <BackgroundScreen>
            <HeaderScreen />
            
            <RocketForWelcome 
            width={220}
            />
            
            <TextsGrouped />
            
            <ButtonShare />

            <ButtonNext />
            
        </BackgroundScreen>
    )
}

export default Screen