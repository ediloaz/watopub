// React
import React , { Fragment } from 'react';

// React native
import { Platform, StyleSheet, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import Register from './register'

// Styles
import { STYLES } from './styles'


function Screen(props){
    
    const { navigation } = props

    return (
        <Register 
        typeAccount='business'
        navigation={navigation}
        />
    )
}

export default Screen