// React
import React , { Fragment } from 'react';

// React native
import { Platform, StyleSheet, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import Login from './login'

// Styles
import { STYLES } from './styles'


function Screen(props){

    return (
        <Fragment>
            <Login 
            typeAccount='business'
            navigation={props.navigation}
            />
        </Fragment>
    )
}

export default Screen