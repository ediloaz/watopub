// React
import React , { Fragment, useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import FormRegister from '../../components/form/formRegister'
import CircleButton from '../../components/button/circle'
import Logo from '../../components/image/logo'

// Styles
import { STYLES } from './styles'


const IMAGE_GO_TO_BACK = require('../../assets/icons/auth/goToBack.png')

function Screen(props){

    const { typeAccount , navigation } = props

    const GoToBack = _ => {
        navigation.goBack();
    }
    
    const GoToWelcome = _ => {
        navigation.navigate('Welcome');
    }
    
    function LogoTop(){
        return (
            <Logo
            styleContainer={STYLES.containerLogo} 
            size={180}
            />
        )
    }

    function TopImageAndButton(){
        return (
            <View
            style={STYLES.containerTopImageAndButton}
            >
                <CircleButton 
                    source={IMAGE_GO_TO_BACK}
                    size={45}
                    styleContainer={STYLES.containerCircleButtonGoToBack} 
                    onPress={GoToBack}
                />
                <LogoTop />
        
            </View>
        )
    }

   

    return (
        <Fragment>
            <BackgroundScreen
            red
            scroll
            >
                <TopImageAndButton />

                <FormRegister
                styleContainer={STYLES.containerFormLogin} 
                typeAccount={typeAccount}
                GoToPointsInformation={GoToWelcome}
                navigation={navigation}
                />
                
            </BackgroundScreen>
        </Fragment>
    )
}

export default Screen