// React
import React , { Fragment } from 'react';

// React native
import { View } from 'react-native';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Title from '../../components/text/title'
import SquareButton from '../../components/button/square'
import Icon from '../../components/icon/circle'

// Firebase auth
import {
    SignOut,
  } from '../../firebase/firestore/accounts'

// Utilities
import { ResetNavigation } from '../../utils/utilities';

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'

const LOGO_CANCEL_SUBSCRIPTION = require('../../assets/icons/menu/cancelSubscription.png')
const LOGO_LOGOUT = require('../../assets/icons/menu/logout.png')
const LOGO_GO_TO_SHARE = require('../../assets/icons/menu/watopubPoints.png')
const LOGO_INFO = require('../../assets/icons/menu/info.png')
const LOGO_WATOPUB = require('../../assets/images/logos/background_transparent.png')



function Screen(props){

    const { navigation } = props

    const GoTo = screenName => {
        navigation.navigate(screenName);
    }

    function TitleScreen(){
        return (
            <Title 
            text="Menú"
            styleContainer={{width:100}}
            />
        )
    }

    function CloseAndSignOut(){
        // SignOut()
        // ResetNavigation(navigation,'AuthNavigation')
        console.log('La sesión ha sido cerrada')
      }

    function ButtonsGrouped(){
        
        return (
            <View  style={[STYLES.containerButtonsGrouped]} >
                <View  style={STYLES.row} >
                    <SquareButton
                    text="Términos y condiciones"
                    sizeIcon={SIZES.squareIconSizeForMenu}
                    source={LOGO_INFO}
                    styleContainer={STYLES.containerCircleButton} 
                    styleButton={STYLES.squareButton} 
                    onPress={_ => GoTo('ConditionsAndTerms')}
                    />
                    <SquareButton
                    text="Cerrar sesión"
                    sizeIcon={SIZES.squareIconSizeForMenu}
                    source={LOGO_LOGOUT}
                    styleContainer={STYLES.containerCircleButton} 
                    styleButton={STYLES.squareButton} 
                    onPress={CloseAndSignOut}
                    />
                </View>
                <View  style={STYLES.row} >
                    <SquareButton
                    text="Cancelar plan"
                    sizeIcon={SIZES.squareIconSizeForMenu}
                    source={LOGO_CANCEL_SUBSCRIPTION}
                    styleContainer={STYLES.containerCircleButton} 
                    styleButton={STYLES.squareButton} 
                    onPress={CloseAndSignOut}
                    />
                </View>
            </View>
        )
    }

    function Logo(){
        return (
            <Icon 
            text="Watopub"
            source={LOGO_WATOPUB}
            size={75}
            styleContainer={STYLES.containerLogo} 
            styleButton={STYLES.circleButton} 
            />
        )
    }



    return (
        <Fragment>
            <BackgroundScreen scroll existsHeader >
                <TitleScreen />
                
                <ButtonsGrouped />
                
                <Logo />

            </BackgroundScreen>
        </Fragment>
    )
}

export default Screen