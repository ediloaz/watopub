import { StyleSheet } from 'react-native';
import { COLORS } from '../../consts/colors'
import { SIZES } from '../../consts/sizes'
import { STYLES_SHARED } from '../../consts/styles'

const FONT_SIZE_3 = 16
const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12



export const STYLES = StyleSheet.create({
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
    },
    textButtonLogin : {
        color:'#FFF',
    },
    containerButtonRegister : {
        bottom:30,
        position:'absolute'
    },
    buttonRegister : {
        backgroundColor:'#FFF',
        marginTop:22,
    },
    textButtonRegister : {
        color:COLORS.blue.default,
    },
    containerTopImageAndButton : {
        width:'100%',
        top:50,
        position:'absolute',
    },
    containerCircleButtonGoToBusiness : { 
        alignSelf:'flex-end',
        marginRight:17,
    },
    containerCircleButtonGoToBack : { 
        alignSelf:'flex-start',
        marginLeft:17,
    },
    circleButton : {
        backgroundColor:'#F00'
    },
    squareButton : {
        backgroundColor:'#FFF',
        borderColor:'#DDD',
        borderStyle:'solid',
        borderWidth:1,
        width:SIZES.squareSizeForMenu,
        height:SIZES.squareSizeForMenu,
        paddingTop:30
        // marginHorizontal:SIZES.squareMarginForMenu,
    },
    containerLogo : {
        alignSelf:'center'
    },
    containerFormLogin : {
        marginTop:80,
    },
    // For use in pointsInformation screen
    textsGrouped : {
        marginHorizontal:20,
    },
    container : {
        width:'100%',
        flex: 1,
        flexDirection: 'column'
    },
    row:{
        marginVertical:0,
        width:'100%',
        flex: 1,
        flexDirection: 'row'
    },
    buttonsGrouped : {
        // marginHorizontal:20,
    },
    rowOfButtonsGrouped : {
        // marginHorizontal:20,
        
    },
    firstText : {
        textAlign:'center',
        fontWeight:"bold",
        marginTop:30,
        marginBottom:45,
        fontSize:FONT_SIZE_4,
    },
    secondText : {
        textAlign:'center',
        marginBottom:30,
        fontSize:FONT_SIZE_4,
    },
    thirdText : {
        marginBottom:30,
        fontSize:FONT_SIZE_5,
    },
    subtitleText : {
        marginBottom:10,
        fontSize:FONT_SIZE_4,
        textAlign:'left',
    },
    containerButtonNext : STYLES_SHARED.buttonBottomPosition,
    buttonNext : {
        backgroundColor:COLORS.yellow.default,
        marginTop:22,
    },
    containerLogo:{
        marginTop:250,
        // bottom:0,
        // marginTop:230
        // bottom:0,
        // marginBottom:0,
        // position:'absolute',
    },
    containerButtonsGrouped:{
        marginTop:20,
    },
})

