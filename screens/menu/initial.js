// React
import React , { Fragment, useState } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { View, TouchableOpacity, Text } from 'react-native';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Title from '../../components/text/title'
import SquareButton from '../../components/button/square'
import Icon from '../../components/icon/circle'
import Backdrop from '../../components/backdrop/loadingFullScreen'

// Firebase auth
import {
    SignOut,
  } from '../../firebase/firestore/accounts'

// Utilities
import { ResetNavigation } from '../../utils/utilities';

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'

const LOGO_CANCEL_SUBSCRIPTION = require('../../assets/icons/menu/cancelSubscription.png')
const LOGO_LOGOUT = require('../../assets/icons/menu/logout.png')
const LOGO_GO_TO_SHARE = require('../../assets/icons/menu/watopubPoints.png')
const LOGO_INFO = require('../../assets/icons/menu/info.png')
const LOGO_WATOPUB = require('../../assets/images/logos/background_transparent.png')

function Screen(props){

    const { navigation } = props

    const [active,setActive] = useState(false)

    const GoTo = screenName => {
        navigation.navigate(screenName);
    }

    function TitleScreen(){
        return (
            <Title 
            text="Menú"
            styleContainer={{width:200}}
            />
        )
    }
    
    function CloseAndSignOut(){
        console.log('La sesión quiere cerrar')
        setActive(true)
        SignOut().then( r => {
            setTimeout( _ => {
                setActive(false) 
                ResetNavigation(navigation,'Auth')
                // navigation.navigate('Auth')
            }, 3000);
        }).catch( e => {
             setActive(false)
             alert(`Error ${e}`)
        })
        
        console.log('La sesión ha sido cerrada')
      }

    function ButtonsGrouped(){
       function ButtonWatopubPoints(){
           return (
                <SquareButton
                text="Watopub Points"
                sizeIcon={SIZES.squareIconSizeForMenu}
                source={LOGO_GO_TO_SHARE}
                styleContainer={STYLES.containerCircleButton} 
                styleButton={STYLES.squareButton} 
                onPress={_ => GoTo('WatopubPoints')}
                />
           )
       } 
       function ButtonTermsAndConditions(){
           return (
                <SquareButton
                text="Términos y condiciones"
                sizeIcon={SIZES.squareIconSizeForMenu}
                source={LOGO_INFO}
                styleContainer={STYLES.containerCircleButton} 
                styleButton={STYLES.squareButton} 
                onPress={_ => GoTo('ConditionsAndTerms')}
                />
           )
       } 
       function ButtonLogout(){
           return (
                <SquareButton
                text="Cerrar sesión"
                sizeIcon={SIZES.squareIconSizeForMenu}
                source={LOGO_LOGOUT}
                styleContainer={STYLES.containerCircleButton} 
                styleButton={STYLES.squareButton} 
                onPress={CloseAndSignOut}
                />
           )
       } 
       function ButtonCancelSubscription(){
           return (
                <SquareButton
                text="Cancelar plan"
                sizeIcon={SIZES.squareIconSizeForMenu}
                source={LOGO_CANCEL_SUBSCRIPTION}
                styleContainer={STYLES.containerCircleButton} 
                styleButton={STYLES.squareButton} 
                onPress={_ => alert("Cancelaremos el plan")}
                />
           )
       } 
        return (
            <View  style={[STYLES.containerButtonsGrouped]} >
                <View  style={STYLES.row} >
                    
                    {
                        props.account.accountType==='business' ? <ButtonTermsAndConditions /> :  <ButtonTermsAndConditions /> 
                    }
                    {
                        props.account.accountType==='business' ? <ButtonLogout /> : <ButtonLogout /> 
                    }
                </View>
                {/* <View  style={STYLES.row} >
                    {
                        props.account.accountType==='business' ?  <ButtonCancelSubscription /> 
                    }
                    

                </View> */}
            </View>
        )
    }

    function Logo(){
        return (
            <Icon 
            text="Watopub"
            source={LOGO_WATOPUB}
            size={75}
            styleContainer={STYLES.containerLogo} 
            styleButton={STYLES.circleButton} 
            />
        )
    }

    return (
        <Fragment>
            <BackgroundScreen scroll existsHeader >
                <Backdrop 
                show={active}
                animationLoading
                text="Cerrando sesión"
                />
                <TitleScreen />
                
                <ButtonsGrouped />
                
                <Logo />

            </BackgroundScreen>
        </Fragment>
    )
}

const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
}
  
const mapDispatchToProps = { }
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Screen)
export default component