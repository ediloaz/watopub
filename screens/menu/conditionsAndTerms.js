// React
import React , { Fragment } from 'react';

// React native
import { Platform, Share, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import WatopubPointsImage from '../../components/image/watopubPoints'
import Header from '../../components/header/header'
import Button from '../../components/button/button'
import CircleButton from '../../components/button/circle'

// Utilities
import { ShareApp } from '../../utils/utilities';

// Styles
import { STYLES } from './styles'


const LOGO_GO_TO_SHARE = require('../../assets/icons/share.png')
// const TEXTS_TO_SHOW = []

function Screen(props){

    const { navigation } = props

    const GoToHome = _ => {
        navigation.navigate("Home");
    }

    function TitleScreen(){
        return (
            <Header
            title='Términos y condiciones'
            backAvalaible
            navigation={navigation}
            />
        )
    }

    function TextsGrouped(){
        return (
            <View  style={STYLES.textsGrouped} >
                <Text style={STYLES.subtitleText} >
                    Bienvenida
                </Text>
                <Text style={STYLES.thirdText} >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae turpis auctor, commodo odio ut, tincidunt massa. Donec eu orci vitae justo commodo euismod. Donec hendrerit libero a dolor tristique tincidunt. Fusce accumsan nec metus nec efficitur. In at lobortis sapien. Etiam rutrum urna vitae sapien vulputate facilisis. Nunc finibus ornare nulla, et consectetur nulla iaculis vel.
                    {"\n\n"}
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis elit nisl. Sed varius id odio id posuere. Aliquam efficitur purus ac mauris molestie cursus. Praesent volutpat, neque id ornare ornare, arcu odio tincidunt urna, ac varius sem neque eget ipsum. Nullam eget libero a tortor sagittis elementum sed a enim. Nullam malesuada facilisis dolor, ut fermentum ante commodo vitae. Maecenas gravida, purus vel auctor tempus, enim ex egestas neque, at eleifend diam tellus vel orci. Vestibulum a turpis ac ligula consequat bibendum in id arcu. Cras a nunc neque. Duis hendrerit, risus id vestibulum tristique, purus elit lobortis tortor, ut pulvinar sem neque sit amet nibh. Pellentesque pharetra tristique ligula, a ultricies tortor pellentesque eget. Integer posuere feugiat turpis non mattis. Donec luctus arcu eu ultrices pretium.
                    {"\n\n"}
                    Etiam orci sem, finibus quis tincidunt ac, varius ullamcorper nisl. Suspendisse dui arcu, efficitur nec tristique laoreet, dapibus et arcu. Sed gravida est risus, vitae pellentesque lectus auctor id. Sed gravida vel leo sed finibus. Nullam eu urna tincidunt, volutpat dolor ac, malesuada orci. Donec at lectus enim. Ut hendrerit, leo faucibus bibendum porttitor, odio libero sagittis tellus, ac porta lacus elit eget sapien. Praesent quis metus lobortis, finibus nibh eu, egestas elit. Quisque porttitor vestibulum erat vel accumsan. Aenean luctus, orci at fermentum congue, erat urna iaculis ex, nec commodo odio augue ac magna. Donec sit amet pellentesque felis. Vestibulum id consectetur magna. Sed non nunc at metus laoreet bibendum. Vestibulum condimentum turpis nulla, id tempor tellus porttitor quis.
                </Text>

                <Text style={STYLES.subtitleText} >
                    Los servicios que ofrecemos
                </Text>
                <Text style={STYLES.thirdText} >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae turpis auctor, commodo odio ut, tincidunt massa. Donec eu orci vitae justo commodo euismod. Donec hendrerit libero a dolor tristique tincidunt. Fusce accumsan nec metus nec efficitur. In at lobortis sapien. Etiam rutrum urna vitae sapien vulputate facilisis. Nunc finibus ornare nulla, et consectetur nulla iaculis vel.
                    {"\n\n"}
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis elit nisl. Sed varius id odio id posuere. Aliquam efficitur purus ac mauris molestie cursus. Praesent volutpat, neque id ornare ornare, arcu odio tincidunt urna, ac varius sem neque eget ipsum. Nullam eget libero a tortor sagittis elementum sed a enim. Nullam malesuada facilisis dolor, ut fermentum ante commodo vitae. Maecenas gravida, purus vel auctor tempus, enim ex egestas neque, at eleifend diam tellus vel orci. Vestibulum a turpis ac ligula consequat bibendum in id arcu. Cras a nunc neque. Duis hendrerit, risus id vestibulum tristique, purus elit lobortis tortor, ut pulvinar sem neque sit amet nibh. Pellentesque pharetra tristique ligula, a ultricies tortor pellentesque eget. Integer posuere feugiat turpis non mattis. Donec luctus arcu eu ultrices pretium.
                </Text>
            </View>
        )
    }



    return (
        <BackgroundScreen
        scroll
        >
            <TitleScreen />
        
            <TextsGrouped />
            
            
        </BackgroundScreen>
    )
}

export default Screen