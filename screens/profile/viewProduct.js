// React
import React , { useState , useEffect } from 'react';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Product from '../../components/product/product'

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'

const IMAGE_RELOAD = require('../../assets/icons/reload.png')

function Screen(props){
    
    // data of product
    const {
        own,
        businessName,
        category,
        controls,
        date,
        description,
        id,
        keyPost,
        media,
        mediaType,
        title,
    } = props.navigation.state.params.data

    // Just navigation, separated by best code order experience
    const { navigation } = props
    
    function ProductView(){
        return (
            <Product
            own={own}
            keyPost={keyPost}
            businessName={businessName}
            title={title}
            date={date}
            media={media}
            controls={controls}
            mediaType={mediaType}
            description={description}
            navigation={navigation}
            />
        )
    }

    return (
        <BackgroundScreen scroll>
            
            <ProductView />
            
        </BackgroundScreen>
    )
}

export default Screen