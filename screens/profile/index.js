// React
import React , { useState, useEffect } from 'react';

// React native
import { View } from 'react-native';

// Redux
import { connect } from 'react-redux';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Products from '../../components/container/profile/products'
import SpecialHeader from '../../components/container/profile/specialHeader'
import Header from '../../components/header/header'
import PostsByCategory from '../home/postsByCategory'

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'
import { GetBusinessData } from '../../firebase/firestore'

// Styles
import { STYLES } from './styles'

function Screen(props){

    const { navigation } = props

    const [productsQuantity,setProductsQuantity] = useState(null)
    const [onlyView,setOnlyView] = useState(false)
    const [data,setData] = useState(null)

    useEffect( _ => {
        const DefineBusinessData = key => {
            alert(key)
            GetBusinessData(key)
            .then(
                r => {
                    setData(r)
                }
            )
        }
        const KEY = props.navigation.state.params ? props.navigation.state.params.businessEmail : props.account.email
        DefineBusinessData(KEY)
    },[])
    
    useEffect( _ => {
        if (props.navigation.state.params){
            if (props.navigation.state.params.onlyView){
                setOnlyView(true)
            }
        }
    },[props.navigation.state.params])

    const GoToAddPost = _ => {
        navigation.navigate("AddProduct");
    }
    


    function ProductsContainer(){
        return (
            <View
            style={{paddingTop:50}}
            >
                <PostsByCategory
                navigation={{
                    state : {
                        params : { 
                            category : data ? data.name : null,
                            embed:true
                        }
                    }}
                }
                />
            </View>
            // <Products
            // own={!onlyView}
            // // businessEmail ={data ? data.email : null}
            // businessEmail ={data ? data.name : null}
            // navigation={navigation}
            // setProductsQuantity={setProductsQuantity}
            // />
        )
    }

    return (
        <BackgroundScreen scroll>
            
            <Header
            own={!onlyView}
            profilePicture={data ? data.media : null}
            name={data ? data.name : null}
            forProfileBusiness
            GoToAddPost={GoToAddPost} 
            styleContainer={STYLES.headerIndexProfile}
            navigation={navigation}
            /> 

            <SpecialHeader 
            own={!onlyView}
            information={data ? data.information : null}
            productsQuantity={productsQuantity}
            navigation={navigation}
            />
            
            <ProductsContainer />
            
        </BackgroundScreen>
    )
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
    }
}
  
const mapDispatchToProps = { }
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Screen)
export default component
