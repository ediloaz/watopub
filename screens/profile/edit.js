// React
import React , { } from 'react';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Header from '../../components/header/header'
import EditProfileForm from '../../components/form/editProfile'

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'

function Screen(props){
    
    const { navigation } = props
    
    return (
        <BackgroundScreen scroll>
            <Header 
            title='Editar perfil'  
            backAvalaible
            /> 

            <EditProfileForm
            navigation={navigation}
            /> 
            
        </BackgroundScreen>
    )
}

export default Screen