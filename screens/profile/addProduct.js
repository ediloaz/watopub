// React
import React , { } from 'react';

// Redux
import { connect } from 'react-redux';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import AddProductForm from '../../components/form/addProduct'
import Header from '../../components/header/header'


function Screen(props){

    const { navigation } = props

    const businessName = props.account.name

    return (
        <BackgroundScreen scroll>
            
            <Header
            title={businessName || 'Tu negocio'}
            backAvalaible
            navigation={navigation}
            />

            <AddProductForm
            navigation={navigation}
            /> 
            
        </BackgroundScreen>
    )
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
    }
}
  
const mapDispatchToProps = { }
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Screen)
export default component