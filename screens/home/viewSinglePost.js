// React
import React , { useState, useEffect } from 'react';

// React native
import { Platform, Share, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'



// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Post from '../../components/post/post'
import ContainerButtons from '../../components/container/useInPostsNav/buttonsOfInteractivity'
import Header from '../../components/header/header'
import Title from '../../components/text/title'

// Utilities
// import { ShareApp } from '../../utils/utilities';

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'
import { COLORS } from '../../consts/colors';

const IMAGE_RELOAD = require('../../assets/icons/reload.png')

function Screen(props){

    const { navigation } = props

    const [dataPost, setDataPost] = useState({
        keyPost:null,
        description:null,
        category:null,
        controls:null,
        date:null,
        description:null,
        media:null,
        mediatype:null,
    })
    
    useEffect( _ => {
        const params = props.navigation.state.params
        setDataPost(params.dataPost)
    },[props.navigation.state.params])

    function TextBottom(){
        return (
            <Title
            text="Abre una de las tarjetas"
            styleContainer={STYLES .textBottomContainer }
            styleText={STYLES .textBottomText }
            />
        )
    }

    return (
        <BackgroundScreen scroll>
            
            <Header
            title={dataPost.businessName || 'Post individual'}
            backAvalaible
            navigation={navigation}
            />
            <Post
            own={false}
            keyPost={dataPost.keyPost || ''}
            description={dataPost.description || 'Sin descripcion'}
            mediaType = {dataPost.mediaType || null}
            media = {dataPost.media || null}
            date = {dataPost.date  || 'Sin fecha'}
            imageAccount = {null}
            nameAccount = "Nombre empresa"
            controls = {dataPost.controls || 0}
            navigation = { navigation }
            />
            
            
            
        </BackgroundScreen>
    )
}

export default Screen