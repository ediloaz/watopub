// React
import React , {  } from 'react';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import ContainerButtons from '../../components/container/useInPostsNav/buttonsBusinessCategoriesInPosts'
import Header from '../../components/header/header'

// Utilities
// import { ShareApp } from '../../utils/utilities';

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'



function Screen(props){

    const { navigation } = props

    const GoToInteractivyBoxes = _ => {
        navigation.navigate("InteractivityBoxes");
    }

    const GoToAddPost = _ => {
        navigation.navigate("AddPost");
    }

    return (
        <BackgroundScreen 
        scroll
        >
            <Header 
            forHome
            GoToInteractivyBoxes={GoToInteractivyBoxes}
            GoToAddPost={GoToAddPost}
            />
            
            <ContainerButtons
            // GoToPostsByCategory={GoToPostsByCategory}
            navigation={navigation}
            /> 
            
        </BackgroundScreen>
    )
}

export default Screen