// React
import React , { useState, useEffect } from 'react';

// React native
import { Platform, Share, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'



// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Post from '../../components/post/post'
import Posts from '../../components/container/useInPostsNav/posts'
import Header from '../../components/header/header'

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'

const IMAGE_RELOAD = require('../../assets/icons/reload.png')

function Screen(props){

    const { navigation } = props

    const [category, setCategory] = useState("Categoría")
    const [embed, setEmbed] = useState(false)
    

    useEffect( _ => {
        const params = props.navigation.state.params
        setCategory(params.category)
        setEmbed(params.embed || false)
    },[props.navigation.state.params])



    return (
        <BackgroundScreen scroll>
            
            {!embed
            &&
            <Header
            title={category}
            backAvalaible
            navigation={navigation}
            />
            }

            <Posts
            own={false}
            navigation={navigation}
            category={category} 
            isForUserProfile={embed}
            /> 
            
        </BackgroundScreen>
    )
}

export default Screen