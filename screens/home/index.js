// React
import React , { Fragment } from 'react';

// React native
import { Platform, Share, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import WatopubPointsImage from '../../components/image/watopubPoints'
import Title from '../../components/text/title'
import Button from '../../components/button/button'
import CircleButton from '../../components/button/circle'

// Utilities
import { ShareApp } from '../../utils/utilities';

// Styles
import { STYLES } from './styles'

const LOGO_GO_TO_SHARE = require('../../assets/icons/share.png')

function Screen(props){

    const { navigation } = props

    function TitleScreen(){
        return (
            <Fragment>
                <Title 
                text="Watopub Points"
                />
            </Fragment>
        )
    }

    function TextsGrouped(){
        return (
            <View  style={STYLES.textsGrouped} >
                <Text style={STYLES.firstText} >
                    ¡Felicidades has ganado 500 Watopub points!
                </Text>
                <Text style={STYLES.secondText} >
                    Pronto podrás utilizar los Watopub Points
                </Text>
                <Text style={STYLES.thirdText} >
                    Comparte el enlace para invitar a tus amigos a descargar la aplicación para que puedas ganar los Watopub Points y tus amigos también.
                </Text>
            </View>
        )
    }

    function ButtonNext(){
        return (
            <Fragment>
                <Button 
                text="Siguiente"
                styleContainer={STYLES.containerButtonNext}
                styleButton={STYLES.buttonNext} 
                />
            </Fragment>
        )
    }

    function ButtonShare(){
        return (
            <Fragment>
                <CircleButton 
                text="Compartir"
                source={LOGO_GO_TO_SHARE}
                size={45}
                styleContainer={STYLES.containerCircleButton} 
                styleButton={STYLES.circleButton} 
                onPress={ShareApp}
                />
            </Fragment>
        )
    }



    return (
        <Fragment>
            <BackgroundScreen>
                <TitleScreen />
                
                <WatopubPointsImage 
                width={220}
                />
                
                
                <Text>Hooola</Text>
                
                
                
            </BackgroundScreen>
        </Fragment>
    )
}

export default Screen