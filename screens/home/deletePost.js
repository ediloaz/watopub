// React
import React , { useState, useEffect } from 'react';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import DeleteForm from '../../components/form/delete'
import Header from '../../components/header/header'

function Screen(props){

    const { navigation } = props

    const [type, setType] = useState(null)
    const [keyPost, setKeyPost] = useState(null)
    // const [navigation,setNavigation] = useState(null)

    useEffect( _ => {
        console.log(`2 : ${props.navigation.state.params.keyPost}`)
        setType(props.navigation.state.params.type)
        setKeyPost(props.navigation.state.params.keyPost)
    },[props.navigation.state.params])    

    return (
        <BackgroundScreen>

            <Header 
            title="Borrar"
            navigation = { navigation }
            />

            <DeleteForm
            keyPost={keyPost}
            type={type}
            styleContainer={{marginTop:100}}
            navigation={navigation}
            />

        </BackgroundScreen>
    )
}

export default Screen