// React
import React , { useState, useEffect } from 'react';

// React native
import { Platform, Share, Text, View,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'



// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Post from '../../components/post/post'
import ContainerButtons from '../../components/container/useInPostsNav/buttonsOfInteractivity'
import Header from '../../components/header/header'
import Title from '../../components/text/title'

// Utilities
// import { ShareApp } from '../../utils/utilities';

// Firestore
import { GetPostsOfMyBusiness as _GetPosts } from '../../firebase/firestore/posts'

// Consts
import { SIZES } from '../../consts/sizes'

// Styles
import { STYLES } from './styles'
import { COLORS } from '../../consts/colors';

const IMAGE_RELOAD = require('../../assets/icons/reload.png')

function Screen(props){

    const { navigation } = props

    function TextBottom(){
        return (
            <Title
            text="Abre una de las tarjetas"
            styleContainer={STYLES .textBottomContainer }
            styleText={STYLES .textBottomText }
            />
        )
    }

    return (
        <BackgroundScreen scroll>
            
            <Header
            title='Interactividad'
            backAvalaible
            navigation={navigation}
            />
            
            <ContainerButtons 
            navigation={navigation}
            /> 
            
            <TextBottom />
            
        </BackgroundScreen>
    )
}

export default Screen