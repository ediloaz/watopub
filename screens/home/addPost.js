// React
import React , { } from 'react';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import AddPostForm from '../../components/form/addPost'
import Header from '../../components/header/header'
import Separator from '../../components/separator/forMenuBottom'

function Screen(props){

    const { navigation } = props

    return (
        <BackgroundScreen scroll>
            
            <Header
            title='Publicación'
            backAvalaible
            navigation={navigation}
            />

            <AddPostForm 
            navigation={navigation}
            /> 

            <Separator />
            
        </BackgroundScreen>
    )
}

export default Screen