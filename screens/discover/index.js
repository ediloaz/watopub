// React
import React , { useEffect } from 'react';

// React native
import { Text } from 'react-native';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Posts from '../../components/container/useInPostsNav/posts'
import Header from '../../components/header/header'
import { View } from 'react-native';

// Redux
import { connect } from 'react-redux';

function Screen(props){

    const { navigation } = props

    const GoToAddPost = _ => {
        navigation.navigate("AddPost");
    }

    
    return (
        <BackgroundScreen 
        scroll
        // onReachEnd={ReachedEnd}
        >
            <Header 
            forHomeBusiness 
            GoToAddPost={GoToAddPost} 
            /> 

            <Posts 
            own={true}
            navigation={navigation} 
            // reachEnd={reachEnd}
            /> 
            
        </BackgroundScreen>
    )
}

const mapStateToProps = (state) => {
return {
    account: state.account,
}
}

const mapDispatchToProps = { 
// LoginAccount
}


const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Screen)
export default component

// export default Screen