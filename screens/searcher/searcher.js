// React
import React , { useState, useEffect } from 'react';

// React native
import { FlatList, Text, View, } from 'react-native';


// Progress
import { Bar  } from 'react-native-progress';

// Watopub components
import BackgroundScreen from '../../components/backgroundScreen/standard'
import Footer from '../../components/footers/lists'
import Header from '../../components/header/header'
import Cover from '../../components/image/imageForSearcherCover'
import Lupa from '../../components/image/imageForSearcherLupa'
import Post from '../../components/post/resultFromSearch'

// Firestore
import { 
    SearchByBusinessName
 } from '../../firebase/firestore/posts'

// Utilities
import {
    FirebaseDateToLocalDateHumanReadable
} from '../../utils/utilities'

// Styles
import { STYLES } from './styles'



function Screen(props){

    const { navigation } = props

    const [stringToSearch, setStringToSearch] = useState("")
    const [loading, setLoading] = useState(false)
    const [existsPosts,setExistsPosts] = useState(true)
    const [posts, setPosts] = useState([])

    
    const SearchTheString = value => {
        setPosts([])
        setExistsPosts(true)
        let cont = 0
        SearchByBusinessName(value)
            .then(
                r => {
                r.get()
                .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        const POST = doc.data()
                        POST['id'] = cont
                        POST['keyPost'] = doc.id.toString()
                        POST['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
                        setPosts(posts => [...posts , POST]) 
                        cont ++
                        console.log(cont)
                        console.log(POST)
                    })
                    cont===0 && setExistsPosts(false)
                    setTimeout(() => {
                        setLoading(false)
                    }, 2000); 
                }
            )
            console.log(`Totales: ${cont}`)
        })
        .catch(
            e => {
                console.log(e)
                console.log('Error trayendo las publicaciones')
                setLoading(false)
                setExistsPosts(false)
                alert("No hubo conexión")
            }
        )
    }

    const Search = async value => {
        console.log(`Buscando: ${value}`)
        SearchTheString(value)
        setStringToSearch(value)
    }
    
    function HeaderAndInput(){
        return (
            <Header
            forSearcher
            stringToSearch={stringToSearch}
            Search={Search}
            />
        )
    }

    function TextResults(){
        return (
            <Text
            style={STYLES.textResults}
            >
                {`Resultados (${posts.length})`}
            </Text>
        )
    }

    
    function Loading(){
        return (
            <CircleAnimation 
            text={props.text || category ? `Cargando ${category}` : `Cargando tus publicaciones` }
            styleText={{color:'black'}}
            />
        )
    }


    function Initial(){
        return (
            <View
            style={{ alignItems:'center' , flex:0 }}
            >
                <Cover />

                <Lupa />

                {loading && <Bar size={30} indeterminate={true} />}


                <Text>
                    En Watopub encuentra lo que quieras
                </Text>

            </View>
        )
    }

    function Posts(){
        
        function PostsForFlatlist({item}){
            return (
                <Post
                own={props.own}
                keyPost={item.keyPost}
                description={item.description || 'Sin descripcion'}
                mediaType = {item.mediaType}
                media = {item.media}
                date = {item.date  || 'Sin fecha'}
                businessPicture = {null}
                businessEmail = {item.businessEmail || "Email de empresa"}
                businessName = {item.businessName || "Nombre empresa"}
                controls = {item.controls}
                navigation = { navigation }
                />
                // <Text> {item.description} </Text>
            )
        }

        function FlatListWithPosts(){
            return (
                <FlatList 
                style={STYLES.flatListOfResults}
                ListFooterComponent={<Footer origin='search' />}
                data={posts}
                renderItem={PostsForFlatlist}
                keyExtractor = { item => item.id.toString() }
                />
            )
        }
        return (
            <View>
                <TextResults />
                <FlatListWithPosts />
            </View>
        )
    }
    
    function Empty(){
        return (
            <View>
                <TextResults />
                <Text>No hay coincidencias </Text>

            </View>
        )
    }
    

    return (
        <BackgroundScreen scroll>
            
            <HeaderAndInput /> 
            
            {
                stringToSearch.length === 0
                ?
                <Initial />
                :
                posts.length > 0
                ?
                <Posts />
                :
                <Empty />
            }
            
        </BackgroundScreen>
    )
}

export default Screen