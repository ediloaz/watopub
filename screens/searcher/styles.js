import { StyleSheet } from 'react-native';
import { COLORS } from '../../consts/colors'
import { STYLES_SHARED } from '../../consts/styles'

const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    container : {
        width:'100%',
        flex: 1,
        flexDirection: 'column'
    },
    rowForTitle:{
        marginVertical:0,
        // width:'100%',
        flex: 1,
        flexDirection: 'row',
        
    },
    titleInInitialPosts:{
        alignContent:'flex-start'
    },
    containerSquareButtonInTitleInInitialPosts : {
        alignContent:'flex-end',
    },
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
    },
    textButtonLogin : {
        color:'#FFF',
    },
    containerButtonRegister : {
        bottom:30,
        position:'absolute'
    },
    buttonRegister : {
        backgroundColor:'#FFF',
        marginTop:22,
    },
    textButtonRegister : {
        color:COLORS.blue.default,
    },
    containerTopImageAndButton : {
        width:'100%',
        top:50,
        position:'absolute',
    },
    containerCircleButtonGoToBusiness : { 
        alignSelf:'flex-end',
        marginRight:17,
    },
    containerCircleButtonGoToBack : { 
        alignSelf:'flex-start',
        marginLeft:17,
    },
    circleButton : {
        backgroundColor:'#F00'
    },
    containerLogo : {
        alignSelf:'center'
    },
    containerFormLogin : {
        marginTop:80,
    },
    // For use in pointsInformation screen
    textsGrouped : {
        marginHorizontal:20,
    },
    firstText : {
        textAlign:'center',
        fontWeight:"bold",
        marginTop:30,
        marginBottom:45,
        fontSize:FONT_SIZE_4,
    },
    secondText : {
        textAlign:'center',
        marginBottom:30,
        fontSize:FONT_SIZE_4,
    },
    thirdText : {
        marginBottom:30,
        fontSize:FONT_SIZE_5,
    },
    containerButtonNext : STYLES_SHARED.buttonBottomPosition,
    buttonNext : {
        backgroundColor:COLORS.yellow.default,
        marginTop:22,
    },

    // Interactivity Boxes
    textBottomContainer : {
        backgroundColor : COLORS.yellow.default , 
        width:'60%' ,
    },
    textBottomText : {
        color : 'black' , 
        fontSize : 12
    },

    // Results
    flatListOfResults:{
        marginTop:20
    },
    textResults:{
        marginHorizontal:20,
        paddingBottom:10,
        marginBottom:0,
        borderBottomColor:'#EEE',
        borderBottomWidth:1,
    },

})

