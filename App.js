// React
import React , { useState, Fragment, useEffect } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

// Redux
import { connect } from 'react-redux';

// Actions
import { LoginAccount } from './redux/actions/account'

// Navigation
// import AuthNavigation from './navigation/auth'
// import HomeNavigation from './navigation/home'
import AppNavigation from './navigation/app'



// const instructions = Platform.select({
//   ios: `Press Cmd+R to reload,\nCmd+D or shake for dev menu`,
//   android: `Double tap R on your keyboard to reload,\nShake or press menu button for dev menu`,
// });
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('') <= -1) {
    _console.warn(message);
  }
};
  


function App(props) {

  // const [account,setAccount] = useState(props.account.login)

  // const { navigation } = props

  
  
  // function AccountLogged(){
  //   return (
  //     <Fragment>
  //       <AppNavigation />
  //     </Fragment>
  //   )
  // }

  // function AccountInlogged(){
  //   return (
  //     <Fragment>
  //       <AuthNavigation />
  //     </Fragment>
  //   )
  // }

  return (
    <AppNavigation />
        // account===false
        // ? <AccountInlogged />
        // : <AccountLogged />
  )

}




const mapStateToProps = (state) => {
  return {
    account: state.account,
  }
}

const mapDispatchToProps = { 
  LoginAccount
}

const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(App)
export default component