// Redux
import { createStore } from 'redux';
// import devToolsEnhancer  from 'remote-redux-devtools'

// Reducers
import reducer from './reducers';

// const store = _ => createStore( 
const store =  createStore( 
    reducer , 
    // devToolsEnhancer()
)

export default store