// Types
import {
    LOGIN_ACCOUNT,
    LOGOUT_ACCOUNT,
} from './types'


export const LoginAccount = payload => {
    return {
        type :  LOGIN_ACCOUNT,
        payload
    }
}
    


export const LogoutAccount = payload => (
    {
        type :  LOGOUT_ACCOUNT,
        payload
    }
)