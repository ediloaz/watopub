// Types
import {
    LOGIN_ACCOUNT,
    LOGOUT_ACCOUNT,
} from '../actions/types'

const INITIAL_STATE = { 
    login : null,
    accountType: null,
    name : null,
    lastname : null,
    businessCategory: null,
    email: null,
    password: null,
    registrationDate:null,
    media:null,
    information:null,
    lastUpdateDate:null,
}

const reducer = (state = INITIAL_STATE , {type, payload}) => {
    var stateToReturn = Object.assign({} , state)
    
    switch (type) {
        case LOGIN_ACCOUNT:
            stateToReturn = payload.dataAccount
            stateToReturn.login = true
            break;
            
        case LOGOUT_ACCOUNT:
            stateToReturn.login = payload.login || true
            stateToReturn.name = payload.name || null
            stateToReturn.lastname = payload.lastname || null
            stateToReturn.email = payload.email || null
            stateToReturn = stateToReturn
            break;
        
        default:
            break;
    }
    window.state.account = stateToReturn
    return stateToReturn
}

export default reducer