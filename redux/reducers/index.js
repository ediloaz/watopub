import { combineReducers } from 'redux' 

// Reducers
import account from './account'

// ONLY DEVELOPING ENVIRONMENT
window.state = {}
window.state.account = {}


const REDUCERS = {
    account
}

const reducer = combineReducers(REDUCERS)

export default reducer