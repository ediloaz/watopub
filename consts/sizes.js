import {  Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const PER_FILE = 2;
const BOX_WIDTH_COMPLETE = (width / PER_FILE);
const BOX_WIDTH = (width / PER_FILE)*0.98;
const BOX_MARGIN_HORIZONTAL = (width / PER_FILE)*0.01;

const ASPECT = {
    w:120,
    h:100,
}
const ASPECT_PROFILE_PICTURE = {
    w:1,
    h:1,
}

const CalculateBoxWidth = (perFile,porcentajeOfSize=0.98) => {
    return (width / perFile)*porcentajeOfSize;
}

export const SIZES = {
    /**
     * Use in square buttons in Menu Screen
     */
    squareSizeForMenu : BOX_WIDTH_COMPLETE,
    /**
     * Use in square buttons in Menu Screen
     */
    squareMarginForMenu : BOX_MARGIN_HORIZONTAL,
    /**
     * Use in square buttons in Menu Screen
     */
    squareIconSizeForMenu : BOX_WIDTH*0.52,
    /**
     * Use in square buttons in Title Posts
     */
    squareIconSizeForTitlePosts : BOX_WIDTH*0.2,
    /**
     * Use in container of buttons in Posts (initial) Screen 
     * Must be three buttons by row
     */
    squareIconSizeForScreenPosts : CalculateBoxWidth(3 , 0.8),
    /**
     * Use in container of buttons in Posts (initial) Screen 
     * Must be three buttons by row
     */
    squareIconSizeForInteractivityBoxes : CalculateBoxWidth(2 , 0.52),
    
    /**
     * Height of a post
     */
    post : {
        image: {
            height : 250
        },
        date: {
            height : 22
        },
        image: {
            height : 250
        },
        aspect: ASPECT,
    },
    mediaForView : {
        width : width * 1 ,
        marginHorizontal : width * 0 ,
        height : ASPECT.h * (width * 1) / ASPECT.w ,
        // quality : 0.2,
    },
    mediaForUpload : {
        width : width * 0.90 ,
        marginHorizontal : width * 0.05 ,
        height : ASPECT.h * (width * 0.90) / ASPECT.w ,
        quality : 0.2,
    },
    profilePicture : {
        width : width * 0.90 ,
        marginHorizontal : width * 0.05 ,
        height : ASPECT_PROFILE_PICTURE.h * (width * 0.90) / ASPECT_PROFILE_PICTURE.w ,
        quality : 0.2,
        aspect:ASPECT_PROFILE_PICTURE
    },
    resultsContainers : {
        width : '98%' ,
        marginHorizontal : '1%',
        height : 120,
        // quality : 0.2,
        // aspect:ASPECT_PROFILE_PICTURE
    },
    

    /**
     * Use in square buttons as Product Miniature in Profile Screen
     */
    miniatureProductInProfileScreen : BOX_WIDTH*1,


}