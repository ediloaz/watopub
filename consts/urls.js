import { Platform } from 'react-native'
export const URLS = {
    appInGooglePlay : Platform.select({
        android : 'https://www.watopub.com/downloadAndroid',
        ios : 'https://www.watopub.com/downloadiOS',
    })
}