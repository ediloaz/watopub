
export default DATA = {
    software : {
        name : "Software",
        imageLocal : require('../../assets/icons/techCategories/Software.png'),
        enable: true,
    }, 
    ai : {
        name : "Inteligencia Artificial",
        imageLocal : require('../../assets/icons/techCategories/IA.png'),
        enable: true,
    }, 
    neuroscience : {
        name : "Neurociencia",
        imageLocal : require('../../assets/icons/techCategories/Neurociencia.png'),
        enable: true,
    }, 
    robotics : {
        name : "Robótica",
        imageLocal : require('../../assets/icons/techCategories/Robótica.png'),
        enable: true,
    }, 
    mechatronics : {
        name : "Mecatrónica",
        imageLocal : require('../../assets/icons/techCategories/Mecatrónica.png'),
        enable: true,
    }, 
    aerospace : {
        name : "Aeroespacial",
        imageLocal : require('../../assets/icons/techCategories/Aeroespacial.png'),
        enable: true,
    }, 
    electronic : {
        name : "Electrónica",
        imageLocal : require('../../assets/icons/techCategories/Electrónica.png'),
        enable: true,
    }, 
    biotechnology : {
        name : "Biotecnología",
        imageLocal : require('../../assets/icons/techCategories/Biotecnología.png'),
        enable: true,
    }, 
    electromedicine : {
        name : "Electromedicina",
        imageLocal : require('../../assets/icons/techCategories/Electromedicina.png'),
        enable: true,
    }, 
    bigdata : {
        name : "Big Data",
        imageLocal : require('../../assets/icons/techCategories/BigData.png'),
        enable: true,
    }, 

    // "Zapaterías" : {
    //     "name" : "Zapaterías",
    //     // "imageLocal" : require("BC_18.png"),
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_18.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Ropa" : {
    //     "name" : "Ropa",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_26.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Farmacias" : {
    //     "name" : "Farmacias",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_27.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Comida" : {
    //     "name" : "Comida",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_28.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Deporte" : {
    //     "name" : "Deporte",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_17.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Muebles" : {
    //     "name" : "Muebles",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_14.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Cosméticos" : {
    //     "name" : "Cosméticos",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_23.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Juguetes" : {
    //     "name" : "Juguetes",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_24.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Veterinarias" : {
    //     "name" : "Veterinarias",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_15.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Tecnología" : {
    //     "name" : "Tecnología",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_25.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Cocina" : {
    //     "name" : "Cocina",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_19.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Ferreterías" : {
    //     "name" : "Ferreterías",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_20.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Bebés" : {
    //     "name" : "Bebés",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_21.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Hogar" : {
    //     "name" : "Hogar",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_22.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }, 
	// "Escolar" : {
    //     "name" : "Escolar",
    //     "imageLocal" : require('../../assets/icons/businessCategories/BC_16.png'),
    //     "imageFirebasePath" : "",
    //     "defaultColor" : "#000",
    //     "defaultBackgroundColor" : "#000"
    // }
}