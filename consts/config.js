
export const CONFIG = {
    /**
     * Use in media visualization to indicate the Resize Mode
     */
    resizeModeForMedia : 'repeat',

    /**
     * Time in miliseconds for wait in each function
     */
     waitForWaiting: 2500,

}