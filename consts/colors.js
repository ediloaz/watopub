
export const COLORS = {
    red : {
        default : "#FF0000",
    },
    blue: {
        default : '#005EFF',
    },
    yellow: {
        default : '#FFE400',
    },
    green: {
        default : '#00FF12',
    },
    purple: {
        default : '#AC00FF',
    },
    gray: {
        tabBarBackground : '#E8E8E8'
    }
    
}