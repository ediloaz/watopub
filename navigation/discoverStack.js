// React navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import Posts from '../screens/home/posts'
import PostsByCategory from '../screens/home/postsByCategory'
import InteractivityBoxes from '../screens/home/interactivityBoxes'
import ViewSinglePost from '../screens/home/viewSinglePost'
import ProfileIndex from '../screens/profile/index'


const AppStackNavigation = createStackNavigator({
    Posts,
    PostsByCategory,
    InteractivityBoxes,
    ViewSinglePost,
    ProfileIndex,
  },{
    headerMode: 'none',
    initialRouteName:'ProfileIndex',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);
  
  
export default NAVIGATION;