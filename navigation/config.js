// Transitions
import {
    BottomTransition,
    FadeTransition,
} from './transitions'


// Configuration of navigation
const NAVIGATION_CONFIG = _ => {
    return {
        screenInterpolator: (scenceProps) => {
            const position = scenceProps.position;
            const scence = scenceProps.scene;
            const index = scence.index;
            const height = scenceProps.layout.initHeight;
    
            return FadeTransition(index, position);
            // return BottomTransition(index, position, height);
        }
    }
}

export default NAVIGATION_CONFIG