// React
import React , { Fragment, useState, useEffect } from 'react';

// React navigation
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'

// Screens used in this navigation
import Ponint from '../screens/auth/pointsInformation'

// Navigations used in this navigation
import HomeStack from './homeStackBusiness'
import ProfileStack from './profileStack'
import MenuStack from './menuStack'

// Colors
import { COLORS } from '../consts/colors'

// Buttons to use tabBar
import TabBarIcon from './tabBarButtons'
import { Alert } from 'react-native';


// Labels
const LABELS = {
  posts : 'Publicaciones',
  discover : 'Descubrir',
  menu : 'Menú',
}


const HomeNavigation = createBottomTabNavigator({
    Posts      : {
      screen: HomeStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.posts,
        tabBarLabel:LABELS.posts,
        tabBarAccessibilityLabel:LABELS.posts,
        tabBarIcon: ({focused}) => <TabBarIcon name='posts' focused={focused} />,
      }
    },
    Profile      : {
      screen: ProfileStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.discover,
        tabBarLabel:LABELS.discover,
        tabBarAccessibilityLabel:LABELS.discover,
        tabBarIcon: ({focused}) => <TabBarIcon name='discover' focused={focused} />,
        // tabBarOnLongPress: _ => {return AlertForUser()},
      }
    },
    Menu      : {
      screen: MenuStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.menu,
        tabBarLabel:LABELS.menu,
        tabBarAccessibilityLabel:LABELS.menu,
        tabBarIcon: ({focused}) => <TabBarIcon name='menu' focused={focused} />,
        // tabBarOnLongPress: _ => {return AlertForUser()},
      }
    }
    
},{
  initialRouteName : 'Posts',
  activeTintColor: '#F44336',
  tabBarOptions : {
    showLabel:false,
    showIcon:true,
    // activeBackgroundColor:COLORS.gray.tabBarBackground,
    // inactiveBackgroundColor:COLORS.gray.tabBarBackground,
  }
});

const NAVIGATION = createAppContainer(HomeNavigation)

export default NAVIGATION

