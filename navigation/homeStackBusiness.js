// React navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import Index from '../screens/home/indexForBusiness'
// import DeletePost from '../screens/home/deletePost'
// import AddPost from '../screens/home/addPost'
import RegisterBusiness from '../screens/auth/registerBusiness'


const AppStackNavigation = createStackNavigator({
    Index,
    // DeletePost,
    // AddPost,
    OnlyPost:RegisterBusiness,
  },{ 
    headerMode: 'none',
    initialRouteName:'Index',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
})
  

const NAVIGATION = createAppContainer(AppStackNavigation);
  
  
export default NAVIGATION;