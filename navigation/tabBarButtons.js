// React
import React, { useState, useEffect } from 'react';

// Watopub components
import Button from '../components/button/button'
// import CircleButton from '../components/button/circle'
import CircleIcon from '../components/icon/circle'

// Styles
import { STYLES } from './styles'

// Images
const IMAGE_POSTS = require('../assets/icons/tabBar/posts.png')
const IMAGE_POSTS_WHITE = require('../assets/icons/tabBar/postsWhite.png')
const IMAGE_SEARCHER = require('../assets/icons/tabBar/searcher.png')
const IMAGE_SEARCHER_WHITE = require('../assets/icons/tabBar/searcherWhite.png')
const IMAGE_DISCOVER = require('../assets/icons/tabBar/discover.png')
const IMAGE_DISCOVER_WHITE = require('../assets/icons/tabBar/discoverWhite.png')
const IMAGE_MENU = require('../assets/icons/tabBar/menu.png')
const IMAGE_MENU_WHITE = require('../assets/icons/tabBar/menuWhite.png')

// Sizes
const SIZE = 36

function Default(props) {
    
    const [source,setSource] = useState(null)
    const [styleButtonFocusedOn,setStyleButtonFocusedOn] = useState(null)

    const { focused } = props

    useEffect( _ => {
        function UpdateFocused(){
            let sourceToReturn = null
            let styleButtonFocusedOnToReturn = null
            switch (props.name) {
                case 'posts':
                    sourceToReturn = props.focused ? IMAGE_POSTS_WHITE : IMAGE_POSTS
                    // styleButtonFocusedOnToReturn = STYLES.containerPostsUserFocusedOn
                    break;
                case 'searcher':
                    sourceToReturn = props.focused ? IMAGE_SEARCHER_WHITE : IMAGE_SEARCHER
                    // styleButtonFocusedOnToReturn = STYLES.containerSearcherUserFocusedOn
                    break;
                case 'discover':
                    sourceToReturn = props.focused ? IMAGE_DISCOVER_WHITE : IMAGE_DISCOVER
                    // styleButtonFocusedOnToReturn = STYLES.containerDiscoverUserFocusedOn
                    break;
                case 'menu':
                    sourceToReturn = props.focused ? IMAGE_MENU_WHITE : IMAGE_MENU
                    // styleButtonFocusedOnToReturn = STYLES.containerMenuUserFocusedOn
                    break;
                default:
                    sourceToReturn = props.focused ? IMAGE_POSTS_WHITE : IMAGE_POSTS
                    // styleButtonFocusedOnToReturn = STYLES.containerMenuUserFocusedOn
                    break;
                
            }
            setSource(sourceToReturn)
            setStyleButtonFocusedOn(styleButtonFocusedOnToReturn)
        }

        UpdateFocused()

    },[props.focused])

    return (
        <CircleIcon 
        notCircle
        source={ source }
        size={SIZE}
        styleIcon={{ borderRadius:0 }}
        // styleButton={ focused ? [styleButtonFocusedOn] : STYLES.containerOff } 
        />
    )
}

export default Default
