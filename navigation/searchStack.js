// React navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import Searcher from '../screens/searcher/searcher'
import ViewPost from '../screens/home/viewSinglePost'


const AppStackNavigation = createStackNavigator({
    Searcher,
    ViewPost
  },{
    headerMode: 'none',
    initialRouteName:'Searcher',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);
  
  
export default NAVIGATION;