// React navigation
import { createAppContainer , NavigationActions } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import LoginUser from '../screens/auth/loginUser'
import LoginBusiness from '../screens/auth/loginBusiness'
import RegisterUser from '../screens/auth/registerUser'
import RegisterBusiness from '../screens/auth/registerBusiness'
import PointsInformation from '../screens/auth/pointsInformation'

// Navigations used in this navigation
import HomeNavigation from './home'
 

const AppStackNavigation = createStackNavigator({
    LoginUser,
    LoginBusiness,
    RegisterUser,
    RegisterBusiness,
    PointsInformation,
    Home: {
        screen: HomeNavigation,
    }
  },{
    headerMode: 'none',
    initialRouteName:'LoginUser',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);

  
export default NAVIGATION;