// React navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import Index from '../screens/profile/index'
import Edit from '../screens/profile/edit'
import ViewProduct from '../screens/profile/viewProduct'
import DeletePost from '../screens/home/deletePost'
import AddProduct from '../screens/profile/addProduct'



const AppStackNavigation = createStackNavigator({
    Index,
    Edit,
    ViewProduct,
    DeletePost,
    AddProduct,
  },{ 
    headerMode: 'none',
    initialRouteName:'Index',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
})
  

const NAVIGATION = createAppContainer(AppStackNavigation);
  
  
export default NAVIGATION;