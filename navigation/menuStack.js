// React navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import Initial from '../screens/menu/initial'
import WatopubPoints from '../screens/auth/welcome'
import ConditionsAndTerms from '../screens/menu/conditionsAndTerms'
// import AuthNav from '../screens/menu/conditionsAndTerms'

// Navigations used in this navigation
// import AuthNav from './auth'
// import { CustomNavigator } from './auth'
// import { AppStackNavigation as AppStackNavigation2 } from './auth'

const AppStackNavigation = createStackNavigator({
  Initial,
  WatopubPoints,
  ConditionsAndTerms,
  // Auth: {
  //   screen: AppStackNavigation2,
  //   // getScreen()
  // },
  },{
    headerMode: 'none',
    initialRouteName:'Initial',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true  
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);
// const NAVIGATION = AppStackNavigation;
  
  
export default NAVIGATION;