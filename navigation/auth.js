// React navigation
import React from 'react'
import { createAppContainer , NavigationActions } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
import LoginUser from '../screens/auth/loginUser'
// import LoginBusiness from '../screens/auth/loginBusiness'
import RegisterUser from '../screens/auth/registerUser'
// import RegisterBusiness from '../screens/auth/registerBusiness'
import Welcome from '../screens/auth/welcome'

// Navigations used in this navigation
// import HomeNavigation from './home'
 

export const AppStackNavigation = createStackNavigator({
    LoginUser,
    RegisterUser,
    Welcome,
  },{
    headerMode: 'none',
    initialRouteName:'LoginUser',
    defaultNavigationOptions:{
      ...TransitionPresets.SlideFromRightIOS,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);

  
export default NAVIGATION;

export class CustomNavigator extends React.Component {
  static router = AppStackNavigation.router;
  render() {
    const { navigation } = this.props;

    return <AppStackNavigation navigation={navigation} />;
  }
}