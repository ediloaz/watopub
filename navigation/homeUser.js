// React
import React , { Fragment, useState, useEffect } from 'react';

// React navigation
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'

// Screens used in this navigation
import Ponint from '../screens/auth/pointsInformation'

// Navigations used in this navigation
import HomeStack from './homeStackUser'
import SearchStack from './searchStack'
import DiscoverStack from './discoverStack'
import MenuStack from './menuStack'

// Colors
import { COLORS } from '../consts/colors'

// Buttons to use tabBar
import TabBarIcon from './tabBarButtons'
import { Alert } from 'react-native';

// Labels
const LABELS = {
  posts : 'Publicaciones',
  searcher : 'Buscador',
  discover : 'Descubrir',
  menu : 'Menú',
}


const HomeNavigation = createBottomTabNavigator({
    Posts      : {
      screen: HomeStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.posts,
        tabBarLabel:LABELS.posts,
        tabBarAccessibilityLabel:LABELS.posts,
        tabBarIcon: ({focused}) => <TabBarIcon name='posts' focused={focused} />,
      }
    },
    Searcher      : {
      screen: SearchStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.searcher,
        tabBarLabel:LABELS.searcher,
        tabBarAccessibilityLabel:LABELS.searcher,
        tabBarIcon: ({focused}) => <TabBarIcon name='searcher' focused={focused} />,
      }
    },
    Discover      : {
      screen: DiscoverStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.discover,
        tabBarLabel:LABELS.discover,
        tabBarAccessibilityLabel:LABELS.discover,
        tabBarIcon: ({focused}) => <TabBarIcon name='discover' focused={focused} />,
        // tabBarOnLongPress: _ => {return AlertForUser()},
      }
    },
    Menu      : {
      screen: MenuStack,
      navigationOptions : {
        tabBarVisible:true,
        title:LABELS.menu,
        tabBarLabel:LABELS.menu,
        tabBarAccessibilityLabel:LABELS.menu,
        tabBarIcon: ({focused}) => <TabBarIcon name='menu' focused={focused} />,
        // tabBarOnLongPress: _ => {return AlertForUser()},
      }
    }
    
},{
  initialRouteName : 'Posts',
  activeTintColor: '#000',
  // tabBarComponent: ({focused}) => <TabBarIcon name='menu' focused={focused} />,
  
  tabBarOptions : {
    showLabel:false,
    showIcon:true,
    
    style:{
      position:'absolute',
      borderRadius:30,
      marginHorizontal:'18%',
      marginVertical:5,
      paddingVertical:20,
      paddingHorizontal:15,
      borderTopColor:'#EFEFEF',
      borderBottomColor:'#EFEFEF',
      borderWidth:0,
      backgroundColor:'#F5F5F5',
      // backgroundColor:'#0FF',
      shadowColor: "#FFF",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.33,
      shadowRadius: 5.97,

      elevation: 2,
    }
    // activeBackgroundColor:COLORS.gray.tabBarBackground,
    // inactiveBackgroundColor:COLORS.gray.tabBarBackground,
  }
});

const NAVIGATION = createAppContainer(HomeNavigation)

export default NAVIGATION


async function AlertForUser(){
  
    Alert.alert(
        'Mensaje',
        'Pendiente para la próxima versión testeable',
        [
          {
              text:'Aceptar',
              style : 'default'
          }
        ],
        {
            cancelable:false,
            onDismiss:false,
        }
    )
}