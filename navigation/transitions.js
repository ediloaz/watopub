export const FadeTransition = (index, position) => {
    const sceneRange = [index-1, index];
    const outputOpacity = [0, 1];
    const transition = position.interpolate({
      inputRange: sceneRange,
      outputRange: outputOpacity,
    });
    return {
      opacity: transition
    }
  }

export const BottomTransition = (index, position, height) => {
    const sceneRange = [index-1, index, index+1];
    const outputHeight = [height, 0, 0];
    const transition = position.interpolate({
      inputRange: sceneRange,
      outputRange: outputHeight,
    });
    return {
      transform: [{ translateX: transition }]
    }
  }