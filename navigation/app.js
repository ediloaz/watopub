// React navigation
import React from 'react'

// Redux
import store from '../redux/store';

import { createAppContainer , NavigationActions } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';

// Screens used in this navigation
// import LoginUser from '../screens/auth/loginUser'
// import LoginBusiness from '../screens/auth/loginBusiness'
// import RegisterUser from '../screens/auth/registerUser'
// import RegisterBusiness from '../screens/auth/registerBusiness'
import Loading from '../screens/splash/initial'

// Navigations used in this navigation
// import HomeNavigation from './home'
import HomeNavigationBusiness from './homeBusiness'
import HomeNavigationUser from './homeUser'
import AuthNavigation from './auth'
 

export const AppStackNavigation = createStackNavigator({
    Loading:Loading, 
    // Home:HomeNavigation,
    // Home: store.getState().account.accountType==='user' ? HomeNavigationUser : HomeNavigationBusiness,
    HomeUser: HomeNavigationUser,
    HomeBusiness: HomeNavigationBusiness,
    Auth:AuthNavigation,
    // LoginBusiness,
    // RegisterUser,
    // RegisterBusiness,
    // PointsInformation,
    // Home: {
    //     screen: HomeNavigation,
    // }
  },{
    headerMode: 'none',
    initialRouteName:'Loading',
    defaultNavigationOptions:{
      ...TransitionPresets.ScaleFromCenterAndroid,
      headerTitleAlign:'center',
    },
    navigationOptions : {
        tabBarVisible : true,
    }
  })
  

const NAVIGATION = createAppContainer(AppStackNavigation);

  
export default NAVIGATION;

export class CustomNavigator extends React.Component {
  static router = AppStackNavigation.router;
  render() {
    const { navigation } = this.props;

    return <AppStackNavigation navigation={navigation} />;
  }
}