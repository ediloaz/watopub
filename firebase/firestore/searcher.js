// Firebase
import firebase from '../init'


// Firestore
import DB_FIRESTORE from '../firestore'

// Consts
import {
    COLLECTION_NAMES,
    DOC_NAMES,
} from '../consts'

// Store
import store from '../../redux/store'

// Actions
import {
    LoginAccount,
    LogoutAccount
} from '../../redux/actions/account'


export async function Search ( stringToSearch ) {
    
    return new Promise( (resolve) => {
        console.log(`user,password ${email} ${password}`)
        firebase.auth().signInWithEmailAndPassword(email, password) 
        .then(r => {
            console.log("logueeeado")
            console.log(r)
            console.log('.')
            store.dispatch(LoginAccount({ login:true , email }))
            resolve(true)
        })
        .catch(e => {
            console.log("error en el logueeeado")
            console.log(e)
            console.log('.')
            resolve(false)
        })
    })  
}

export function LogIn (object) {
    const { email , password } = object 
    return new Promise( (resolve) => {
        console.log(`user,password ${email} ${password}`)
        firebase.auth().signInWithEmailAndPassword(email, password) 
        .then(r => {
            console.log("logueeeado")
            console.log(r)
            console.log('.')
            store.dispatch(LoginAccount({ login:true , email }))
            resolve(true)
        })
        .catch(e => {
            console.log("error en el logueeeado")
            console.log(e)
            console.log('.')
            resolve(false)
        })
    })  
}

export  const RegisterUser = async function (object){
    return new Promise(
        function(resolve, reject){
            console.log('User data to register')
            console.log(object)
            SaveDataUse(object).then(
                _ => CreateUser(object.email , object.password).then(
                    resolve()       // successful
                ).catch( 
                    _ => reject()   // failed
                )
            ).catch(
                _ => reject()       // failed
            )
        }
    )
    
}

const SaveDataUse = function(object){
    return new Promise (
        function (resolve, reject){
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
            const DOC_REF = COLLECTION_REF.doc(DOC_NAMES.user)
            const KEY = object.email || parseInt(Math.random()*1000000)
            const OBJECT = { [KEY] : object }
            DOC_REF.set( OBJECT , {merge:true} )
            .then(r => {
                console.log("Éxito agregando los datos de registro del usuario")
                console.log(r)
                resolve()
            })
            .catch(e => {
                console.log("Error agregando los datos de registro del usuario")
                console.log(e)
                reject()
            })
        }
    )
}

const CreateUser = function (email, password){
    return new Promise(
        function(resolve, reject){
            firebase.auth().createUserWithEmailAndPassword(email,password)
            .then(r => {
                console.log("Exito en la creación del usuario")
                console.log(r)
                resolve()
            })
            .catch(r => {
                console.log("Error creando el usuario")
                console.log(r)
                reject()
            })
        }
    )
}

export function CheckAccountIsLogged () {
    return new Promise( (resolve) => {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log(`El usuario ${user.email} sí está loggueado`)
                // valueToReturn = true
                resolve(true)
            } else {
                console.log("El usuario NO está loggueado")
                resolve(false)
            }
        })
    })
}
// export let CheckAccountIsLogged_2 =  new Promise( (resolve,reject) => {
//     firebase.auth().onAuthStateChanged(function(user) {
//         if (user) {
//             console.log(`El usuario ${user.email} sí está loggueado`)
//             // valueToReturn = true
//             resolve(true)
//         } else {
//             console.log("El usuario NO está loggueado")
//             resolve(false)
//         }
//     })
// })

export const CurrentEmail = firebase.auth().currentUser ? firebase.auth().currentUser.email : 'No está logueado'

export function SignOut () {
    firebase.auth().signOut()
    .then( r => {
        console.log(`Éxito en el deslogue ${r}`)
        store.dispatch(LogoutAccount({ login:false , email:CurrentEmail  }))
    })
    .catch( e => console.log(`Error en el deslogue ${e}`))
}
 

/**
 * 
 * TO UPDATE
 let cityRef = db.collection('cities').doc('DC');

// Set the 'capital' field of the city
let updateSingle = cityRef.update({capital: true});
 */