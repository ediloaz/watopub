// Firebase
import firebase from '../init'


// Firestore
import DB_FIRESTORE , {
    UploadImage
} from '../firestore'

// Consts
import {
    COLLECTION_NAMES,
} from '../consts'

// Utilities
import {
    GetDateForStorageControl
} from '../../utils/utilities'

/**
 * Login method
 */
export function LogIn (object) {
    const { email , password } = object 
    return new Promise( (resolve) => {
        console.log(`user,password ${email} ${password}`)
        firebase.auth().signInWithEmailAndPassword(email, password) 
        .then(r => {
            console.log("logueeeado")
            console.log(r)
            console.log('.')
            // store.dispatch(LoginAccount({ login:true , email }))
            resolve(true)
        })
        .catch(e => {
            console.log("error en el logueeeado")
            console.log(e)
            console.log('.')
            resolve(false)
        })
    })  
}

export function GetDataOfCurrentAccount(){
    return new Promise (
        function (resolve,reject){
            try {
                console.log("func 1")
                // Get current email
                const CURRENT_EMAIL = firebase.auth().currentUser.email.toString().toLowerCase()
                // Query to DB to extract its data
                console.log("func 2")
                const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
                console.log("func 3")
                const KEY = CURRENT_EMAIL
                const DOC_REF = COLLECTION_REF.doc(KEY)
                console.log("func 4")
                console.log(DOC_REF.id)
                console.log(DOC_REF.path)
                DOC_REF.get()
                .then(
                    r => {
                        console.log(r)
                        resolve(r.data())
                    }
                )
                .catch(
                    e => {
                        console.log(e)
                        reject(e)
                    }
                )
            } catch (e) {
                reject(e)
            }
        }
    )
}

export function RegisterUser (object){
    object.accountType = 'user'
    return RegisterAccount(object)
}

export function RegisterBusiness (object){
    object.accountType = 'business'
    return RegisterAccount(object)
}

export async function RegisterAccount (object){
    return new Promise(
        function(resolve, reject){
            console.log('Registro para los datos')
            console.log(object)
            CreateUser(object.email , object.password)
            .then(
                _ => {
                    object.registrationDate = firebase.firestore.FieldValue.serverTimestamp()
                    SaveDataForUser(object)
                    .then(
                        _ => {
                            console.log("Éxito registrando al usuario")
                            resolve()
                        }
                    ).catch( 
                        e => {
                            const message = 'Ya se creó el usuario, pero hubo un problema al guardar tus datos (ya podés iniciar sesión con tus datos normales)'
                            console.log(message)
                            console.log(e)
                            reject(message)
                        }
                    )
                }
            ).catch(
                e => {
                    const message = "Ya existe registrado este correo"
                    console.log(message)
                    console.log(e)
                    reject(message)
                }
            )
        }
    )
}

function SaveDataForUser (object){
    return new Promise (
        function (resolve, reject){
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
            object.email = object.email.toString().toLowerCase()
            const KEY = object.email || parseInt(Math.random()*1000000)
            const DOC_REF = COLLECTION_REF.doc(KEY)
            // const OBJECT = { [KEY] : object }
            // DOC_REF.set( OBJECT , {merge:true} )
            DOC_REF.set( object )
            .then(r => {
                console.log("Éxito agregando los datos de registro del usuario")
                console.log(r)
                resolve()
            })
            .catch(e => {
                console.log("Error agregando los datos de registro del usuario")
                console.log(e)
                reject()
            })
        }
    )
}

function CreateUser (email, password){
    return new Promise(
        function(resolve, reject){
            firebase.auth().createUserWithEmailAndPassword(email,password)
            .then(r => {
                console.log("Exito en la creación del usuario")
                console.log(r)
                resolve()
            })
            .catch(r => {
                console.log("Error creando el usuario")
                console.log(r)
                reject()
            })
        }
    )
}

export function CheckAccountIsLogged () {
    return new Promise( (resolve) => {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log(`El usuario ${user.email} sí está loggueado`)
                // valueToReturn = true
                resolve(true)
            } else {
                console.log("El usuario NO está loggueado")
                resolve(false)
            }
        })
    })
}
// export let CheckAccountIsLogged_2 =  new Promise( (resolve,reject) => {
//     firebase.auth().onAuthStateChanged(function(user) {
//         if (user) {
//             console.log(`El usuario ${user.email} sí está loggueado`)
//             // valueToReturn = true
//             resolve(true)
//         } else {
//             console.log("El usuario NO está loggueado")
//             resolve(false)
//         }
//     })
// })

export const CurrentEmail = firebase.auth().currentUser ? firebase.auth().currentUser.email : 'No está logueado'

// export function SignOut () {
//     firebase.auth().signOut()
//     .then( r => {
//         console.log(`Éxito en el deslogue ${r}`)
//         store.dispatch(LogoutAccount({ login:false , email:CurrentEmail  }))
//     })
//     .catch( e => console.log(`Error en el deslogue ${e}`))
// }

export function SignOut () {
    return new Promise( function(resolve,reject){
        try {
            firebase.auth().signOut()
            .then( r => {
                console.log(`Éxito en el deslogue | ${r}`)
                // store.dispatch(LogoutAccount({ login:false , email:CurrentEmail  }))
                resolve(true)
            })
            .catch( e => {
                console.log(`Error en el deslogue | ${e}`)
                reject(e)
            })
        } catch (error) {
            reject(error)
        }
    } )
}
 

/**
 * 
 * TO UPDATE
 let cityRef = db.collection('cities').doc('DC');

// Set the 'capital' field of the city
let updateSingle = cityRef.update({capital: true});
 */



 
/**
 * Update business information 
 * @param {*} object 
 */
export function UpdateBusinessInformation (object) {
    return new Promise( (resolve,reject) => {
        try {
            const CURRENT_EMAIL = firebase.auth().currentUser.email
            const CURRENT_DATE = firebase.firestore.FieldValue.serverTimestamp()
            const KEY = CURRENT_EMAIL
            const OBJECT_TO_UPLOAD = {}

            console.log("Objeto a subir:")
            console.log(object)
            
            OBJECT_TO_UPLOAD.information = object.information
            OBJECT_TO_UPLOAD.lastUpdateDate = CURRENT_DATE
            
            const IMAGE_NAME = object.media ? `${CURRENT_EMAIL} | ${GetDateForStorageControl(CURRENT_DATE)} | ${object.media.toString().split("/").pop()}` : null
            if (object.media) OBJECT_TO_UPLOAD.media = IMAGE_NAME   // Only create key if exist media

            // For use in DB save
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
            const DOC_REF = COLLECTION_REF.doc(KEY)

            // Post on Database
            function PostOnDatabase(){
                // DOC_REF.set( OBJECT , { merge:true } )
                DOC_REF.set( OBJECT_TO_UPLOAD, {merge:true} )
                .then(r => {
                    console.log("Se actualizó con éxito la información del negocio")
                    console.log(r)
                    resolve(true)
                })
                .catch(e => {
                    console.log("Error actualizando la información del negocio")
                    console.log(e)
                    reject(`Error publicando en la base de datos del servidor | ${e}`)
                })
            }
        
            // Upload media (image or video) as resource on Firebase Storage
            IMAGE_NAME
            ?
            UploadImage(object.media, IMAGE_NAME, COLLECTION_NAMES.accounts)
            .then((r) => {
                console.log(`El archivo se subió exitosamente ${r}`)
                PostOnDatabase()
            })
            .catch((e) => {
                console.log("Error subiendo el archivo")
                console.log(e)
                reject(`Error al subir el archivo al servidor | ${e}`)
            })
            :
            PostOnDatabase()

        } catch (error) {
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
        
}