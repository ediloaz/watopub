// Firebase
import firebase from '../init'


// Firestore
import DB_FIRESTORE , {
    GetImage
} from '../firestore'

// Consts
import {
    COLLECTION_NAMES,
    DOC_NAMES,
    VALUES_FORMATS,
} from '../consts'

// Utilities
import {
    GetDateForStorageControl
} from '../../utils/utilities'

// Store
import store from '../../redux/store'

export function UpdateControl (object) {
    return new Promise( (resolve,reject) => {
        try {
            console.log(1)
            const KEY_IN_POST_CONTAINS_ACCOUNTS = COLLECTION_NAMES.keyInPostThatContainsControlsByAccounts
            const KEY_IN_ACCOUNT_CONTAINS_POSTS = COLLECTION_NAMES.keyInAccountThatContainsControlsByPosts
            const KEY_POST = object.key 
            const CURRENT_EMAIL = firebase.auth().currentUser.email
            console.log(2)
            
            // For use in DB save
            const ACCOUNTS_COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
            const ACCOUNTS_DOC_REF = ACCOUNTS_COLLECTION_REF.doc(CURRENT_EMAIL)
            console.log(3)
            console.log(COLLECTION_NAMES.controls)
            console.log(CURRENT_EMAIL)
            
            // Post on Database
            ACCOUNTS_DOC_REF.get()
            .then( r => {
                console.log("Se actualizó el control con éxito")
                console.log(r)
                if (r.get(KEY_IN_ACCOUNT_CONTAINS_POSTS)){
                    console.log("SÍ existe este control registrado en su listado")
                    resolve(true)
                }else{
                    console.log("NO existe este control registrado en su listado")
                    reject("NO existe este control registrado en su listado")
                }
            }).catch( e => {
                console.log("Error subiendo la publicación")
                console.log(e)
                reject(`Error publicando en la base de datos del servidor | ${e}`)
            })
        } catch (error) {
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
}

export function CheckControl (object) {
    return new Promise( (resolve,reject) => {
        try {
            const KEY_POST = object.key 
            const CURRENT_EMAIL = firebase.auth().currentUser.email

            // For use in DB save
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.controls)
            const DOC_REF = COLLECTION_REF.doc(CURRENT_EMAIL)

            // Post on Database
            DOC_REF.get()
            .then( r => {
                console.log("Se subió la publicación con éxito")
                console.log(r)
                if (r.get(KEY_POST)){
                    console.log("Existe este control registrado en su listado")
                    resolve(true)
                }else{
                    console.log("NO existe este control registrado en su listado")
                    reject("NO existe este control registrado en su listado")
                }
            }).catch( e => {
                console.log("Error subiendo la publicación")
                console.log(e)
                reject(`Error publicando en la base de datos del servidor | ${e}`)
            })
        } catch (error) {
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
}

/**
 * Return posts to current user
 */
export async function GetPostsOfMyBusiness( category=null){
    // let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts)
    // let res = collectionRef.get()
    // const CURRENT_EMAIL = firebase.auth().currentUser.email
    // let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts).doc(CURRENT_EMAIL)
    let collectionRef = 
    category
    ?
    typeof category === 'object'
    ?
    DB_FIRESTORE.collection(COLLECTION_NAMES.posts).where('businessName','==',category.user)
    :
    DB_FIRESTORE.collection(COLLECTION_NAMES.posts).where('category','==',category)
    :
    DB_FIRESTORE.collection(COLLECTION_NAMES.posts).orderBy('date','desc')
    // let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts).get()
    // let res = collectionRef.get()
    return collectionRef
}

/**
 * Return posts to current user
 */
export async function SearchByBusinessName( businessName ){
    let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts).where('businessName','>=',businessName).where('businessName','<=',businessName+'\uf8ff')
    // let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts).where('forSearch','array-contains',businessName)
    // return (await collectionRef.get()).docs()
    return collectionRef
}

/**
 * Return posts to current user
 */
export async function GetRandomPost( ){
    
    let collectionRef = (await DB_FIRESTORE.collection(COLLECTION_NAMES.posts).get()).docs
    const CANT = collectionRef.length
    const INDEX_SELECTED = (Math.random() * (CANT-1 - 0) + 0).toFixed(0)
    console.log("cantidad")
    console.log(INDEX_SELECTED)
    return collectionRef[INDEX_SELECTED].data()
}

/**
 * Upload a post 
 * @param {*} object 
 */
export function UploadPost (object) {
    return new Promise( (resolve,reject) => {
        try {
            const CURRENT_EMAIL = firebase.auth().currentUser.email
            const CURRENT_DATE = firebase.firestore.FieldValue.serverTimestamp()
            const KEY = `${Date.now().toPrecision(13)}-${CURRENT_EMAIL}`
            const OBJECT_TO_UPLOAD = VALUES_FORMATS.postEmpty
            OBJECT_TO_UPLOAD.description = object.description
            OBJECT_TO_UPLOAD.mediaType = object.mediaType
            OBJECT_TO_UPLOAD.category = object.category
            // OBJECT_TO_UPLOAD.media = object.media
            OBJECT_TO_UPLOAD.date = CURRENT_DATE
            // OBJECT_TO_UPLOAD.category = store.getState().account.businessCategory
            OBJECT_TO_UPLOAD.businessName = store.getState().account.name
            OBJECT_TO_UPLOAD.businessEmail = store.getState().account.email

            
            const IMAGE_NAME = object.media ? `${CURRENT_EMAIL} | ${GetDateForStorageControl(CURRENT_DATE)} | ${object.media.toString().split("/").pop()}` : null
            OBJECT_TO_UPLOAD.media = IMAGE_NAME

            // For use in DB save
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.posts)
            const DOC_REF = COLLECTION_REF.doc(KEY)
            // const OBJECT = { [KEY] : OBJECT_TO_UPLOAD }

            // Post on Database
            function PostOnDatabase(){
                // DOC_REF.set( OBJECT , { merge:true } )
                DOC_REF.set( OBJECT_TO_UPLOAD )
                .then(r => {
                    console.log("Se subió la publicación con éxito")
                    console.log(r)
                    resolve(true)
                })
                .catch(e => {
                    console.log("Error subiendo la publicación")
                    console.log(e)
                    reject(`Error publicando en la base de datos del servidor | ${e}`)
                })
            }
        
            // Upload media (image or video) as resource on Firebase Storage
            IMAGE_NAME
            ?
            UploadImage(object.media, IMAGE_NAME)
            .then((r) => {
                console.log(`El archivo se subió exitosamente ${r}`)
                PostOnDatabase()
            })
            .catch((e) => {
                console.log("Error subiendo el archivo")
                console.log(e)
                reject(`Error al subir el archivo al servidor | ${e}`)
            })
            :
            PostOnDatabase()

        } catch (error) {
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
        
}


// export function Delete (object) {
//     const TYPE = COLLECTION_NAMES.posts
//     return DeleteItem(object,TYPE)
// }

const UploadImage = async(uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    var ref = firebase.storage().ref().child(COLLECTION_NAMES.posts).child(imageName);
    return ref.put(blob);
}

export const GetImageFromPosts = (nameImage) => GetImage(nameImage,'posts')
export const GetImageFromProducts = (nameImage) => GetImage(nameImage,'products')



export  const RegisterUser = async function (object,){
    return new Promise(
        function(resolve, reject){
            console.log('User data to register')
            console.log(object)
            SaveDataUse(object).then(
                _ => CreateUser(object.email , object.password).then(
                    resolve()       // successful
                ).catch( 
                    _ => reject()   // failed
                )
            ).catch(
                _ => reject()       // failed
            )
        }
    )
    
}

const SaveDataUse = function(object){
    return new Promise (
        function (resolve, reject){
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts)
            const DOC_REF = COLLECTION_REF.doc(DOC_NAMES.user)
            const KEY = object.email || parseInt(Math.random()*1000000)
            const OBJECT = { [KEY] : object }
            DOC_REF.set( OBJECT , {merge:true} )
            .then(r => {
                console.log("Éxito agregando los datos de registro del usuario")
                console.log(r)
                resolve()
            })
            .catch(e => {
                console.log("Error agregando los datos de registro del usuario")
                console.log(e)
                reject()
            })
        }
    )
}

const CreateUser = function (email, password){
    return new Promise(
        function(resolve, reject){
            firebase.auth().createUserWithEmailAndPassword(email,password)
            .then(r => {
                console.log("Exito en la creación del usuario")
                console.log(r)
                resolve()
            })
            .catch(r => {
                console.log("Error creando el usuario")
                console.log(r)
                reject()
            })
        }
    )
}

export function CheckAccountIsLogged () {
    return new Promise( (resolve) => {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log(`El usuario ${user.email} sí está loggueado`)
                // valueToReturn = true
                resolve(true)
            } else {
                console.log("El usuario NO está loggueado")
                resolve(false)
            }
        })
    })
}
// export let CheckAccountIsLogged_2 =  new Promise( (resolve,reject) => {
//     firebase.auth().onAuthStateChanged(function(user) {
//         if (user) {
//             console.log(`El usuario ${user.email} sí está loggueado`)
//             // valueToReturn = true
//             resolve(true)
//         } else {
//             console.log("El usuario NO está loggueado")
//             resolve(false)
//         }
//     })
// })

export const CurrentEmail = firebase.auth().currentUser ? firebase.auth().currentUser.email : 'No está logueado'

// export function SignOut () {
//     firebase.auth().signOut()
//     .then( r => {
//         console.log(`Éxito en el deslogue ${r}`)
//         store.dispatch(LogoutAccount({ login:false , email:CurrentEmail  }))
//     })
//     .catch( e => console.log(`Error en el deslogue ${e}`))
// }

export function SignOut () {
    return new Promise( function(resolve,reject){
        try {
            firebase.auth().signOut()
            .then( r => {
                console.log(`Éxito en el deslogue | ${r}`)
                // store.dispatch(LogoutAccount({ login:false , email:CurrentEmail  }))
                resolve(true)
            })
            .catch( e => {
                console.log(`Error en el deslogue | ${e}`)
                reject(e)
            })
        } catch (error) {
            reject(error)
        }
    } )
}
 

/**
 * 
 * TO UPDATE
 let cityRef = db.collection('cities').doc('DC');

// Set the 'capital' field of the city
let updateSingle = cityRef.update({capital: true});
 */