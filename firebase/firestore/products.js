// Firebase
import firebase from '../init'


// Firestore
import DB_FIRESTORE from '../firestore'

// Consts
import {
    COLLECTION_NAMES,
    VALUES_FORMATS,
} from '../consts'

// Utilities
import {
    GetDateForStorageControl
} from '../../utils/utilities'

// Store
import store from '../../redux/store'


/**
 * Upload a post 
 * @param {*} object 
 */
export function UploadProduct (object) {
    return new Promise( (resolve,reject) => {
        try {
            const CURRENT_EMAIL = firebase.auth().currentUser.email
            const CURRENT_DATE = firebase.firestore.FieldValue.serverTimestamp()
            const KEY = `${Date.now().toPrecision(13)}-${CURRENT_EMAIL}`
            const OBJECT_TO_UPLOAD = VALUES_FORMATS.postEmpty
            OBJECT_TO_UPLOAD.title = object.title
            OBJECT_TO_UPLOAD.description = object.description
            OBJECT_TO_UPLOAD.mediaType = object.mediaType
            // OBJECT_TO_UPLOAD.media = object.media
            OBJECT_TO_UPLOAD.date = CURRENT_DATE
            OBJECT_TO_UPLOAD.category = store.getState().account.businessCategory
            OBJECT_TO_UPLOAD.businessName = store.getState().account.name

            
            const IMAGE_NAME = object.media ? `${CURRENT_EMAIL} | ${GetDateForStorageControl(CURRENT_DATE)} | ${object.media.toString().split("/").pop()}` : null
            OBJECT_TO_UPLOAD.media = IMAGE_NAME

            // For use in DB save
            const COLLECTION_REF = DB_FIRESTORE.collection(COLLECTION_NAMES.products)
            const DOC_REF = COLLECTION_REF.doc(KEY)
            // const OBJECT = { [KEY] : OBJECT_TO_UPLOAD }

            // Post on Database
            function PostOnDatabase(){
                // DOC_REF.set( OBJECT , { merge:true } )
                DOC_REF.set( OBJECT_TO_UPLOAD )
                .then(r => {
                    console.log("Se subió el producto con éxito")
                    console.log(r)
                    resolve(true)
                })
                .catch(e => {
                    console.log("Error subiendo el producto")
                    console.log(e)
                    reject(`Error publicando en la base de datos del servidor | ${e}`)
                })
            }
        
            // Upload media (image or video) as resource on Firebase Storage
            IMAGE_NAME
            ?
            UploadImage(object.media, IMAGE_NAME)
            .then((r) => {
                console.log(`El archivo se subió exitosamente ${r}`)
                PostOnDatabase()
            })
            .catch((e) => {
                console.log("Error subiendo el archivo")
                console.log(e)
                reject(`Error al subir el archivo al servidor | ${e}`)
            })
            :
            PostOnDatabase()

        } catch (error) {
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
        
}


const UploadImage = async(uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    var ref = firebase.storage().ref().child(COLLECTION_NAMES.products).child(imageName);
    return ref.put(blob);
}


/**
 * Return posts to current user
 */
export async function GetProductsOfMyBusiness( businessEmail ){
    console.log(businessEmail)
    console.log("businessEmail")
    let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.products).where('businessName','==',businessEmail)
    // let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.products).where('businessEmail','==',businessEmail)
    return collectionRef
}

