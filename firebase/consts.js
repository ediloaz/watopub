
export const NAME_BUSINESS_CATEGORIES = 'businessCategories'

export const COLLECTION_NAMES = {
    businessCategories : 'businessCategories',
    
    accounts : 'accounts',

    posts : 'posts', 

    products : 'products', 
    
    controls : 'controls', 

    /**
     * Contains a list of every keys/emails of accounts that
     * controls to this post
     */
    keyInPostThatContainsControlsByAccounts: 'controlsByAccounts',
    
    /**
     * Contains a list of every keys of posts that this user did
     * controls in posts
     */
    keyInAccountThatContainsControlsByPosts: 'controlsByPosts',
    
}

export const DOC_NAMES = {
    user : 'users',
    business : 'business'
}

export const VALUES_FORMATS = {
    postEmpty : {
        description:'Descripción de la publicación',
        media:'link de la imagen',
        mediaType:null,
        controls : 0,
        date:'asd',
    }
}