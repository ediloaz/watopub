// Firebase
import firebase from './init'

// Consts
import {
    NAME_BUSINESS_CATEGORIES, 
    COLLECTION_NAMES,
} from './consts'

const DB_FIRESTORE = firebase.firestore()
// DB_FIRESTORE.settings({ experimentalForceLongPolling: true });
firebase.firestore().enablePersistence() 

export default DB_FIRESTORE


/**
 * Delete a post 
 * 
 * Can use two methods:
 *  1. Delete all data associated.
 *  2. Use a boolean field, that indicates if is a active (or not delete) post.
 * 
 * We'll use the first 1.
 * 
 * @param {*} object 
 */
export function DeleteItem (object,type) {
    return new Promise( (resolve,reject) => {
        try {
            const KEY = object.key
            const TYPE = type

            // For use in DB save
            console.log("TYPE")
            console.log("1TYPE")
            console.log(TYPE)
            console.log(KEY)
            console.log("2TYPE")
            console.log("2TYPE")
            const COLLECTION_REF = DB_FIRESTORE.collection( COLLECTION_NAMES[TYPE] )
            const DOC_REF = COLLECTION_REF.doc(KEY)

            // Delte on Database
            function DeleteOnDatabase(){
                DOC_REF.delete()
                .then(r => {
                    console.log(`Se borró (con campo ${KEY})la publicación con éxito`)
                    console.log(r)
                    resolve(true)
                })
                .catch(e => {
                    console.log(`Error borrando el campo ${KEY} y la publicación`)
                    console.log(e)
                    reject(`Error borrando el campo ${KEY} en la base de datos del servidor | ${e}`)
                })
            }
        
            DeleteOnDatabase()

        } catch (e) {
            console.log(`Error: ${e}`)
            reject("Error desconocido, contacte a servicio al cliente")
        }
    })
        
}

export function ManageResultOfFirestore(doc){
    if (!doc.exists){
        return (null)
    }else{
        return doc.data().values.sort()
    }
}

export async function GetBusinessCategories(){
    let collectionRef = DB_FIRESTORE.collection(NAME_BUSINESS_CATEGORIES).doc('strings')
    let getDoc = collectionRef.get()
    return getDoc
}



/**
 * SECTION OF POSTS
 */


export async function WritePost({ user = "w@w.com" , text , mediaFile ,  }) {
    let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts)
    let docRef = collectionRef.doc(user)



}

export async function Testing_CreateMuchPosts( quantity = 10 ){
    const objectToSave = {
        account : "w@w.com",
        text : "Primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post, primer post",
        date : firebase.firestore.FieldValue.serverTimestamp(),
        media: null,
    }
    console.log("1")
    const collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.posts)
    console.log("2")
    const res = collectionRef.get( ) 
    console.log("3")
    console.log("res")
    console.log(res)
    console.log("res.id")
    console.log(res.id)
    res()
    .then( r => {
        console.log("TOdo bien!")
        console.log(r)
    } )
    .catch( r => {
        console.log("NADA bien!")
        console.log(r)
    } )


}


export const UploadImage = async(uri, imageName, collectionName) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    var ref = firebase.storage().ref().child(collectionName).child(imageName);
    return ref.put(blob);
}


export const GetImage = async (nameImage,collection) => { 
    return new Promise( function(resolve,reject){
        var path = COLLECTION_NAMES[collection] + "/" + nameImage
        var ref = firebase.storage().ref(path);
        ref.getDownloadURL().then(data => { 
            resolve(data)
        }).catch(error => {
            reject()
      })
    })
}


/**
 * Return object of business profile
 */
export async function GetBusinessData( businessKey ){
    console.log('3. GetBusinessData')
    console.log(businessKey)
    let collectionRef = DB_FIRESTORE.collection(COLLECTION_NAMES.accounts).doc(businessKey)
    console.log('collectionRef')
    console.log(collectionRef)
    return (await collectionRef.get()).data()
}