
// firebase
import * as firebase from 'firebase'
import 'firebase/firestore'

// Config for firebase
import { firebaseConfig } from '../consts/credentials/firebaseAuth' 

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
    // firebase.firestore().settings({ experimentalForceLongPolling: true });
    // firebase.firestore().enablePersistence()
}

export default firebase