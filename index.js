// React
import React from 'react'

// Expo
import { registerRootComponent } from 'expo';


// App component
import App from './App';

// Redux
import { Provider } from 'react-redux'

// Store
import store from './redux/store'

// const store = configureStore()

const Index = _ => 
    <Provider store={store}>
        <App />
    </Provider>


// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(Index);
