// React native
import { Share } from 'react-native';

// Consts
import { URLS } from '../consts/urls'

// React navigations
import { StackActions, NavigationActions } from 'react-navigation';


export function FirebaseDateToLocalDateHumanReadable( dateFromFirebase ){
    const NEW_DATE = new Date( dateFromFirebase.toDate() )
    const HUMAN_READABLE = `${NEW_DATE.getDate()}/${NEW_DATE.getMonth()+1}/${NEW_DATE.getFullYear()}`
    return HUMAN_READABLE
}

export function GetDateForStorageControl( ){
    const NEW_DATE = new Date(Date.now())
    const HUMAN_READABLE = `${NEW_DATE.getFullYear()}-${NEW_DATE.getMonth()+1}-${NEW_DATE.getDate()}`
    return HUMAN_READABLE
}

export function GetDateHumanReadable( ){
    const NEW_DATE = new Date(Date.now())
    const HUMAN_READABLE = `${NEW_DATE.getDate()}/${NEW_DATE.getMonth()+1}/${NEW_DATE.getFullYear()}`
    return HUMAN_READABLE
}

export function UpperCaseFirstLetter(text){
    return text.charAt(0).toUpperCase() + text.slice(1);
}

export function ResetNavigation(navigation,routeName,params){
    const resetAction = StackActions.reset({
        index: 0,
        actions: [
            // params
            // ? NavigationActions.navigate({ routeName: routeName })
            NavigationActions.navigate({ routeName: routeName , params:params })
        ],
    });
    navigation.dispatch(resetAction)
}

export const ShareApp = _ => {
    // const RESULT = await Share.share({
    Share.share({
        message:`¡Hola! He encontrado esta app que te va a encantar 🤯♥ \n\n Da clic ${URLS.appInGooglePlay} descargala y ¡empieza a usar! 🙌`,
        url: URLS.appInGooglePlay,
    })
}

export const SharePost = (description,businessName) => {
    Share.share({
        message:`¡Hola! Te comparto la siguiente en publicación de ${businessName} en Watopub:\n${description}  \n\n 🔴 Sino tenés todavía Watopub, descagala en ${URLS.appInGooglePlay} y ¡empieza a usar! 🙌`,
        url: URLS.appInGooglePlay,
        title:'Watopub'
    })
}

export function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}