// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text, Image } from 'react-native';

// Material Kit
// import { Button,  } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'

const IMAGE = require('../../assets/images/logos/background_transparent.png')

function Component(props){

    return (
        <Fragment>
            <View
            style={[
                STYLES.containerComponent,
                props.styleContainer,
            ]}
            >
                <Image
                style={ [
                    STYLES.image,
                    props.size && {
                        width:props.size,
                        height:props.size
                    }
                ] }
                source={IMAGE}
                />
                    
            </View>
        </Fragment>
    )
}

export default Component