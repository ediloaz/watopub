// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text, Image } from 'react-native';

// Material Kit
// import { Button,  } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'

const IMAGE = require('../../assets/images/watopub_points.png')

function Component(props){

    return (
        <Fragment>
            <View
            style={[
                STYLES.containerComponent,
                props.styleContainer,
            ]}
            >
                <Image
                style={ [
                    STYLES.image,
                    props.width && {
                        width:props.width,
                        height:props.width
                    }
                ] }
                source={IMAGE}
                />
                    
            </View>
        </Fragment>
    )
}

export default Component