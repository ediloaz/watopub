// React
import React , { useState } from 'react';

// React native
import { View, Image , TouchableWithoutFeedback } from 'react-native';

// Video
import { Video } from 'expo-av';

// Styles
import { STYLES } from './styles'
import { SIZES } from '../../consts/sizes'


function Component(props){

    const { mediaType , source } = props

    const [backCount, setBackCount] = useState(0)

    return (
        <View
        style={[
        // STYLES.container,
        STYLES.borderRadius,
        props.forView ? STYLES.containerForJustView : STYLES.container,
        props.styleContainer
        ]}
        >
            {/* <TouchableWithoutFeedback
            onPress={() => {
                setBackCount(backCount+1)
                let backTimer
                if (backCount == 2) {
                    clearTimeout(backTimer)
                    props.PressControlFromMedia()
                } else {
                    backTimer = setTimeout(() => {
                    setBackCount(0)
                    }, 3000)
                }

            }}
            > */}
                {
                mediaType==='video'
                ?
                <Video
                source={source}
                rate={1.0}
                volume={1.0}
                isMuted={false}
                resizeMode="contain"
                shouldPlay
                isLooping
                useNativeControls
                style={[
                    props.forView ? STYLES.videoForPostForJustView : STYLES.videoForPost,
                    // STYLES.borderRadius,
                ]}
                // style={{ width: 300 , height: 300 }}
                />
                :
                <Image
                source={source}
                style={ [
                // STYLES.imageForpost,
                props.forView ? STYLES.imageForpostForJustView : STYLES.imageForpost,
                STYLES.borderRadius,
                props.styleMedia
                ]}
                />
                }
            {/* </TouchableWithoutFeedback> */}

        </View>

    )
}

export default Component