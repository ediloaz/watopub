import { StyleSheet } from 'react-native';

import { SIZES } from '../../consts/sizes'
import { CONFIG } from '../../consts/config'

export const STYLES = StyleSheet.create({
    container:{
        // backgroundColor:'#000',
        justifyContent: 'center', //Centered vertically
        alignItems: 'center', // Centered horizontally
        flex:1,
        height:SIZES.mediaForUpload.height,
        width: SIZES.mediaForUpload.width,
        marginHorizontal:SIZES.mediaForUpload.marginHorizontal,
        borderColor:'#111' , 
        borderWidth:1 , 
        borderStyle:'solid' , 
        borderRadius:23,
    },
    containerForJustView : {
        height:SIZES.mediaForView.height,
        width: SIZES.mediaForView.width,
        marginHorizontal:SIZES.mediaForView.marginHorizontal,
    },
    videoForPost:{
        height:'100%',
        width:'100%',
        borderRadius:21,
    },
    image : {
        width:100, 
        height:100, 
        paddingVertical:11,
        borderRadius:23,
        
        resizeMode:CONFIG.resizeModeForMedia,
    },
    borderRadius: {
        // borderRadius:23,
        resizeMode:'contain',
    },
    imageForpost: {
        height:'100%',
        width:'100%',
        borderRadius:21,
    },
    imageForpostForJustView: {
        height:'100%',
        width:'100%',
        // borderRadius:21,
    },
    videoForPostForJustView: {
        height:'100%',
        width:'100%',
        // borderRadius:21,
    },
    
})

