// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text, Image } from 'react-native';

// Styles
import { STYLES } from './styles'
import { SIZES } from '../../consts/sizes'

const IMAGE = require('../../assets/icons/searcher/searcher.png')

function Component(props){
    return (
        <Fragment>
            
            <Image
            source={IMAGE}
            style={ [
                STYLES.image,
                props.style,
                {height:120 },
                // height: SIZES.heightOfPost.max },
                props.height && {
                    height:props.height
                }
            ] }
            />
                   
        </Fragment>
    )
}

export default Component