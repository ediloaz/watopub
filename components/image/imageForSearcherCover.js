// React
import React , { Fragment } from 'react';

// React native
import { Dimensions , Image } from 'react-native';

// Styles
import { STYLES } from './styles'
import { SIZES } from '../../consts/sizes'

const IMAGE = require('../../assets/images/coverSearcher.png')
const RATIO = Dimensions.get('window').width / 1200
const HEIGHT = 628 * RATIO

function Component(props){
    return (
        <Fragment>
            
            <Image
            source={IMAGE}
            // resizeMode='contain'
            style={ [
                props.style,
                {
                width: '100%',
                height: HEIGHT
                },
            ] }
            />
                   
        </Fragment>
    )
}

export default Component