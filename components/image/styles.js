import { StyleSheet } from 'react-native';

export const STYLES = StyleSheet.create({
    containerComponent : {
        justifyContent: 'center',
        alignItems: 'center',
        width:200,
        
    },
    containerTextFieldWithIcon : {
        // direction : 'ltr'
        flexDirection:'column',
        flexWrap:'wrap',
        display:'flex'
    },
    image : {
        width:100, 
        height:100, 
        paddingVertical:11,
        borderRadius:23,
    },
    imageForpost: {
        width:'100%'
        // borderColor:'#111' , 
        // borderWidth:1 , 
        // borderStyle:'solid' , 
        // width:'86%' , 
        // marginHorizontal:'7%' , 
        // marginVertical:10
    },
    
})

