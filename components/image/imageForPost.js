// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text, Image } from 'react-native';

// Material Kit
// import { Button,  } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'
import { SIZES } from '../../consts/sizes'

const HEIGHT =SIZES.post.image.height

function Component(props){
    return (
        <Image
        source={props.source}
        style={ [
            STYLES.image,
            STYLES.imageForpost,
            props.style,
            props.fullwidth && {width:'100%' },
            // height: SIZES.heightOfPost.max },
            {
                height:HEIGHT
            }
        ] }
        />
    )
}

export default Component