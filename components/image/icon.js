// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text, Image } from 'react-native';

// Material Kit
// import { Button,  } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'

// const IMAGE = 

function Component(props){
    return (
        <Fragment>
            
            <Image
            source={props.source}
            style={ [
                STYLES.image,
                props.style,
                props.size && {
                    width:props.size,
                    height:props.size
                }
            ] }
            />
                   
        </Fragment>
    )
}

export default Component