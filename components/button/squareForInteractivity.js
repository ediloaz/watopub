// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text } from 'react-native';

// Watopub components
import Icon from '../image/icon'

// Styles
import { STYLES } from './styles'



function Component(props){

    function TextBottom(){
        return(
            <Fragment>
                <Text
                style={STYLES.textSquare}
                >{props.text}</Text>
            </Fragment>
        )
    }
    return (
        <Fragment>
            <View
            style={[
                STYLES.circleContainerComponent,
                props.styleContainer,
            ]}
            >
                <TouchableOpacity
                style={ [
                    STYLES.squareButton,
                    props.styleButton,
                    props.size && {
                        width:props.size,
                        height:props.size
                    }
                ] }
                onPress={props.onPress}
                >
                    <Icon
                    source={props.source}
                    size={props.sizeIcon || props.size}
                    />
                {
                props.text
                && <TextBottom />
                }
                </TouchableOpacity>
            </View>
        </Fragment>
    )
}

export default Component