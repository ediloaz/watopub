import { StyleSheet } from 'react-native';


const FONT_SIZE_3 = 16
const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    containerComponent : {
        justifyContent: 'center',
        alignItems: 'center',
        // width:200,
    },
    containerTextFieldWithIcon : {
        // direction : 'ltr'
        flexDirection:'column',
        flexWrap:'wrap',
        display:'flex'
    },
    button : {
        width:'100%',
        paddingVertical:11,
        borderRadius:23,
    },
    circleContainerComponent : {
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleButton:{
        borderRadius:100,
        justifyContent:'center',
        alignItems:'center'
    },
    squareButton:{
        alignItems:'center',
        borderRadius:0,
    },
    barProgress : {
        marginVertical:7,
        marginHorizontal:25,
    },
    text : {
        fontSize : FONT_SIZE_3,
        textAlign:'center',
        fontWeight:'bold',
        paddingHorizontal:10,
    },
    textCirle : {
        fontSize : FONT_SIZE_5,
        textAlign:'center',
        fontWeight:'bold',
        marginTop:5,
    },
    textSquare : {
        fontSize : FONT_SIZE_4,
        textAlign:'center',
        fontWeight:'bold',
        marginTop:5,
    } 
})

