// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text } from 'react-native';

// Progress
import { Bar } from 'react-native-progress';


// Material Kit
// import { Button,  } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'


function Component(props){

    return (
        <Fragment>
            <View
            style={[
                STYLES.containerComponent,
                props.styleContainer,
            ]}
            >
                <TouchableOpacity
                style={ [
                    STYLES.button,
                    props.styleButton,
                ] }
                onPress={props.onPress}
                onLongPress={props.onLongPress}
                delayLongPress={3000}
                // disabled={true}
                >
                    {
                        props.isLoading
                        ? <Bar size={30} indeterminate={true} 
                        style={ [
                            STYLES.barProgress,
                            props.styleText,
                        ] }
                        color='#AAA'
                        />
                        : <Text
                        style={ [
                            STYLES.text,
                            props.styleText,
                        ] }
                        >
                            {props.text || 'Accionar'}
                        </Text>
                    }
                    
                    
                    
                </TouchableOpacity>
                    
            </View>
        </Fragment>
    )
}

export default Component