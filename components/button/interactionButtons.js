// React
import React , { useState , useEffect  } from 'react';

// Prop types
import PropTypes from 'prop-types'

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import Icon from '../image/icon'
import CircleButton from './circle'

// Firestore
import {
    UpdateControl,
    CheckControl,
} from '../../firebase/firestore/posts'

// Utilities
import {
    SharePost
} from '../../utils/utilities'

// Styles
import { STYLES } from '../product/styles'

const IMAGE_SHARE = require('../../assets/icons/share.png')
const IMAGE_CONTROL_BUTTON_CHECKED = require('../../assets/icons/controlWhite.png')
const IMAGE_CONTROL_BUTTON = require('../../assets/icons/control.png')
const IMAGE_CONTROL_TEXT_CHECKED = require('../../assets/icons/controlWhite.png')
const IMAGE_CONTROL_TEXT = require('../../assets/icons/control.png')

const SIZE_BUTTON = 40
const SIZE_BUTTON_ICON = 40


Component.propTypes = {
    /**
     * Indicates if control pressed for the current user
    */
    controlPressed : PropTypes.bool.isRequired,
    OnShare : PropTypes.func.isRequired,
    onControl : PropTypes.func.isRequired,
    controls : PropTypes.number.isRequired,
    description : PropTypes.string.isRequired,
    userPost : PropTypes.string.isRequired,
    keyPost : PropTypes.string.isRequired,
}

function Component(props){

    // const { 
    //     controlPressed
    //  } = props

    const [ controlPressed , setControlPressed ] = useState(false)
    const [ controls , setControls ] = useState(0)
    

    // useLayoutEffect( _ => {
    //     OnControl()
    // },[props.controlPressedIntermetent])

    useEffect( _ => {
        // CheckControl({
        //     key : '1234567891'
        // })
        // .then( r => {
        //     setControlPressed(true)
        // }).catch( e => {
        //     setControlPressed(false)
        // })
    },[])

    const OnShare = _ => {
        SharePost(props.description, props.businessName)
    }
    const OnControl = _ => {
        if (controlPressed){
            setControlPressed(false)
            setControls(controls-1)
            // const answer = UpdateControl({
            UpdateControl({
                key : '1601048030625-ediloaz6@gmail.com'
            })
            // answer
            .then(
                r => {
                    console.log("Desde la interacción")
                    console.log(r)
                }
            )
            .catch(
                r => {
                    console.log("Desde la interacción - error")
                    console.log(r)
                }
            )
        }else{
            setControlPressed(true)
            setControls(controls+1)
        }
    }

    return (
            <View
            style={[
                STYLES.containerForInteractionButtons, 
                // props.styleContainer,
            ]}
            >
                <Grid>
                    {/* Quantity of controls */}
                    <Col
                    size={2}
                    style={[
                        STYLES.colForInteractionButtons, 
                        STYLES.colForInteractionButtonsControls, 
                    ]}  
                    >
                        <Icon
                        source={controlPressed ? IMAGE_CONTROL_TEXT_CHECKED : IMAGE_CONTROL_TEXT}
                        size={20}
                        style={STYLES.iconForControl}
                        />
                        <Text
                        style={[
                        STYLES.textForControl,
                        controlPressed && STYLES.textForControlPressed,

                        ]}
                        >
                            {controls} {controls===1 ? 'control' : 'controls'}
                        </Text>
                        
                    </Col>

                    {/* Share button */}
                    <Col
                    size={1}
                    style={[
                        STYLES.colForInteractionButtons, 
                    ]}  
                    >
                        <CircleButton 
                        text="Compartir"
                        source={IMAGE_SHARE}
                        size={SIZE_BUTTON}
                        sizeIcon={SIZE_BUTTON_ICON}
                        styleContainer={[
                            STYLES.containerButtonInteractiOnShare,
                        ]}
                        styleButton={[
                            STYLES.buttonInteraction,
                        ]} 
                        onPress={OnShare}
                        />
                    </Col>

                    {/* Control button */}
                    <Col
                    size={1}
                    style={[
                        STYLES.colForInteractionButtons, 
                    ]}  
                    >
                        <CircleButton 
                        text="Control"
                        source={
                            controlPressed ? IMAGE_CONTROL_BUTTON_CHECKED : IMAGE_CONTROL_BUTTON
                        }
                        size={SIZE_BUTTON}
                        sizeIcon={SIZE_BUTTON_ICON}
                        styleContainer={[
                            STYLES.containerButtonInteractionControl,
                        ]}
                        styleButton={[
                            controlPressed ? STYLES.buttonInteraction : STYLES.buttonInteractionUnpressed ,
                        ]} 
                        onPress={OnControl}
                        />
                    </Col>
                        

                </Grid>


               
            </View>
    )
}

export default Component