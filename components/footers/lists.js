// React
import React , {} from 'react';

// React native
import { View , Text } from 'react-native';

// Styles
import { STYLES } from './styles'

function Component(props){

    const MESSAGE = props.origin==='posts' ? 'todas las publicaciones' : props.origin==='products' ? 'todos los productos' : props.origin==='search' ? 'todos los resultados' : 'todo'

    return (
        <View>
            <Text
            style={STYLES.textForFlatlistFooter}
            >
                Has visto {MESSAGE} 🙌🏽
            </Text>
        </View>
    )
}

export default Component