import * as React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from "../../consts/colors";

const FONT_SIZE_3 = 16
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    textForFlatlistFooter : {
        fontSize:FONT_SIZE_3,
        textAlign:'center',
        marginTop:60,
        marginBottom:90,
        color:'#888'
    }
})

