import { StyleSheet , Dimensions } from 'react-native';

import { SIZES } from '../../consts/sizes'
import { CONFIG } from '../../consts/config'

export const STYLES = StyleSheet.create({
    container:{
        backgroundColor:'#00000099',
        position:'absolute',
        zIndex:1,
        elevation:1,
        flex:1,
        height:Dimensions.get('window').height,
        width:Dimensions.get('window').width,
    },
    containerLoading: {
        // backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    lottieLoading: {
        
    },
    lottieRocket: {
        width:300,
        // height:300,
    },
    textLoading : {
        fontWeight:'bold',
        color:'white',
        marginTop:90,
    },
    
})

