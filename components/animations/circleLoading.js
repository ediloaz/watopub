// React
import React , { useRef, useEffect } from 'react';

// React native
import { View , Text , StyleSheet ,Button } from 'react-native';

// Lottie
import LottieView from 'lottie-react-native';

// Styles
import { STYLES } from './styles'
import { SIZES } from '../../consts/sizes'

// Consts
// const DURATION = 300

const ANIMATION = require('../../assets/lotties/196-material-wave-loading.json')

export default function Component(props) {
    // componentDidMount() {
    //   this.animation.play();
    //   // Or set a specific startFrame and endFrame with:
    //   // this.animation.play(30, 120);
    // }
  
    // resetAnimation = () => {
    //   this.animation.reset();
    //   this.animation.play();
    // };
  
      return (
        <View style={STYLES.containerLoading}>
          <LottieView
            style={STYLES.lottieLoading}
            // ref={animation => {
            //   this.animation = animation;
            // }}
            // autoSize
            autoPlay
            loop
            source={ ANIMATION }
          />
          {props.text && <Text style={[STYLES.textLoading , props.styleText ]}>{props.text || "Cargando"}</Text>}
        </View>
      );
  }
  

// function Component(props){

//     const animation = useRef( null ).current;

//     useEffect(_ => {
//         animation.current.play();
//     },[])

//     return (
//         <View>
//             <Text>Lottie inicio</Text>
//             <LottieView
//             // autoPlay
//             // loop
//             ref={animation}
//             // ref={animation => {
//             //     this.animation = animation;
//             // }}
            
//             style={{
//                 width: 400,
//                 height: 400,
//                 backgroundColor: '#eee',
//             }}
//             source={ require('../../assets/lotties/circle-loading.json') }
//             />
//             <Text>Lottie Final</Text>
//         </View>
//     )
// }

// export default Component