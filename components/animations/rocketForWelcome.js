// React
import React , {  } from 'react';

// React native
import { View , Text } from 'react-native';

// Lottie
import LottieView from 'lottie-react-native';

// Styles
import { STYLES } from './styles'

const ANIMATION = require('../../assets/lotties/1712-bms-rocket.json')

function Component(props){

    return (
        <View
        style={[
            STYLES.containerComponent,
            props.styleContainer,
        ]}
        >
            <LottieView
            style={STYLES.lottieRocket}
            // ref={animation => {
            //   this.animation = animation;
            // }}
            autoSize
            autoPlay
            loop
            source={ ANIMATION }
        />
            {/* <Image
            style={ [
                STYLES.image,
                props.width && {
                    width:props.width,
                    height:props.width
                }
            ] }
            source={IMAGE}
            /> */}
                
        </View>
    )
}

export default Component