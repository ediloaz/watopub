// React
import React , { Fragment, useState, useRef, useEffect } from 'react';

// React native
import { View, Text , TouchableOpacity, Animated } from 'react-native';

import CircleAnimation from '../animations/circleLoading'

// Styles
import { STYLES } from './styles'

// Consts
import { SIZES } from '../../consts/sizes'

// Consts
const DURATION = 300

function Component(props){

    const [show,setShow] = useState(false)
    const opacity = useRef(new Animated.Value(0.1)).current;

    useEffect(_ => {
        props.show ? Begin() : End()
    },[props.show])
    
    const Begin = _ => {
        // setShow(true)
        const TO_VALUE = 1
        Animated.timing(opacity, {
            toValue:TO_VALUE,
            duration:DURATION,
            useNativeDriver:true
        }).start()
        setTimeout( _ => opacity.setValue(TO_VALUE), DURATION*3 )
    }

    const End = _ => {
        // setShow(false)
        const TO_VALUE = 0.1
        Animated.timing(opacity, {
            toValue:TO_VALUE,
            duration:DURATION,
            useNativeDriver:true
        }).start()
        setTimeout( _ => opacity.setValue(TO_VALUE), DURATION*3 )
    }

    function AnimationLoading(){
        return (
            <Text>Cargando</Text>
        )
    }

    return (
        props.show
        ?
        <Animated.View
        on
        style={[
            { 
            opacity: opacity,
            // display: props.show ? 'flex' :'none'
             },
            // opacity._value>0.5 ? {display:'none'} : {display:'flex'} ,
            STYLES.container
        ]}
        >
            <CircleAnimation 
            text={props.text || "Cargando"}
            />
            {/* <TouchableOpacity
            onPress={props.onClick}
            >
                
                <Text>CARGANDO</Text>
            </TouchableOpacity> */}
            
            { props.animationLoading && <AnimationLoading /> }
        </Animated.View>
        :
        <Fragment />
        // <View
        // style={[
        // STYLES.container,
        // {opacity:opacity}
        // ]}
        // >
        // </View>
                   
    )
}

export default Component