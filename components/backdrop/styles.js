import { StyleSheet , Dimensions } from 'react-native';

import { SIZES } from '../../consts/sizes'
import { CONFIG } from '../../consts/config'

export const STYLES = StyleSheet.create({
    container:{

        backgroundColor:'#00000099',
        position:'absolute',
        zIndex:1,
        elevation:1,
        flex:1,
        height:Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        
        
    },
    
})

