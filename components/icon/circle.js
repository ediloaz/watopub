// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text } from 'react-native';

// Watopub components
import Icon from '../image/icon'

// Styles
import { STYLES } from './styles'



function Component(props){

    function TextBottom(){
        return(
            <Text
            style={STYLES.textCirle}
            >{props.text}</Text>
        )
    }
    return (
        <View
        style={[
            STYLES.circleContainerComponent,
            props.styleContainer,
        ]}
        >
            <View
            style={ [
                STYLES.circleButton,
                props.styleButton,
                props.size && {
                    width:props.size,
                    height:props.size
                }
            ] }
            // onPress={props.onPress}
            >
                <Icon
                source={props.source}
                size={props.size}
                style={[
                    props.styleIcon,
                    // {bor}
                    !props.notCircle && {borderRadius:200}
                ]}
                />
            </View>
            {
            props.text
            && <TextBottom />
            }
        </View>
    )
}

export default Component