// import React from 'react';
// import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';

import { StyleSheet } from 'react-native';
import { COLORS } from '../../consts/colors'
import { STYLES_SHARED } from '../../consts/styles'

const FONT_SIZE_3 = 16

export const STYLES = StyleSheet.create({
    containerComponent : {
        backgroundColor: COLORS.red.default ,           // Must in Props
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center',
        minWidth:120,
        paddingHorizontal:20,
        flexDirection: 'row'
    },
    // buttonTopPosition:STYLES_SHARED.buttonTopPosition,
    icon : {
        marginRight:15
    },
    text : {
        color:'#FFF',                                   // Must in Props
        width:'100%',
        paddingVertical:11,
        paddingHorizontal:10,
        borderRadius:23,
        fontSize : FONT_SIZE_3,
        textAlign:'center',
        // fontWeight:'bold',
        // fontFamily: Font 'BerkshireSwash-Regular' 
        // Script MT
        // Script MT
        // Script MT
        // Script MT
    },
    containerComponentColorInverted:{
        backgroundColor:'white',
        color:'black',
        fontWeight:'bold'
    },
})

