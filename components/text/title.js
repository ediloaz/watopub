// React
import React , { Fragment } from 'react';

// React native
import { TouchableOpacity, View, Text } from 'react-native';

// Watopub components
import Icon from '../image/icon'

// Styles
import { STYLES } from './styles'



function Component(props){

    return (
        <Fragment>
            <View
            style={[
                STYLES.containerComponent,
                STYLES.buttonTopPosition,
                props.styleContainer,
                props.colorInverted && STYLES.containerComponentColorInverted,
            ]}
            >
                <Text
                style={ [
                    STYLES.text,
                    props.styleText,
                    props.width && {
                        width:props.width,
                    },
                    props.colorInverted && STYLES.containerComponentColorInverted,
                ] }
                >
                    {props.text || 'Título estándar'}
                </Text>
                {props.source
                &&
                <Icon
                source={props.source}
                size={props.sizeIcon || props.size || 40}
                style={STYLES.icon}
                />
                }
                    
            </View>
        </Fragment>
    )
}

export default Component