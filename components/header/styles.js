import { StyleSheet } from 'react-native';
import { COLORS } from '../../consts/colors'
import { STYLES_SHARED } from '../../consts/styles'
import { Constants } from 'expo';


const FONT_SIZE_3 = 16

export const STYLES = StyleSheet.create({
    containerComponent : {
        backgroundColor: '#FFF' ,           // Must in Props
        // backgroundColor: '#FFD' ,           // Must in Props
        width:'100%',
        height:70,
    },
    containerComponentForSearcher : {
        backgroundColor: COLORS.red.default ,           // Must in Props
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        minWidth:220,
        paddingHorizontal:20,
        flexDirection: 'row',
        marginTop:12,
        width:'90%',
        marginHorizontal:'5%',
        
    },
    buttonTopPosition:STYLES_SHARED.buttonTopPosition,
    icon : {
        marginRight:15
    },
    text : {
        color:'#FFF',                                   // Must in Props
        width:'100%',
        paddingVertical:11,
        paddingHorizontal:10,
        borderRadius:23,
        fontSize : FONT_SIZE_3,
        textAlign:'center',
        // fontWeight:'bold',
    },
    textInputForSearcher : {
        color:'#FFF' ,
        textAlign:'center' ,
        fontWeight: 'bold' ,
        marginLeft:30,
    },
    textInputContainerForSearcher : {
        backgroundColor: COLORS.red.default,
        borderColor: COLORS.red.default,
        width:'100%',
        paddingVertical:11,
        paddingHorizontal:10,
        borderRadius:23,
        fontSize : FONT_SIZE_3,
        textAlign:'center',
        // fontWeight:'bold',
    },

    verticalCenter:{
        justifyContent:'center'
    },

    containerCircleButtonGoToBack : { 
        alignSelf:'flex-start',
        marginLeft:17,
    },

    invertColorsContainer:{
        backgroundColor:'white'
    },
    invertColorsText : {
        color:'black'
    },
    containerTitleHome : {
        marginLeft:-10,
        backgroundColor:'white'
    },
    textTitleHome : {
        color:COLORS.red.default,
        textAlign: 'left',
        fontSize:24,
    },
    // containerProfilePicture:{
    //     // margin:0
    //     // borderRadius:100,
    //     // width:10,
    //     // height:10,
    // },
    // buttonProfilePicture:{
    //     borderRadius:100,
    //     width:10,
    //     // height:10,
    // },
    // iconProfilePicture:{
    //     // borderRadius:100,
    //     width:10,
    //     // height:10,
    // },
})

