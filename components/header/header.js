// React
import React , { useState, useEffect } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { TouchableOpacity, View, Text } from 'react-native';

import PropTypes from 'prop-types';

// React Native Easy Grid
import { Grid, Row, Col  } from "react-native-easy-grid";

// Watopub components
// import Icon from '../image/icon'
import Title from '../text/title'
import SquareButton from '../button/square'
import CircleButton from '../button/circle'
import Icon from '../icon/circle'
import TextInput from '../textInput/textInput'

// Consts
import { SIZES } from '../../consts/sizes'
import { COLLECTION_NAMES } from '../../firebase/consts'

// Store of redux
import store from '../../redux/store'

// Firestore
import {
    GetImage
} from '../../firebase/firestore'

// Styles
import { STYLES } from './styles'



// Images and resources
const IMAGE_GO_TO_BACK = require('../../assets/icons/navigation/goToBack.png')
const IMAGE_GO_TO_BACK_WHITE = require('../../assets/icons/navigation/goToBackWhite.png')
const LOGO_GO_TO_INTERACTIVITY = require('../../assets/icons/discover/red.png')
const LOGO_ADD_POST = require('../../assets/icons/posts/addPost.png')
const LOGO_WATOPUB = require('../../assets/images/logos/background_transparent.png')

Component.propTypes = {
    backAvalaible:PropTypes.bool,
    title:PropTypes.string.isRequired,
}

function Component(props){

    const { navigation } = props
    const [media,setMedia] = useState(null)

    useEffect( _ => {
        if(props.forProfileBusiness){
            GetImage(props.profilePicture, COLLECTION_NAMES.accounts)
            .then(
                r => {
                    setMedia(r)
                }
            )
        }
    },[props.forProfileBusiness])

    const GoToBack = _ => {
        navigation.goBack();
    }

    function ButtonGoToBack(){
        return (
            <CircleButton 
            // source={props.forProfileBusiness ? IMAGE_GO_TO_BACK_WHITE : IMAGE_GO_TO_BACK}
            source={IMAGE_GO_TO_BACK_WHITE}
            size={45}
            styleContainer={STYLES.containerCircleButtonGoToBack} 
            onPress={GoToBack}
            />
        )
    }

    function ForHome(){
        return (
            <Grid>
                <Row>
                    <Col size={5} style={[ STYLES.verticalCenter ]} >
                        <Title 
                            text="Watopub"
                            // source={LOGO_WATOPUB}
                            // logoPosition='right'
                            // colorInverted
                            styleContainer={STYLES.containerTitleHome}
                            styleText={STYLES.textTitleHome}
                        />
                    </Col>
                    <Col size={1} style={[STYLES.verticalCenter ]} >
                        <SquareButton
                        sizeIcon={SIZES.squareIconSizeForTitlePosts}
                        source={LOGO_GO_TO_INTERACTIVITY}
                        styleContainer={STYLES.containerSquareButtonInTitleInInitialPosts} 
                        onPress={props.GoToInteractivyBoxes}
                        />
                    </Col>
                    <Col size={1} style={[STYLES.verticalCenter ]} >
                        <SquareButton
                        sizeIcon={SIZES.squareIconSizeForTitlePosts}
                        source={LOGO_ADD_POST}
                        styleContainer={STYLES.containerSquareButtonInTitleInInitialPosts} 
                        onPress={props.GoToAddPost}
                        />
                    </Col>
                </Row>
            </Grid>
        )
    }

    function ForHomeBusiness(){
        return (
            <Grid>
                <Row>
                    <Col size={3} style={[ STYLES.verticalCenter ]} >
                        <Title 
                            text="Watopub"
                            source={LOGO_WATOPUB}
                            logoPosition='right'
                        />
                    </Col>
                    <Col size={1} style={[STYLES.verticalCenter ]} >
                        <SquareButton
                        sizeIcon={SIZES.squareIconSizeForTitlePosts}
                        source={LOGO_ADD_POST}
                        styleContainer={STYLES.containerSquareButtonInTitleInInitialPosts} 
                        onPress={props.GoToAddPost}
                        />
                    </Col>
                </Row>
            </Grid>
        )
    }
    
    function ForProfileBusiness(){
        return (
            <Grid>
                <Row>
                    <Col size={1} style={[STYLES.verticalCenter ]} >
                        {
                        !props.own
                        &&
                        // props.own
                        // &&
                        // ?
                        // <Icon 
                        // source={media ? {uri:media} : IMAGE_GO_TO_BACK}
                        // size={45}
                        // />
                        // :
                        <ButtonGoToBack />
                        }
                    </Col>
                    <Col size={3} style={[ STYLES.verticalCenter ]} >
                        <Title
                        styleContainer={ STYLES.invertColorsContainer }
                        styleText={ STYLES.invertColorsText }
                        text={props.name || "Negocio"}
                        />
                    </Col>
                    <Col size={1} style={[STYLES.verticalCenter ]} >
                    {
                        // props.own
                        // &&
                        // ?
                        // <SquareButton
                        // sizeIcon={SIZES.squareIconSizeForTitlePosts}
                        // source={LOGO_ADD_POST}
                        // styleContainer={STYLES.containerSquareButtonInTitleInInitialPosts} 
                        // onPress={props.GoToAddPost}
                        // />
                        // :
                        <Icon 
                        source={media ? {uri:media} : IMAGE_GO_TO_BACK}
                        size={45}
                        />
                        }
                    </Col>
                </Row>
            </Grid>
        )
    }

    function ForSearcher(){
        return (
            <View
            style={[
                STYLES.containerComponentForSearcher,
                props.styleContainer,
            ]}
            >
                <TextInput 
                value={props.stringToSearch}
                placeholder="Buscador"
                // textOut={typeAccount==='user' ? "Nombre y apellidos" : "Nombre del negocio" }
                styleParent={STYLES.textInputContainerForSearcher}
                styleTextInput={STYLES.textInputForSearcher}
                // LeftIcon={ <FontAwesome5 name="at" size={24} color={'#FFF'} /> }
                OnChangeParent={props.Search}
                inDirect
                />
            </View>
        )
    }

    function Standard(){
        return (
            <Grid>
                <Row>
                    <Col size={1}  style={[ STYLES.verticalCenter ]}>
                        {
                            props.backAvalaible
                            &&
                            <ButtonGoToBack />
                        }
                    </Col>
                    <Col size={3} style={[ STYLES.verticalCenter ]} >
                        <Title 
                        text={props.title}
                        />
                    </Col>
                    <Col size={1} ></Col>
                </Row>
            </Grid>
        )
    }

    function Content(){
        return (
            props.forHome
            ?
            <ForHome />
            :
            props.forHomeBusiness
            ?
            <ForHomeBusiness />
            :
            props.forSearcher
            ?
            <ForSearcher />
            :
            props.forProfileBusiness
            ?
            <ForProfileBusiness />
            :
            <Standard />
        )
    }

    return (
        <View
        style={[
            STYLES.containerComponent,
            props.styleContainer,
            props.forProfileBusiness && {borderBottomRightRadius:50}
        ]}
        >
            <Content />
        </View>
    )
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
    }
}
  
const mapDispatchToProps = { }
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Component)
export default component
