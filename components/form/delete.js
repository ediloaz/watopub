// React
import React , { useState } from 'react';

// React native
import { Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Grid } from "react-native-easy-grid";

// Firestore
import {
    DeleteItem
} from '../../firebase/firestore'

// Utilities
import {
    UpperCaseFirstLetter,
    ResetNavigation,
} from '../../utils/utilities'

// Watopub components
import Button from '../button/button'
import Backdrop from '../backdrop/loadingFullScreen'


// Consts
import {
    CONFIG
} from '../../consts/config'

// Styles
import { STYLES } from './styles'

function Component(props){

    const [inProgress,setInProgress] = useState(false)

    const { navigation } = props

    console.log(`3 : ${props.keyPost}`)

    const GoToBack = _ => {
        navigation.goBack();
    }

    const GoToHomeAfterDelete = _ => {
        ResetNavigation(navigation,'HomeBusiness')
    }

    const Affirm = _ => {
        
        const Successful = _ => {
            Alert.alert(
                `¡${UpperCaseFirstLetter(props.type) || "Item"} eliminado! 👏🏽`,
                "Tu publicación fue eliminada con éxito, ve a la pantalla de inicio",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => GoToHomeAfterDelete()
                    }
                ]
            )
        }
        const DeleteConfirmed = _ => {
            // Only has two items to delete: Products and Posts
            const TYPE = props.type === 'producto' ? 'products' : 'posts'
            setInProgress(true)
            setTimeout( _ => {
                DeleteItem({key:props.keyPost},TYPE)
                .then(
                    r => {
                        setInProgress(false)
                        Successful()
                    }
                )
                .catch(
                    e => {
                        setInProgress(false)
                        alert('Hubo un pequeño error, lo sentimos 😔 \n\n Intenta más tarde')
                    }
                )
            },CONFIG.waitForWaiting)
        }
        Alert.alert(
            '¡Última confirmación! 🚫',
            `¿Desea eliminar por completo su ${props.type || "item"}?`,
            [
                {
                    text:'Aceptar y borrar',
                    onPress: DeleteConfirmed
                },
                {
                    text:'Cancelar y volver',
                    style:'cancel',
                    onPress:GoToBack
                }
            ]
        )
    }

    function AffirmButton(){
        return (
            <Button 
            text="Sí"
            styleButton={[STYLES.affirmButton,STYLES.deleteButton]} 
            styleText={[STYLES.textDeleteButton,STYLES.textAffirmDeleteButton]} 
            onPress={Affirm}
            />
        )
    }

    const Deny = _ => {
        GoToBack()
    }

    function DenyButton(){
        return (
            <Button 
            text="No"
            styleButton={[STYLES.denyButton,STYLES.deleteButton]} 
            styleText={[STYLES.textDeleteButton,STYLES.textDenyDeleteButton]} 
            onPress={Deny}
            />
        )
    }

    

    return (
            <View
            style={[
                STYLES.containerComponent,
                STYLES.containerComponentDelete,
                props.styleContainer,
            ]}
            >
                <Backdrop 
                show={inProgress}
                animationLoading
                text="Borrando tu publicación"
                />
                <Text style={[STYLES.textQuestionDelete, STYLES.separator]} >
                    ¿Desea eliminar su {props.type || "item"}?
                </Text>
                <Grid style={STYLES.gridForDeleteButtons} >
                    <Col
                    size={1}
                    style={[
                    STYLES.colForDeleteButtons, 
                    ]}  
                    >
                        <AffirmButton />
                    </Col>
                    <Col
                    size={1}
                    style={[
                    STYLES.colForDeleteButtons, 
                    ]}  
                    >
                        <DenyButton />
                    </Col>

                </Grid>
            </View>
    )
}

export default Component