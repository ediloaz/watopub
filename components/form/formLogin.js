// React
import React , { Fragment , useState } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { Text, View, Alert,  } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import MailInput from '../textInput/mailInput'
import PasswordInput from '../textInput/passwordInput'
import Button from '../button/button'

// Styles
import { STYLES } from './styles'

// Firestore
import { 
    LogIn,
    GetDataOfCurrentAccount,
} from '../../firebase/firestore/accounts'

// Utilities
import {
    validateEmail,
    ResetNavigation,
} from '../../utils/utilities'

// Redux actions
import { LoginAccount } from '../../redux/actions/account'


const FORM_DATA_STRUCTURE = {
    email : '',
    password : '',
}

function Component(props){

    const [inProgress,setInProgress] = useState(false)
    const [formData, setFormData] = useState(FORM_DATA_STRUCTURE)

    const { navigation } = props

    const OnChange = (value,variable) => {
        setFormData({ ...formData , [variable]:value })
        setTimeout(_ => {
            console.log(formData)
        },500)
    }


    // const  NavegationToHomeUser = _ => ResetNavigation(navigation , 'HomeUser')
    // const  NavegationToHomeBusiness = _ => ResetNavigation(navigation , 'HomeBusiness')

    const GoToWelcome = _ => {
        props.GoToWelcome()
    }

    const LoginSuccessful = _ => {

        function WriteInReduxAccountData(dataAccount){
            props.LoginAccount({
                dataAccount
            })
            GoToWelcome()
            // dataAccount.accountType==='user'
            // ?
            // NavegationToHomeUser()
            // :
            // NavegationToHomeBusiness()
        }
        async function UserConnected(){
            GetDataOfCurrentAccount()
            .then(
                r => {
                    WriteInReduxAccountData(r)
                }
            )
            .catch(
                e => {
                }
            )
            
        }
        setInProgress(false)
        UserConnected()
        // GoToHome()

    }

    const LoginFail = _ => {
        Alert.alert(
            '¡Datos incorrectos! 🚫',
            "Intenta nuevamente, el correo electrónico y/o contraseña son incorrectos",
            [
                {
                    text:'Aceptar',
                    onPress: _ => setInProgress(false)
                }
            ]
        )
    }

    const Login = async _ => {
        setInProgress(true)
        const checkForm = CheckForm() 
        if (checkForm===true){
            await LogIn(formData)
            .then(r => {
                r
                ? LoginSuccessful()
                : LoginFail()
            })
        }
    }

    const CheckForm = _ => {
        let condition = true
        let messageToAlert = '\n'
        let countErrors = 0

        if (formData.password.length === 0){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El campo de contraseña está vacío')
        }else if (formData.password.length < 6){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - Longitud de contraseña debe ser mayor a seis (6)')
        }
        if (formData.email.length === 0){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El campo de correo electrónico está vacío')
        }else if (!validateEmail(formData.email)){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El correo electrónico tiene un formato inválido')
        }
        if (countErrors>0){
            setInProgress(false)
            Alert.alert(
                '¡Formato incorrecto! 🚫',
                countErrors===1 ? 'Revise el siguiente punto:\n'.concat(messageToAlert) : 'Revise las siguientes puntos:\n'.concat(messageToAlert),
                [
                    {
                        text:'Aceptar y corregir'
                    }
                ]
            )
        }
        return condition
    }

    const FillExample = _ => {
        
        setFormData({
            email:props.accountType==='user' ? 'ediloaz6@gmail.com' : 'w@w.com',
            password:'aabbcc'
        })
    }

    function ButtonLogin(){
        return (
            <Fragment>
                <Button 
                text="Iniciar sesión"
                styleButton={STYLES.buttonLogin} 
                styleText={STYLES.textButtonLogin} 
                onPress={Login}
                onLongPress={FillExample}
                isLoading={inProgress}
                />
            </Fragment>
        )
    }

    

    return (
            <View
            style={[
                STYLES.containerComponent,
                props.styleContainer,
            ]}
            >
                <Text style={[STYLES.textTitle, STYLES.separator]} >{props.accountTypeMessage}</Text>
                <MailInput 
                value={formData.email}
                style={STYLES.separator} 
                OnChangeParent={OnChange}
                OnChangeVariable="email"
                />
                <PasswordInput 
                value={formData.password}
                OnChangeParent={OnChange}
                OnChangeVariable="password"
                />
                <ButtonLogin />
            </View>
    )
}

const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
}
  
const mapDispatchToProps = { 
    LoginAccount
}
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Component)
export default component