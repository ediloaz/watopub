// React
import React , { useState, useEffect } from 'react';

// React native
import { View, Alert } from 'react-native';

// Image picker
import * as ImagePicker from "expo-image-picker"

import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


// Watopub components
import TextInput from '../textInput/textInput'
import Button from '../button/button'
import Title from '../text/title'
import Media from '../media/forPost'
import Backdrop from '../backdrop/loadingFullScreen'

// Styles
import { STYLES } from './styles'

// Firestore
import { 
    UpdateBusinessInformation as Update
} from '../../firebase/firestore/accounts'
import { 
    GetImage
} from '../../firebase/firestore'

// Consts
import {
    SIZES
} from '../../consts/sizes'
import {
    COLLECTION_NAMES
} from '../../firebase/consts'


function Component(props){

    const [inProgress,setInProgress] = useState(false)
    const [information, setInformation] = useState(props.navigation.state.params.information)
    const [media,setMedia] = useState(null)

    const { navigation } = props

    useEffect( _ => {
        console.log("vamos a traer este:")
        console.log(props.navigation.state.params.media)
        GetImage(props.navigation.state.params.media,COLLECTION_NAMES.accounts)
        .then( r => setMedia(r) )
    },[props.navigation.state.params.media])

    const OnChange = (value) => {
        setInformation(value)
    }

    const GoToIndex = () => {
        navigation.replace('Index')
    }

    
    useEffect( _ => {
        const getPermissionAsync = async () => {
            if (Constants.platform.ios) {
              const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
              }
            }
          };
        getPermissionAsync();
    },[])

  
    ChooseImage = async() => { 
        try{
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                quality: SIZES.profilePicture.quality,
                aspect:[ SIZES.profilePicture.aspect.w , SIZES.profilePicture.aspect.h ]
            })
            if (!result.cancelled) {
                setMedia(result.uri)
            }else{
                alert("Cancelaste la subida")
            }
        } catch (e) {
            console.log("Error seleccionando el archivo a subir");
            console.log(e);
        }
    }

    
    const Upload = _ => {
        const Successful = _ => {
            Alert.alert(
                'Cambios realizados! 💙',
                "Tu perfil está actualizado con los cambios que has aplicado, ve a verlo",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => GoToIndex()
                    }
                ]
            )
        }
        const NoMediaWarning = _ => {
            Alert.alert(
                'No cargaste imagen de perfil 🚫',
                "¿Está seguro que desea continuar sin ésta?",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => UpdateChanges()
                    },
                    {
                        text:'Cancelar',
                        style:'cancel'
                    }
                ]
            )
        }
        const EmptyDescription = _ => {
            Alert.alert(
                'Información vacía 🚫',
                `Dígite al menos algún texto en el campo de información`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const UpdateChanges = _ => {
            setInProgress(true)
            Update({
                information,
                media,
            }).then(
                r => {
                    setInProgress(false)
                    Successful()
                }
            ).catch(
                e => {
                    setInProgress(false)
                    alert('Hubo un pequeño error, lo sentimos 😔 \n\n Intenta más tarde')
                }
            )
        }
        
        // Check before upload the post
        if (information.replace(' ','') .length<1){
            EmptyDescription()
        }else if (media===null){
            NoMediaWarning()
        }else{
            setInProgress(true)
            UpdateChanges()
        }
    }


    function MediaVisualization(){
        return (
            <Media
            source={{uri:media}}
            mediaType={'image'}
            styleContainer={STYLES.mediaContainerForUpdateProfile}
            styleMedia={STYLES.mediaForUpdateProfile}
            />
        )
    }

    function UploadButton(){
        return (
            <Button 
            text="Cambiar imagen de perfil"
            styleButton={[
            STYLES.uploadImageButton,
            STYLES.uploadImageButtonForEditProfile,
            ]} 
            styleText={[
            STYLES.uploadImageText,
            STYLES.uploadImageTextForEditProfile,
            ]} 
            onPress={ChooseImage}
            />
        )
    }

    function DescriptionInput(){
        return (
            <TextInput
            value={information}
            multiline
            placeholder="Información del producto"
            styleParent={{width:'86%',marginHorizontal:'7%' , marginVertical:10 , height:150}}
            OnChangeParent={OnChange}
            OnChangeVariable={'information'}
            />
        )
    }
    
    function PostButton(){
        return (
            <Button 
            text="Publicar"
            styleButton={[STYLES.postButton]} 
            styleText={[STYLES.uploadImageText]} 
            onPress={Upload}
            />
        )
    }
    
    return (
            <View
            style={[
            STYLES.containerComponent,
            STYLES.containerComponentAddPost,
            props.styleContainer,
            ]}
            >
                <Backdrop 
                show={inProgress}
                animationLoading
                text="Actualizando tu perfil"
                />

                <MediaVisualization />

                <UploadButton />

                <DescriptionInput />    

                <PostButton />
                
            </View>
    )
}

export default Component