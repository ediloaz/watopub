import * as React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from "../../consts/colors";
import { SIZES } from "../../consts/sizes";

const FONT_SIZE_3 = 16
const FONT_SIZE_5 = 12

const HEIGHT_DELETE_BUTTONS = 180 

export const STYLES = StyleSheet.create({
    containerComponent : {
        color:'#FFF',
        width:'100%',
        flexDirection:'column',
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        
    },
    textTitle : {
        fontWeight: 'bold',
        color:'#FFF',
        textAlign:"center",
        fontSize:FONT_SIZE_3,
    },
    backgroundWhite : {
        backgroundColor: '#FFF',
        borderColor: '#555'
    },
    textInputRoot : {
        borderBottomWidth: 0
    },
    separator : {
        marginBottom:20,
    },
    picker : {
        // marginTop:-20,
        marginBottom:30,
        width:'85%',
        marginHorizontal:'7.5%',
    },
    conditionsAndTerms : {
        marginTop:10,
        flexDirection:'row',
        flexWrap:'wrap',
    },
    textLink : {
        color:COLORS.yellow.default,
    },
    textConditionsAndTerms : {
        textAlign:'center',
        color:'#FFF',
        fontSize:FONT_SIZE_5,
    },
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
        width:'80%',
        minWidth:240,
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        // marginLeft:95,
        
    },
    textButtonLogin : {
        color:'#FFF',
    },
    containerButtonRegister : {
        width:'70%',
        marginHorizontal:'15%',
        marginTop:150,
        // marginBottom:30,
        // position:'absolute'
    },
    containerButtonRegisterFromRegisterScreen : {
        // width:'70%',
        // marginHorizontal:'15%',
        marginTop:10,
        marginBottom:30,
        // marginBottom:30,
        // position:'absolute'
    },
    buttonRegister : {
        backgroundColor:'#FFF',
        marginTop:22,
    },
    textButtonRegister : {
        color:COLORS.blue.default,
    },

    // Delete post
    containerComponentDelete : {
        height: HEIGHT_DELETE_BUTTONS,
    },
    gridForDeleteButtons: {
        height: HEIGHT_DELETE_BUTTONS,
    },
    colForDeleteButtons : {
        justifyContent:'center',
        height:HEIGHT_DELETE_BUTTONS,
        width:'100%',
        textAlign:'center',
        // backgroundColor:'#fa0',
    },
    affirmButton : {
        backgroundColor:COLORS.red.default
    },
    deleteButton : {
        height:40,
        width:80,
    },
    denyButton : {
        borderColor:COLORS.red.default,
        borderStyle:'solid',
        borderWidth:2,
    },
    textDeleteButton: {
        marginBottom:200
    },
    textAffirmDeleteButton: {
        color:'white'
    },
    textDenyDeleteButton: {
        color:'black'
    },
    textQuestionDelete : {
        fontWeight: 'normal',
        color:'black',
        textAlign:"center",
        fontSize:FONT_SIZE_3,
    },

    // Add Post
    uploadImageButton : {
        height:40,
        width:'60%',
        minWidth:260,
        backgroundColor:COLORS.red.default,
        marginTop:5,
        opacity:0.9
    },
    uploadImageButtonForEditProfile : {
        // borderColor:COLORS.red.default,
        // backgroundColor:'white',
        // borderWidth:1,
        marginTop:15,
        marginBottom:35,
        // borderRadius
    },
    postButton : {
        height:40,
        width:'80%',
        backgroundColor:COLORS.red.default,
        marginBottom:20
    },
    uploadImageText : {
        color:'white'
    },
    uploadImageTextForEditProfile : {
        // color:'#555',
    },
    containerComponentAddPost: {
        // alignItems:'center',
        width:'100%',
        textAlign:'center'
    },

    // Profile
    mediaContainerForUpdateProfile:{
        borderRadius:1000,
        height:SIZES.profilePicture.height,
        width: SIZES.profilePicture.width,
        marginHorizontal:SIZES.profilePicture.marginHorizontal,
    },
    mediaForUpdateProfile:{
        borderRadius:1000,
    }
    
})

