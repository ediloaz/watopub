// React
import React , { useState, useEffect } from 'react';

// React native
import { View, Alert } from 'react-native';

// Image picker
import * as ImagePicker from "expo-image-picker"

import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


// Watopub components
import TextInput from '../textInput/textInput'
import Button from '../button/button'
import Title from '../text/title'
import Media from '../media/forPost'
import Backdrop from '../backdrop/loadingFullScreen'

// Styles
import { STYLES } from './styles'

// Firestore
import { 
    UploadProduct
} from '../../firebase/firestore/products'

// Consts
import {
    SIZES
} from '../../consts/sizes'


function Component(props){

    const [inProgress,setInProgress] = useState(false)
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [mediaType,setMediaType] = useState(null)
    const [media,setMedia] = useState(null)
    const [duration,setDuration] = useState(null)

    const { navigation } = props

    const OnChange = (value,variable) => {
        variable==='title'
        ?
        setTitle(value)
        :
        setDescription(value)
    }

    const GoToViewProduct = (dataProduct) => {
        // navigation.replace('ViewProduct',{dataProduct})  // Possible method, but more difficult. In the screen you need search
                                                            // data in DB and download. With a simple query: get the last product uploaded
        navigation.replace('Index')
    }

    
    useEffect( _ => {
        const getPermissionAsync = async () => {
            if (Constants.platform.ios) {
              const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
              }
            }
          };
        getPermissionAsync();
    },[])

  
    ChooseImage = async() => { 
        try{
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                quality: SIZES.mediaForUpload.quality,
                aspect:[ SIZES.post.aspect.w , SIZES.post.aspect.h ]
            })
            if (!result.cancelled) {
                setMediaType(result.type)
                setMedia(result.uri)
                result.type==='video' ? setDuration(result.duration) : setDuration(null)
            }else{
                alert("Cancelaste la subida")
            }
        } catch (e) {
            console.log("Error seleccionando el archivo a subir");
            console.log(e);
        }
    }

    
    const Upload = _ => {
        const Successful = _ => {
            Alert.alert(
                'Producto en línea! 💙',
                "Tu nuevo producto ya está subido, ve a verlo",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => GoToViewProduct()
                    }
                ]
            )
        }
        const NoMediaWarning = _ => {
            Alert.alert(
                'No hay archivo cargado 🚫',
                "¿Está seguro que desea publicar sin ninguna imagen o vídeo?",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => UploadPostConfirmed()
                    },
                    {
                        text:'Cancelar',
                        style:'cancel'
                    }
                ]
            )
        }
        const VideoOutSeconds = _ => {
            Alert.alert(
                'Edite o seleccione otro vídeo 🚫',
                `Este video tiene una duración de ${(duration/1000).toFixed(1)} segundos. \n \n La duración máxima es de ${40} segundos.`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const EmptyTitle = _ => {
            Alert.alert(
                'Título vacío 🚫',
                `Dígite al menos algún texto en el campo de título`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const EmptyDescription = _ => {
            Alert.alert(
                'Descripción vacía 🚫',
                `Dígite al menos algún texto en el campo de descripción`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const UploadPostConfirmed = _ => {
            setInProgress(true)
            UploadProduct({
                title,
                description,
                media,
                mediaType,
            }).then(
                r => {
                    setInProgress(false)
                    Successful()
                }
            ).catch(
                e => {
                    setInProgress(false)
                    alert('Hubo un pequeño error, lo sentimos 😔 \n\n Intenta más tarde')
                }
            )
        }
        
        // Check before upload the post
        if (title.replace(' ','') .length<1){
            EmptyTitle()
        }else if (description.replace(' ','') .length<1){
            EmptyDescription()
        }else if (duration>40000){
            VideoOutSeconds()
        }else if (media===null){
            NoMediaWarning()
        }else{
            setInProgress(true)
            UploadPostConfirmed()
        }
    }


    function MediaVisualization(){
        return (
            <Media 
            source={{uri:media}}
            mediaType={mediaType}
            />
        )
    }

    function UploadButton(){
        return (
            <Button 
            text="Seleccionar imagen o vídeo"
            styleButton={[STYLES.uploadImageButton]} 
            styleText={[STYLES.uploadImageText]} 
            onPress={ChooseImage}
            />
        )
    }

    function HeadTitle(){
        return (
            <Title
            text="Encabezado"
            styleContainer={{width:'45%',marginHorizontal:'7%' , marginVertical:10, marginTop:50}}
            />
        )
    }

    function TitleInput(){
        return (
            <TextInput
            value={title}
            placeholder="Título del producto"
            styleParent={{width:'86%',marginHorizontal:'7%' , marginVertical:10}}
            OnChangeParent={OnChange}
            OnChangeVariable={'title'}
            />
        )
    }

    function DescriptionInput(){
        return (
            <TextInput
            value={description}
            multiline
            placeholder="Información del producto"
            styleParent={{width:'86%',marginHorizontal:'7%' , marginVertical:10 , height:150}}
            OnChangeParent={OnChange}
            OnChangeVariable={'description'}
            />
        )
    }
    
    function PostButton(){
        return (
            <Button 
            text="Publicar"
            styleButton={[STYLES.postButton]} 
            styleText={[STYLES.uploadImageText]} 
            onPress={Upload}
            />
        )
    }
    
    return (
            <View
            style={[
            STYLES.containerComponent,
            STYLES.containerComponentAddPost,
            props.styleContainer,
            ]}
            >
                <Backdrop 
                show={inProgress}
                animationLoading
                text="Subiendo tu producto"
                />

                <MediaVisualization />

                <UploadButton />

                <HeadTitle />

                <TitleInput />

                <DescriptionInput />    

                <PostButton />
                
            </View>
    )
}

export default Component