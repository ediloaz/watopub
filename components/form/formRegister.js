// React
import React , { Fragment, useState, useEffect } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// Expo
import { FontAwesome5 } from '@expo/vector-icons'; 

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Watopub components
import TextInput from '../textInput/textInput'
import Picker from '../picker/picker'
import MailInput from '../textInput/mailInput'
import PasswordInput from '../textInput/passwordInput'
import Button from '../button/button'

// Utilities
import {
    validateEmail, 
    ResetNavigation,
} from '../../utils/utilities'

// Firestore
import{ GetBusinessCategories , ManageResultOfFirestore } from '../../firebase/firestore'
import { 
    RegisterUser as RegisterUserInFirebase,
    RegisterBusiness as RegisterBusinessInFirebase,
    GetDataOfCurrentAccount,
} from '../../firebase/firestore/accounts'

// Redux actions
import { LoginAccount } from '../../redux/actions/account'

// Styles
import { STYLES } from './styles'

const FORM_DATA_STRUCTURE = {
    name : '',
    email : '',
    password : '',
    businessCategory : '',
}

function Component(props){

    const { typeAccount , navigation } = props

    const [inProgress,setInProgress] = useState(false)
    const [formData, setFormData] = useState(FORM_DATA_STRUCTURE)
    const [listOfBusinessCategories, setListOfBusinessCategories] = useState([])

    useEffect( _ => {
        GetBusinessCategories()
        .then(r => { 
            setListOfBusinessCategories(ManageResultOfFirestore(r))
        })
    },[])

    const OnChange = (value,variable) => {
        setFormData({ ...formData , [variable]:value })
        setTimeout(_ => {
            console.log(formData)
        },500)
    }

    const  NavegationToHomeUser = _ => ResetNavigation(navigation , 'HomeUser')
    const  NavegationToHomeBusiness = _ => ResetNavigation(navigation , 'HomeBusiness')

    const GoToWelcome = _ => {
        props.GoToWelcome()
    }

    const SuccessfulRegistered = _ => {
        function WriteInReduxAccountData(dataAccount){
            props.LoginAccount({
                dataAccount
            })
            GoToWelcome()
            // dataAccount.accountType==='user'
            // ?
            // NavegationToHomeUser()
            // :
            // NavegationToHomeBusiness()
        }
        async function UserConnected(){
            GetDataOfCurrentAccount()
            .then(
                r => {
                    WriteInReduxAccountData(r)
                }
            )
            .catch(
                e => {
                }
            )
            
        }
        Alert.alert(
            '¡Registrado con éxito!',
            `Que buena noticia, ${formData.name}, ahora sos parte de Watopub 🙌 \n\n Ahora tenés la llave de acceso \n (recuerda tu correo y contraseña)`,
            [
                {
                    text:'Continuar 🗝',
                    onPress: UserConnected,
                    style : 'default'
                }
            ],
            {
                cancelable:false,
                onDismiss:false,
            }
        )
    }
    
    const FailRegistered = e => {
        Alert.alert(
            '¡Fallo en el registro!',
            `Parece que: ${e} 🤷‍♂️ \n\n Te recomendamos solucionar el error y continuar, sino podés intentalo nuevamente más tarde.`,
            [
                {
                    text:'Aceptar',
                    style : 'default'
                }
            ],
            {
                cancelable:false,
                onDismiss:false,
            }
        )
    }

    const RegisterUser = async _ => {
        await RegisterUserInFirebase(formData)
        .then(
            r => {
                SuccessfulRegistered()
                setInProgress(false)
            }
        )
        .catch(
            e => {
                FailRegistered(e)
                setInProgress(false)
            }
        )
    }

    const RegisterBusiness = async _ => {
        await RegisterBusinessInFirebase(formData)
        .then(
            r => {
                SuccessfulRegistered()
                setInProgress(false)
            }
        )
        .catch(
            e => {
                FailRegistered(e)
                setInProgress(false)
            }
        )
    }

    const Register = _ => {
        setInProgress(true)
        const checkForm = CheckForm() 
        checkForm
        &&
        setTimeout(_=>{
            typeAccount==='user'
            ? RegisterUser()
            : RegisterBusiness()
        },1500)
    }
    

    const CheckForm = _ => {
        let condition = true
        let messageToAlert = '\n'
        let countErrors = 0

        if (formData.name.length === 0){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El campo de nombre está vacío')
        }
        if (formData.password.length === 0){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El campo de contraseña está vacío')
        }else if (formData.password.length < 6){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - Longitud de contraseña debe ser mayor a seis (6)')
        }
        if (formData.email.length === 0){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El campo de correo electrónico está vacío')
        }else if (!validateEmail(formData.email)){
            condition = false
            countErrors ++
            messageToAlert = messageToAlert.concat('\n - El correo electrónico tiene un formato inválido')
        }
        if (countErrors>0){
            setInProgress(false)
            Alert.alert(
                '¡Formato incorrecto! 🚫',
                countErrors===1 ? 'Revise el siguiente punto:\n'.concat(messageToAlert) : 'Revise las siguientes puntos:\n'.concat(messageToAlert),
                [
                    {
                        text:'Aceptar y corregir'
                    }
                ]
            )
        }
        return condition
    }



    function NameInput(){
        return (
            <Fragment>
                <TextInput 
                value={formData.name}
                textOut={typeAccount==='user' ? "Nombre y apellidos" : "Nombre del negocio" }
                styleParent={STYLES.separator}
                LeftIcon={ <FontAwesome5 name="at" size={24} color={'#FFF'} /> }
                OnChangeParent={OnChange}
                OnChangeVariable="name"
                />
            </Fragment>
        )
    }

   
    function BusinessCategory(){
        return (
            <Fragment>
                <Picker 
                items={ listOfBusinessCategories }
                styleParent={STYLES.separator}
                LeftIcon={
                    <FontAwesome5 name="at" size={24} color={'#FFF'} />
                }
                textOut="Tipo de negocio"
                OnChangeParent={OnChange}
                OnChangeVariable="businessCategory"
                />
            </Fragment>
        )
    }

    function MailInputForRegister(){
        return (
            <MailInput 
            value={formData.email}
            style={STYLES.separator} 
            textOut="Correo electrónico" 
            OnChangeParent={OnChange}
            OnChangeVariable="email"
            />
        )
    }
    function PasswordInputForRegister(){
        return (
            <PasswordInput 
            value={formData.password}
            textOut="Contraseña" 
            helperText="Mínimo 6 carácteres"  
            OnChangeParent={OnChange}
            OnChangeVariable="password"
            />
        )
    }

    function ConditionsAndTerms(){
        function TextLink(props){
            return (
                <Text
                style={STYLES.textLink}>
                    {props.children}
                </Text>
            )
        }
        return (
            <Fragment>
                <View style={STYLES.conditionsAndTerms}>
                    <Text style={STYLES.textConditionsAndTerms} >Al presionar el botón "Regístrate", estarás aceptando nuestros <TextLink>Términos de uso</TextLink> y nuestra <TextLink>declaración de privacidad</TextLink>, confirmas que tienes más de 18 años.</Text>
                </View>
            </Fragment>
        )
    }

    function ButtonRegister(){
        return (
            <Button 
            text="¡Regístrate!"
            styleContainer={STYLES.containerButtonRegisterFromRegisterScreen} 
            styleButton={STYLES.buttonRegister} 
            styleText={STYLES.textButtonRegister} 
            onPress={Register}
            isLoading={inProgress}
            />
        )
    }

    function InputsForUser(){
        return (
            <Fragment>
                <Text style={[STYLES.textTitle, STYLES.separator]} > ¡Únete gratis! </Text>
                
                <NameInput />
                
                <MailInputForRegister />
                
                <PasswordInputForRegister />
                
                <ConditionsAndTerms />

                <ButtonRegister />
            </Fragment>
        )
    }

    function InputsForBusiness(){
        return (
            <Fragment>
                <Text style={[STYLES.textTitle, STYLES.separator]} > ¡Haz crecer tu negocio! </Text>
                
                <NameInput />
                
                <BusinessCategory />
                
                <MailInputForRegister />
                
                <PasswordInputForRegister />
                
                <ButtonRegister />

            </Fragment>
        )
    }

    return (
        <Fragment>
            <View
            style={[
                STYLES.containerComponent,
                props.styleContainer,
            ]}
            >
                <InputsForUser />
            </View>
        </Fragment>
    )
}


const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
}
  
const mapDispatchToProps = { 
    LoginAccount
}
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Component)
export default component