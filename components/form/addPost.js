// React
import React , { useEffect , useState } from 'react';

// React native
import { View, Alert,  } from 'react-native';

// Image picker
import * as ImagePicker from "expo-image-picker"

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import TextInput from '../textInput/textInput'
import Button from '../button/button'
import CircleButton from '../button/circle'
import Media from '../media/forPost'
import Backdrop from '../backdrop/loadingFullScreen'
import Picker from '../picker/picker'
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

// Styles
import { STYLES } from './styles'

// Firestore
import { 
    UploadPost
} from '../../firebase/firestore/posts'

// Utilities
import {
    ResetNavigation
} from '../../utils/utilities'
import { SIZES } from '../../consts/sizes';

// Data
import DATA from '../../consts/data/businessCategoriesExpanded'


const IMAGE_CAMERA = require('../../assets/icons/posts/camera.png')
const IMAGE_GALLERY = require('../../assets/icons/posts/gallery.png')
const SIZE_BUTTON = 40
const SIZE_BUTTON_ICON = 27

function Component(props){

    const [inProgress,setInProgress] = useState(false)
    const [description, setDescription] = useState('')
    const [mediaType,setMediaType] = useState(null)
    const [media,setMedia] = useState(null)
    const [duration,setDuration] = useState(null)
    const [category,setCategory] = useState("Software")

    const { navigation } = props

    const OnChange = (value) => {
        setDescription(value)
    }
    
    const OnChangeCategory = (value) => {
        setCategory(value)
    }

    const GoToHome = _ => {
        ResetNavigation(navigation,'HomeUser')
    }


    useEffect( _ => {
        const getPermissionAsync = async () => {
            if (Constants.platform.ios) {
              const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
              }
            }
          };
        getPermissionAsync();
    },[])

    
    ChooseImageFromCamera = async() => { 
        try{
            let result = await ImagePicker.launchCameraAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                quality: SIZES.mediaForUpload.quality,
                aspect:[ SIZES.post.aspect.w , SIZES.post.aspect.h ]
            })
            if (!result.cancelled) {
                setMediaType(result.type)
                setMedia(result.uri)
                result.type==='video' ? setDuration(result.duration) : setDuration(null)
            }else{
                alert("Cancelaste la subida")
            }
        } catch (e) {
            console.log("Error con la cámara");
            console.log(e);
        }
    }

    ChooseImage = async() => { 
        try{
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                quality: SIZES.mediaForUpload.quality,
                aspect:[ SIZES.post.aspect.w , SIZES.post.aspect.h ]
            })
            if (!result.cancelled) {
                setMediaType(result.type)
                setMedia(result.uri)
                result.type==='video' ? setDuration(result.duration) : setDuration(null)
            }else{
                alert("Cancelaste la subida")
            }
        } catch (e) {
            console.log("Error seleccionando el archivo a subir");
            console.log(e);
        }
    }



    const Upload = _ => {
        const Successful = _ => {
            Alert.alert(
                '¡Publicación en línea! 💙',
                "Tu nueva publicación ya está subida, ve a verla",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => GoToHome()
                    }
                ]
            )
        }
        const NoMediaWarning = _ => {
            Alert.alert(
                'No hay archivo cargado 🚫',
                "¿Está seguro que desea publicar sin ninguna imagen o vídeo?",
                [
                    {
                        text:'Aceptar',
                        onPress: _ => UploadPostConfirmed()
                    },
                    {
                        text:'Cancelar',
                        style:'cancel'
                    }
                ]
            )
        }
        const VideoOutSeconds = _ => {
            Alert.alert(
                'Edite o seleccione otro vídeo 🚫',
                `Este video tiene una duración de ${(duration/1000).toFixed(1)} segundos. \n \n La duración máxima es de ${40} segundos.`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const EmptyDescription = _ => {
            Alert.alert(
                'Descripción vacía 🚫',
                `Dígite al menos algún texto en el campo de descripción`,
                [
                    {
                        text:'Aceptar'
                    }
                ]
            )
        }
        const UploadPostConfirmed = _ => {
            setInProgress(true)
            UploadPost({
                description,
                media,
                mediaType,
                category
            }).then(
                r => {
                    setInProgress(false)
                    Successful()
                }
            ).catch(
                e => {
                    setInProgress(false)
                    alert('Hubo un pequeño error, lo sentimos 😔 \n\n Intenta más tarde')
                }
            )
        }
        
        // Check before upload the post
        if (description.replace(' ','') .length<1){
            EmptyDescription()
        }else if (duration>40000){
            VideoOutSeconds()
        }else if (media===null){
            NoMediaWarning()
        }else{
            UploadPostConfirmed()
        }
    }

    function MediaVisualization(){
        return (
            <Media 
            source={{uri:media}}
            mediaType={mediaType}
            />
        )
    }

    function UploadButton(){
        return (
            <Grid style={{marginBottom:20}}>
                <Row>
                    <Col size={1}></Col>
                    <Col size={1}>
                        <CircleButton 
                        notCircle
                        text="Cámara"
                        source={IMAGE_CAMERA}
                        size={SIZE_BUTTON}
                        sizeIcon={SIZE_BUTTON_ICON}
                        onPress={ChooseImageFromCamera}
                        />
                    </Col>
                    <Col size={1}>
                         <CircleButton 
                         notCircle
                        text="Galería"
                        source={IMAGE_GALLERY}
                        size={SIZE_BUTTON}
                        sizeIcon={SIZE_BUTTON_ICON}
                        onPress={ChooseImage}
                        />
                    </Col>
                    <Col size={1}></Col>
                </Row>

            </Grid>
        )
    }

    function DescriptionInput(){
        return (
            <TextInput
            value={description}
            multiline
            placeholder="¡Escribe aquí!"
            styleParent={{width:'86%',marginHorizontal:'7%' , marginVertical:10 , height:150}}
            OnChangeParent={OnChange}
            />
        )
    }
    
    function ListCategories(){
        return (
            <Picker 
            value={category}
            items={Object.keys(DATA).map( key => DATA[key].name )}
            styleParent={STYLES.picker}
            // LeftIcon={
            //     <FontAwesome5 name="at" size={24} color={'#555'} />
            // }
            textOut="Categoría"
            OnChangeParent={OnChangeCategory}
            // OnChangeVariable="businessCategory"
            />
        )
    }
    
    function PostButton(){
        return (
            <Button 
            text="Publicar"
            styleButton={[STYLES.postButton]} 
            styleText={[STYLES.uploadImageText]} 
            onPress={Upload}
            />
        )
    }
    
    return (
            <View
            style={[
                STYLES.containerComponent,
                STYLES.containerComponentAddPost,
                props.styleContainer,
            ]}
            >
                <Backdrop 
                show={inProgress}
                animationLoading
                text="Subiendo tu publicación"
                />

                <MediaVisualization />

                <UploadButton />

                <DescriptionInput />    

                <ListCategories />

                <PostButton />
                
            </View>
    )
}

export default Component