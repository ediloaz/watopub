// React
import React , { Fragment, useEffect, useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, TextInput, Image , Picker  } from 'react-native';

// React native community
// import { Picker } from '@react-native-community/picker';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'


function Component(props){

    const [selectedValue, setSelectedValue] = useState(null)

    const { items } = props

    useEffect( _ => {
        setSelectedValue(props.value)
    },[props.value])

    function LeftIcon(){
        return(
            <View
            style={STYLES.icon}
            >
                {props.LeftIcon}
            </View>
        )
    }

    function Item({label , value , key , color=null}){
        return (
            <Picker.Item  label={label} value={value} key={key} color={color} />
        )
    }

    function Items(){
        const itemsComponent = [items.map( (item,index) => 
            <Item label={item} value={item} key={index} color="#444" />
        )]
        return itemsComponent
    }

    const OnChange = value => {
        setSelectedValue(value)
        props.OnChangeParent && OnChangeParent(value)
    }

    const OnChangeParent = value => {
        // props.OnChangeParent(value,props.OnChangeVariable)
        props.OnChangeParent(value)
    }

    function Input(){
        return (
        <View
            style={[
                STYLES.containerTextField,
                STYLES.containerTextFieldWithIcon,
                props.styleParent,
            ]}
            {...props}
            >
                {props.LeftIcon && <LeftIcon />}

                <Picker
                mode='dialog'
                prompt='Seleccione una categoría'
                style={ [STYLES.picker] }
                selectedValue={props.value}
                onValueChange={OnChange}
                >
                    {Items()}
                </Picker>
                
            </View>
        )
    }

    function HelperText(props){
        return (
            <Text style={STYLES.textHelper} >
                    {props.children}
                </Text>
        )
    }

    function InputWithTextOut(){
        function TextOut(props){
            return (
                <Text style={STYLES.textOut} >
                    {props.children}
                </Text>
            )
        }
        return (
            <Fragment>
                <TextOut>{props.textOut}</TextOut>
                <Input />
                {props.helperText && <HelperText> {props.helperText} </HelperText> }
            </Fragment>
        )
    }

    return (
        <Fragment>
            {props.textOut
            ? <InputWithTextOut />
            : <Input />
            }
        </Fragment>
    )
}

export default Component