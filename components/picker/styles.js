import * as React from 'react';
import { StyleSheet } from 'react-native';

const FONT_SIZE_3 = 16
const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    containerTextField : {
        backgroundColor: '#FFF',
        borderColor: '#999',
        borderRadius: 13,
        borderWidth: 1,
        paddingHorizontal:15,
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerTextFieldWithIcon : {
        flexDirection:'column',
        flexWrap:'wrap',
        display:'flex'
    },
    picker : {
        width:'100%',
        fontSize : FONT_SIZE_3,
        height:40,
    },
    
    icon:{
        width:'10%',
        alignItems: 'center',
        justifyContent: 'center',
    },


    textOut : {
        marginHorizontal:'10%',
        fontSize:FONT_SIZE_4,
        color:'#555',
        marginBottom:-6,
        backgroundColor:'#FFF',
        width:20,
        paddingLeft:5,
        zIndex:10
    },
    textHelper : {
        fontSize:FONT_SIZE_5,
        color:'#FFF',
        marginBottom:10,
    }

})

