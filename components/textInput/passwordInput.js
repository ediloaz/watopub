// React
import React , { Fragment } from 'react';

// React native
import { Platform, StyleSheet, Text, View,  } from 'react-native';
import { FontAwesome } from '@expo/vector-icons'; 

// Material Kit
// import { Textfield } from 'react-native-material-kit'

// Watopub components
import TextInput from './textInput'

// Consts
import { COLORS } from '../../consts/colors'

// Styles
import { STYLES } from './styles'

// const ICON = require('')

function Component(props){

    return (
        <Fragment>
            <TextInput 
                value={props.value || null}
                textOut={props.textOut || false}
                helperText={props.helperText || false}
                placeholder='Contraseña'
                // keyboardType='visible-password' // instead we use 'secureTextEntry'
                secureTextEntry={true}
                styleParent={props.style}
                LeftIcon={
                    <FontAwesome name="lock" size={24} color={COLORS.yellow.default} />
                }
                OnChangeParent={props.OnChangeParent || null}
                OnChangeVariable={props.OnChangeVariable || null}
            />
        </Fragment>
    )
}

export default Component