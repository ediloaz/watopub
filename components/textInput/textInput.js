// React
import React , { Fragment , useState, useEffect } from 'react';

// React native
import { Platform, StyleSheet, Text, View, TextInput, Image } from 'react-native';

// Material Kit
import { Textfield } from 'react-native-material-kit'

// Styles
import { STYLES } from './styles'
import { onChange } from 'react-native-reanimated';


function Component(props){


    function LeftIcon(){
        return(
            <View
            style={STYLES.icon}
            >
                {props.LeftIcon}
            </View>
        )
    }

    function TextInputSpecial(){
        
        const [value, setValue] = useState(props.value || '')

        const OnChange = value => {
            setValue(value)
            // props.inDirect && props.OnChangeParent(value)
        }

        const OnBlur = _ => {
            if (props.OnChangeParent) {
                if (props.OnChangeVariable){
                    props.OnChangeParent( value , props.OnChangeVariable )
                }else{
                    props.OnChangeParent( value )
                }
            }
        }
        
        return (
            <TextInput 
            multiline={props.multiline}
            value={value}
            // defaultValue={value}
            placeholder={props.textOut ? 'Escribe aquí' : props.placeholder}
            keyboardType={props.keyboardType || 'default'} 
            style={ [STYLES.textField , props.styleTextInput] }
            textInputStyle={[STYLES.textInputRoot]}
            secureTextEntry={props.secureTextEntry || false}
            onChangeText={OnChange}
            onBlur={OnBlur}
            />
        )
    }
    
    function Input(){
        return (
            <Fragment>
                <View
                style={[
                    STYLES.containerTextField,
                    STYLES.containerTextFieldWithIcon,
                    props.styleParent,
                ]}
                {...props}
                >
                    {props.LeftIcon && <LeftIcon />}
                    <TextInputSpecial />
                </View>
            </Fragment>
        )
    }

    function HelperText(props){
        return (
            <Fragment>
                <Text style={STYLES.textHelper} >
                        {props.children}
                    </Text>
            </Fragment>
        )
    }

    function InputWithTextOut(){
        function TextOut(props){
            return (
                <Fragment>
                    <Text style={STYLES.textOut} >
                        {props.children}
                    </Text>
                </Fragment>
            )
        }
        return (
            <Fragment>
                <TextOut>{props.textOut}</TextOut>
                <Input />
                {props.helperText && <HelperText> {props.helperText} </HelperText> }
            </Fragment>
        )
    }

    return (
        <Fragment>
            {props.textOut
            ? <InputWithTextOut />
            : <Input />
            }
        </Fragment>
    )
}

export default Component