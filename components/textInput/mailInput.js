// React
import React , { Fragment } from 'react';

// React native
import { Platform, StyleSheet, Text, View,  } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons'; 

// Material Kit
// import { Textfield } from 'react-native-material-kit'

// Watopub components
import TextInput from './textInput'

// Consts
import { COLORS } from '../../consts/colors'

// Styles
import { STYLES } from './styles'


// const ICON = require('')

function Component(props){

    return (
        <Fragment>
            <TextInput
                value={props.value || null}
                textOut={props.textOut || false}
                placeholder='Correo electrónico'
                keyboardType='email-address'
                styleParent={props.style}
                LeftIcon={
                    <FontAwesome5 name="at" size={24} color={COLORS.yellow.default} />
                }
                OnChangeParent={props.OnChangeParent || null}
                OnChangeVariable={props.OnChangeVariable || null}
            />
        </Fragment>
    )
}

export default Component