import * as React from 'react';
import { StyleSheet } from 'react-native';

const FONT_SIZE_3 = 16
const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    containerTextField : {
        backgroundColor: '#FFF',
        // width:'100%',
        borderColor: '#999',
        borderRadius: 13,
        borderWidth: 1,
        paddingHorizontal:15,
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerTextFieldWithIcon : {
        // direction : 'ltr'
        flexDirection:'column',
        flexWrap:'wrap',
        display:'flex'
    },
    textField : {
        width:'90%',
        fontSize : FONT_SIZE_3,
        // backgroundColor:'#0F0'
        // width: '100%',
        // height : 100 ,

        
    },
    backgroundWhite : {
        
    },
    textInputRoot : {
        borderBottomWidth: 0
    },
    icon:{
        width:'10%',
        alignItems: 'center',
        justifyContent: 'center',
    },


    textOut : {
        fontSize:FONT_SIZE_4,
        color:'#FFF',
        marginBottom:10,
    },
    textHelper : {
        fontSize:FONT_SIZE_5,
        color:'#FFF',
        marginBottom:10,
    }

})

