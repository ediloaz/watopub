// React
import React , { useEffect , useState } from 'react';

// React native
import { View , FlatList , Text } from 'react-native';

// Watopub components
import Post from '../../post/post'
import CircleButton  from '../../button/circle'
import CircleAnimation from '../../animations/circleLoading'
import Footer from '../../footers/lists'

// Firestore
import { 
    GetPostsOfMyBusiness as _GetPosts
} from '../../../firebase/firestore/posts'

// Utilities
import { 
    FirebaseDateToLocalDateHumanReadable
} from '../../../utils/utilities'

// Styles
import { STYLES } from '../styles'



const IMAGE_RELOAD = require('../../../assets/icons/reload.png')

function Component(props){

    // const [currentID, setCurrentID]
    
    const [loading,setLoading] = useState(true)
    const [existsPosts,setExistsPosts] = useState(true)
    const [posts, setPosts] = useState([])
    // const [reloadNextPosts,setReloadNextPosts] = useState(false)

    const { navigation , category } = props

    // useEffect( _ => {
    //    setCategory(props.category) 
    // },[])

    // // Load more posts
    // useEffect( _ => {
    //     _GetPosts()
    //         .then(
    //             r => {
    //             r.get()
    //             .then(function(querySnapshot) {
    //                 querySnapshot.forEach(function(doc) {
    //                     const POST = doc.data()
    //                     POST['id'] = posts.length
    //                     POST['keyPost'] = doc.id.toString()
    //                     POST['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
    //                     setPosts(posts => [...posts , POST]) 
    //                 })
    //                 console.log("cargamos más posts")
    //                 // setLoading(false)
    //             }
    //         )
    //     })
    //     .catch(
    //         e => {
    //             console.log(e)
    //             console.log('Error trayendo las publicaciones')
    //             // setLoading(false)
    //             alert("No hubo conexión")
    //         }
    //     )
    // },[props.reachEnd])

    useEffect( _ => {
        setPosts([])
        setExistsPosts(true)
        let cont = 0
        let categoryForParams = props.isForUserProfile ? {user:category} : category
        _GetPosts(categoryForParams)
            .then(
                r => {
                r.get()
                .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        const POST = doc.data()
                        POST['id'] = cont
                        POST['keyPost'] = doc.id.toString()
                        POST['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
                        setPosts(posts => [...posts , POST]) 
                        cont ++
                    })
                    cont===0 && setExistsPosts(false)
                    setTimeout(() => {
                        setLoading(false)
                    }, 2000); 
                }
            )
        })
        .catch(
            e => {
                console.log(e)
                console.log('Error trayendo las publicaciones')
                setLoading(false)
                setExistsPosts(false)
                alert("No hubo conexión")
            }
        )
    },[])

    
    const GetPosts = _ => {
        setLoading(true)
        setPosts([])
        setExistsPosts(true)
        let cont = 0
        setTimeout(_ => {
            _GetPosts(category || null)
            .then(
                r => {
                    r.get()
                    .then(function(querySnapshot) {
                        querySnapshot.forEach(function(doc) {
                            const POST = doc.data()
                            POST['id'] = cont
                            POST['keyPost'] = doc.id.toString()
                            POST['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
                            setPosts(posts => [...posts , POST]) 
                            cont ++
                        })
                    })
                    setTimeout(() => {
                        setLoading(false)
                    }, 2000); 
                    cont===0 && setExistsPosts(false)
                }
            )
            .catch(
                e => {
                    console.log(e)
                    console.log('Error trayendo las publicaciones')
                    setLoading(false)
                    alert("No hubo conexión")
                }
            )
        },3000)
    }

    // const LoadNextPosts = _ => {
    //     console.log("Vamos a cargar ás posts")
    //     _GetPosts()
    //     .then(
    //         r => {
    //             r.get()
    //             .then(function(querySnapshot) {
    //                 querySnapshot.forEach(function(doc) {
    //                     const POST = doc.data()
    //                     POST['id'] = posts.length
    //                     POST['keyPost'] = doc.id.toString()
    //                     POST['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
    //                     setPosts(posts => [...posts , POST]) 
    //                 })
    //             })
    //             setLoading(false)
    //         }
    //     )
    //     .catch(
    //         e => {
    //             console.log(e)
    //             console.log('Error trayendo las publicaciones')
    //             setLoading(false)
    //             alert("No hubo conexión")
    //         }
    //     )
    // }

    

    function ListEmpty(){
        return (
            <View
            style={STYLES.viewForListEmpty}
            >
                <Text
                style={STYLES.textForListEmpty}
                >
                    {props.own ? 'No tenés publicaciones, ¡empieza con una!' : props.isForUserProfile ? 'No existen publicaciones para este usuario' : 'No existen publicaciones en esta categoría'}
                </Text>
            </View>
        )
    }
    
    
    function Loading(){
        return (
            <CircleAnimation 
            text={props.text || category ? `Cargando ${category}` : `Cargando tus publicaciones` }
            styleText={{color:'black'}}
            />
        )
    }
    
    function PostsForFlatlist({item}){
        return (
            <Post
            isForUserProfile={props.isForUserProfile}
            own={props.own}
            keyPost={item.keyPost}
            description={item.description || 'Sin descripcion'}
            mediaType = {item.mediaType}
            media = {item.media}
            date = {item.date  || 'Sin fecha'}
            businessPicture = {null}
            businessEmail = {item.businessEmail || "Email de empresa"}
            businessName = {item.businessName || "Nombre empresa"}
            controls = {item.controls}
            navigation = { navigation }
            />
        )
    }

    function FlatListWithPosts(){
        
        return (
            <FlatList 
            
            // extraData={{
            //     list: posts, offset: 0, limit: 2
            // }}
            // onEndReached={LoadNextPosts}
            // onEndReachedThreshold={0}

            // initialNumToRender={2}

            // initialNumToRender={}
            // UN COMPONENTE AL FINAL, UNA SIMPLE LÍNEA CON TEXTO INDICANDO EL FINAL DE LOS POSTEOS
            ListFooterComponent={<Footer origin='posts' />}
            data={posts}
            renderItem={PostsForFlatlist}
            keyExtractor = { item => item.id.toString() }
            />
        )
    }
    
    // function ReloadNextPosts(){
    //     const checkVisible = (isVisible) => {
    //         console.log("cambio")
    //         console.log(isVisible)
    //         // if (isVisible){
    //         //   setIsInView(isVisible)
    //         // } else {
    //         //   setIsInView(isVisible)
    //         // }
    //     }
    //     return (
    //         <View
    //         style={STYLES.containerReloadNextPosts}
    //         >
    //             {
    //                 !reloadNextPosts
    //                 &&
    //                 <CircleAnimation
    //                 text={props.text || " "}
    //                 styleText={{color:'black'}}
    //                 />
    //             }
    //         </View>
    //     )
    // }

    return (
            <View
            style={[
            STYLES.containerComponent, 
            ]}
            >
                {!props.isForUserProfile
                &&
                <CircleButton 
                source={IMAGE_RELOAD}
                onPress={ _ => GetPosts()}
                />
                }
                {
                loading
                ?
                <Loading />
                :
                existsPosts
                ?
                <FlatListWithPosts />
                :
                <ListEmpty />
                }

                {/* <ReloadNextPosts /> */}

            </View>
    )
}

export default Component