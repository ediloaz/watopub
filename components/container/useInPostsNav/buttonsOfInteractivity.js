// React
import React , { useState } from 'react';

// React native
import { View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import SquareButton from '../../button/square'

// Utilities
import {
    FirebaseDateToLocalDateHumanReadable,
} from '../../../utils/utilities'

// Firestore
import {
    GetRandomPost,
} from '../../../firebase/firestore/posts'

// Styles
import { STYLES } from '../styles'

// Consts
import { SIZES } from '../../../consts/sizes'


// IMAGES 
const IMAGES = [
    require('../../../assets/icons/discover/blue.png') ,
    require('../../../assets/icons/discover/green.png') ,
    // require('../../../assets/icons/discover/purple.png') ,
    require('../../../assets/icons/discover/red.png') ,
    require('../../../assets/icons/discover/yellow.png') ,
]


function Component(props){

    const { navigation } = props


    const GoToViewOnlyPost = dataPost => {
        // dataPost['keyPost'] = doc.id.toString()
        dataPost['date'] =  FirebaseDateToLocalDateHumanReadable(dataPost.date)
        navigation.navigate('ViewSinglePost' , { dataPost })
    }
    
    
    function EveryButtons (){
        const LIST = []
        for (var i = 0 ; i < 6 ; i++)
            LIST.push(
                <Col size={1}>
                    <ButtonTemplate />
                </Col>
            )
        return LIST
    }

    function ButtonTemplate( ){
        return (
            <SquareButton
            sizeIcon={ SIZES.squareIconSizeForInteractivityBoxes }
            source={IMAGES[Math.floor(Math.random() * IMAGES.length) + 0 ]}
            onPress={_ => GetRandomPost().then(r => GoToViewOnlyPost(r))}
            />
        )
    }

    function Container({everyButtonComponents}){
        const ROWS = []
        var buttonsTemp = []
        var cont = 0
        everyButtonComponents.forEach( ( item ) => {
            buttonsTemp.push(item)
            cont += 1
            if (cont === 2) {
                ROWS.push(
                    <Row size={1} style={{height:0 , marginBottom: 0, width:'100%'}}>
                        {Object.assign([] , buttonsTemp)}
                    </Row>
                )
                // Restart variables
                cont = 0
                buttonsTemp = []
            }
        })
        return (
            <Grid style={[STYLES.grid , {height:400, marginTop:90}]} >
               {ROWS}
            </Grid>
        )
    }


    return (
            <View
            style={[
                STYLES.containerComponent, 
                // props.styleContainer,
            ]}
            >
                <Container
                everyButtonComponents={EveryButtons()}
                />
            </View>
    )
}

export default Component