// React
import React , { Fragment , useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import SquareButton from '../../button/square'

// 
import BusinessCategoryDataExpanded from '../../../consts/data/businessCategoriesExpanded'

// Styles
import { STYLES } from '../styles'

// Firestore
import { 
    LogIn
} from '../../../firebase/firestore/accounts'

// Utilities
import {
    validateEmail,
    ResetNavigation,
} from '../../../utils/utilities'

// Consts
import { SIZES } from '../../../consts/sizes'



const IMAGES = "BusinessCategoryImages()"
// const IMAGES = BusinessCategoryImages()


function Component(props){

    const [inProgress,setInProgress] = useState(false)

    const { navigation } = props


    const GoToHome = _ => {
        setInProgress(false)
        navigation.navigate("Home");
        ResetNavigation(navigation,'Home')
    }

    const GoToPostsByCategory = (category) => {
        navigation.navigate("PostsByCategory", { category });
    }
    
    function EveryButtons (){
        const LIST = Object.keys(BusinessCategoryDataExpanded).map( (key,object) => {
            return (
                <Col size={1}>
                    <ButtonTemplate 
                    text={BusinessCategoryDataExpanded[key].name}
                    imageSource={BusinessCategoryDataExpanded[key].imageLocal}
                    />
                </Col>
            )
        })
        return LIST
    }

    function ButtonTemplate({text , imageSource}){
        // const image = require(imageSource)
        return (
            <SquareButton
            text={text}
            sizeIcon={SIZES.squareIconSizeForScreenPosts}
            source={imageSource}
            styleText={{fontWeight:'normal'}}
            styleContainer={STYLES.containerCircleButton} 
            styleButton={STYLES.squareButton} 
            onPress={_ => GoToPostsByCategory(text)}  // To show the name of business category
            />
        )
    }

    function Container({everyButtonComponents}){
        const ROWS = []
        var buttonsTemp = []
        var cont = 0
        everyButtonComponents.forEach((item,index) => {
            buttonsTemp.push(item)
            cont += 1
            if (cont === 2) {
                ROWS.push(
                    <Row size={1} style={{height:80 , marginVertical: 30, width:'100%'}}>
                        {Object.assign([],buttonsTemp)}
                    </Row>
                )
                // Restart variables
                cont = 0
                buttonsTemp = []
            }
        })
        return (
            <Grid 
            style={[
                STYLES.grid , 
            // {height:800}
            ]} >
               {ROWS}
            </Grid>
        )
    }

    function TitleCategories(){
        return (
            <Text
            style={STYLES.titleCategoriesForCategoriesButtons}
            >
                Categorías
            </Text>
        )
    }


    

    return (
        <View
        style={[
            STYLES.containerForCategoriesButtons, 
            // props.styleContainer,
        ]}
        >
            {/* <View
            style={STYLES.scrollViewForCategoriesButtons}
            > */}
            <TitleCategories />
            <Container
            everyButtonComponents={EveryButtons()}
            />
            {/* </View> */}
                
        </View>
    )
}

export default Component