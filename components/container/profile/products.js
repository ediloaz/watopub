// React
import React , { useEffect , useState } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { View, FlatList } from 'react-native';

// Watopub components
// import Button from '../../button/button'
import MiniatureProduct  from '../../product/miniature'

// Utilities
import {
    FirebaseDateToLocalDateHumanReadable
} from '../../../utils/utilities'

// Firestore
import { 
    GetProductsOfMyBusiness,
} from '../../../firebase/firestore/products'

// Styles
import { STYLES } from '../styles'

function Component(props){

    const [loading,setLoading] = useState(false)
    const [products, setProducts] = useState([])

    const { navigation } = props
    

    useEffect( _ => {
        setProducts([])
        setLoading(true)
        const BUSINESS_EMAIL = props.own ? props.account.name : props.businessEmail
        console.log("BUSINESS_EMAIL")
        console.log(BUSINESS_EMAIL)
        console.log("- BUSINESS_EMAIL")
        BUSINESS_EMAIL
        &&
        GetProductsOfMyBusiness(BUSINESS_EMAIL)
            .then(
                r => {
                r.get()
                .then(function(querySnapshot) {
                    let cont = 0
                    querySnapshot.forEach(function(doc) {
                        const PRODUCT = doc.data()
                        console.log(PRODUCT)
                        PRODUCT['id'] = cont
                        PRODUCT['keyPost'] = doc.id.toString()
                        PRODUCT['own'] = true
                        PRODUCT['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
                        setProducts(products => [...products , PRODUCT]) 
                        cont ++
                    })
                    setLoading(false)
                    props.setProductsQuantity(cont)
                }
            )
        })
        .catch(
            e => {
                console.log(e)
                console.log('Error trayendo los productos')
                setLoading(false)
                alert("No hubo conexión")
            }
        )
    },[])

    // function AA(){
    //     setProducts([])
    //     setLoading(true)
    //     GetProductsOfMyBusiness(BUSINESS_EMAIL)
    //         .then(
    //             r => {
    //             r.get()
    //             .then(function(querySnapshot) {
    //                 querySnapshot.forEach(function(doc) {
    //                     const PRODUCT = doc.data()
    //                     PRODUCT['id'] = products.length
    //                     PRODUCT['keyPost'] = doc.id.toString()
    //                     PRODUCT['date'] =  FirebaseDateToLocalDateHumanReadable(doc.data().date)
    //                     setProducts(products => [...products , PRODUCT]) 
    //                 })
    //                 setLoading(false)
    //             }
    //         )
    //     })
    //     .catch(
    //         e => {
    //             console.log(e)
    //             console.log('Error trayendo los productos')
    //             setLoading(false)
    //             alert("No hubo conexión")
    //         }
    //     )
    // }
    
    function ProductForFlatlist({item}){
        return (
            <MiniatureProduct
            own={props.own}
            data={item}
            navigation = { navigation }
            // keyPost={item.keyPost}
            // description={item.description || 'Sin descripcion'}
            // mediaType = {item.mediaType}
            // media = {item.media}
            // date = {item.date  || 'Sin fecha'}
            // businessPicture = {null}
            // BUSINESS_EMAIL = {item.BUSINESS_EMAIL || "Nombre empresa"}
            // controls = {item.controls}
            />
        )
    }

    
    function FlatListWithProducts(){
        return (
            <FlatList 
            // extraData={{
            //     list: products, offset: 0, limit: 2
            // }}
            // onEndReached={LoadNextPosts}
            // onEndReachedThreshold={0}
            // initialNumToRender={2}
            // initialNumToRender={}
            numColumns={2}
            data={products}
            renderItem={ProductForFlatlist}
            keyExtractor = { item => item.id.toString() }
            style={STYLES.containerFlatListWithProducts}
            />
        )
    }

    return (
        <View
        style={[
        STYLES.containerComponent, 
        ]}
        >
            {/* <Button 
            onPress={AA}
            /> */}

            <FlatListWithProducts />

        </View>
    )
}

const mapStateToProps = (state) => {
    return {
      account: state.account,
    }
}
  
const mapDispatchToProps = { }

const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Component)
export default component