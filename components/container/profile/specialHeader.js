// React
import React , { useState, useEffect } from 'react';

// Redux
import { connect } from 'react-redux';

// React native
import { View, Text } from 'react-native';

// React Native Easy Grid
import { Grid, Row, Col  } from "react-native-easy-grid";

// Watopub components
// import Icon from '../../image/icon'
import Title from '../../text/title'
import CircleButton from '../../button/circle'
import Icon from '../../image/icon'
import Button from '../../button/button'

// Styles
import { STYLES } from './styles'

const IMAGE_CONTROL = require('../../../assets/icons/control.png')



// Images and resources
const ICON_EDIT = require('../../../assets/icons/profile/edit.png')

const TEMPORAL_INFORMATION_BUSINESS = `Encuentra los mejores productos para el hogar \n \n Tel: 22-67-90-00 \n www.worldstores.com`

function Component(props){

    const { navigation } = props

    const [messageProducts,setMessageProducts] = useState('Cargando')
    const [ controls , setControls ] = useState(0)
    const [ follows , setFollows ] = useState(7)

    useEffect( _ => {
        if (props.productsQuantity!==null){
            const TEXT = props.productsQuantity!==null && props.productsQuantity===0 ? `Sin productos` : props.productsQuantity===1 ? `1 producto` : `${props.productsQuantity} productos` 
            setMessageProducts(TEXT)
        }
    },[props.productsQuantity])

    const GoToEditBusiness = _ => {
        navigation.navigate('Edit' , {
            information:props.account.information,
            media:props.account.media,
        })
    }

    function EditButton(){
        return (
            <CircleButton 
            source={ICON_EDIT}
            size={50}
            styleContainer={STYLES.containerEditButton}
            onPress={GoToEditBusiness}
            />
        )
    }

    function Counters(){
        function Controls(){
            return (
                <Col
                size={1}
                style={[
                    STYLES.colForInteractionButtons, 
                    STYLES.colForInteractionButtonsControls, 
                ]}  
                >
                    <Icon
                    source={IMAGE_CONTROL}
                    size={40}
                    style={STYLES.iconForControl}
                    />
                    <Text
                    style={[
                    STYLES.textForControl,
                    // controlPressed && STYLES.textForControlPressed,

                    ]}
                    >
                        {controls} {controls===1 ? 'Control en total' : 'Controls totales'}
                    </Text>
                    
                </Col>
            )
        }
        function Follow(){
            return (
                <Col
                size={1}
                style={[
                    STYLES.colForInteractionButtons, 
                    STYLES.colForInteractionButtonsFollow, 
                ]}  
                >
                    <View
                    style={STYLES.backgroundForFollow}
                    >
                        <Button
                        text='Apoyo'
                        styleContainer={STYLES.backgroundButtonForFollow}
                        styleText={{color:'white', fontSize:14}}
                        />
                        <Text
                        style={[
                        STYLES.textForFollow,
                        // controlPressed && STYLES.textForControlPressed,
                        ]}
                        >
                            {follows}
                        </Text> 
                    </View>
                    {/* <Icon
                    source={IMAGE_CONTROL}
                    size={40}
                    style={STYLES.iconForControl}
                    />
                    */}
                    
                </Col>
            )
        }
        return (
            <Grid>
                <Row>
                    <Controls />
                    <Follow />
                </Row>
            </Grid>
        )
    }

   
    function InformationBox(){
        return (
            <View
            style={STYLES.styleContainerInfoProfile}
            >
                <Text>{props.information || TEMPORAL_INFORMATION_BUSINESS}</Text>
            </View>
        )
    }

    return (
        <View
        style={[
            STYLES.containerComponentSpecialHeader,
            props.styleContainer,
        ]}
        >
            {/* <Title
            text={messageProducts}
            styleContainer={STYLES.styleContainerNumberOfProducts}
            styleText={STYLES.styleTextNumberOfProducts}
            />  */}
            <InformationBox />
            <Counters />
            {props.own && <EditButton />}
        </View>
    )
}


const mapStateToProps = (state) => {
    return {
        account: state.account,
    }
}
  
const mapDispatchToProps = { }
  
const wrapper   = connect(mapStateToProps,mapDispatchToProps)
const component = wrapper(Component)
export default component