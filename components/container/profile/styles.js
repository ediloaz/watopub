import * as React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from "../../../consts/colors";

const FONT_SIZE_3 = 16
const FONT_SIZE_5 = 12
const FONT_SIZE_7 = 8

export const STYLES = StyleSheet.create({
    container : {
        width:'100%',
        flex: 1,
        flexDirection: 'column'
    },
    row:{
        height:50,
        // backgroundColor:'#500',
        // marginVertical:0,
        // width:'100%',
        // flex: 1,
        // flexDirection: 'row'
    },
    grid : {
        width:'100%'
    },
    
    containerComponent : {
        color:'#F0F',
        // width:'100%',
        // height:800,
        // flexDirection:'column',
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        
    },
    textTitle : {
        fontWeight: 'bold',
        color:'#FFF',
        textAlign:"center",
        fontSize:FONT_SIZE_3,
    },
    backgroundWhite : {
        backgroundColor: '#FFF',
        borderColor: '#555'
    },
    textInputRoot : {
        borderBottomWidth: 0
    },
    separator : {
        marginBottom:20,
    },
    conditionsAndTerms : {
        marginTop:10,
        flexDirection:'row',
        flexWrap:'wrap',
    },
    textLink : {
        color:COLORS.yellow.default,
    },
    textConditionsAndTerms : {
        textAlign:'center',
        color:'#FFF',
        fontSize:FONT_SIZE_5,
    },
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        marginLeft:95,
        
    },
    textButtonLogin : {
        color:'#FFF',
    },

    // Special Header
    containerComponentSpecialHeader : {
        // backgroundColor: COLORS.red.default,
        alignContent:'flex-end',
        // borderBottomRightRadius:50,
        borderBottomColor:'#BBB',
        borderBottomWidth:1,
        paddingBottom:8
    },
    styleContainerNumberOfProducts : {
        backgroundColor: 'white',
        width:'40%',
        marginLeft:'55%',
        marginRight:'5%',
        paddingHorizontal:10,
    },
    styleTextNumberOfProducts : {
        // color:'#444',
        fontSize:FONT_SIZE_3,
        paddingVertical:4,
    },
    styleContainerInfoProfile : {
        marginHorizontal:'5%',
        width:'90%',
        backgroundColor: 'white',
        borderRadius: 25,
        paddingHorizontal:20,
        paddingVertical:20,
        flexDirection: 'row',
        marginTop:15,
        marginBottom:20,
        alignItems:'flex-end'
    },
    styleTextNumberOfProducts : {
        color:'black',
        fontSize:FONT_SIZE_3,
        paddingVertical:4,
    },

    containerEditButton:{
        position:'absolute',
        // marginLeft:-35,
        marginBottom:0,
        bottom:60,
        right:20,
    },

    colForInteractionButtons : {
        // backgroundColor:'#0f0',
        justifyContent:'center',
    },
    colForInteractionButtonsControls : {
        // backgroundColor:'#0F0',
        flexDirection:'row',
        marginTop:-15
    },
    colForInteractionButtonsFollow : {
        // backgroundColor:'#00F',
        flexDirection:'row',
        marginTop:-15
    },
    iconForControl:{
        marginTop:-4,
        marginLeft:70
    },
    textForControl:{
        width:'100%',
        color:'black',
        paddingTop:7,
    },

    backgroundForFollow:{
        borderColor:COLORS.red.default,
        borderWidth:1,
        height:30,
        width:160,
        borderRadius:50,
        flexDirection:'row',
        
    },
    backgroundButtonForFollow:{
        backgroundColor:COLORS.red.default,
        borderWidth:0,
        height:28,
        width:120,
        borderRadius:50,
    },
    textForFollow:{
        marginTop:0,
        marginLeft:-4,
        fontSize:20,
        width:40,
        textAlign:'center',
        // backgroundColor:'#0FA'
    },
})

