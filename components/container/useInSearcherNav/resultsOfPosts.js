// React
import React , { Fragment , useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import SquareButton from '../../button/square'

// 
import BusinessCategoryData from '../../../consts/data/businessCategories.json'
import BusinessCategoryDataExpanded from '../../../consts/data/businessCategoriesExpanded'
import BusinessCategoryImages from '../../../consts/data/businesCategoriesImages'

// Styles
import { STYLES } from '../styles'

// Firestore
import { 
    LogIn
} from '../../../firebase/firestore/searcher'

// Utilities
import {
    validateEmail,
    ResetNavigation,
} from '../../../utils/utilities'

// Consts
import { SIZES } from '../../../consts/sizes'

const IMAGES = "BusinessCategoryImages()"

function Component(props){

    const [inProgress,setInProgress] = useState(false)

    const { navigation } = props

    const GoToHome = _ => {
        setInProgress(false)
        navigation.navigate("Home");
        ResetNavigation(navigation,'Home')
    }

    function EveryButtons (){
        const LIST = BusinessCategoryData.map( item => {
            return (
                <Col size={1}>
                    <ButtonTemplate 
                    text={BusinessCategoryDataExpanded[item].name}
                    imageSource={BusinessCategoryDataExpanded[item].imageLocal}
                    />
                </Col>
            )
        })
        return LIST
    }

    function ButtonTemplate({text , imageSource}){
        // const image = require(imageSource)
        return (
            <SquareButton
            // text={text}
            sizeIcon={SIZES.squareIconSizeForScreenPosts}
            source={imageSource}
            styleContainer={STYLES.containerCircleButton} 
            styleButton={STYLES.squareButton} 
            onPress={_ => props.GoToPostsByCategory(text)}  // To show the name of business category
            />
        )
    }

    function Container({everyButtonComponents}){
        const ROWS = []
        var buttonsTemp = []
        var cont = 0
        everyButtonComponents.forEach((item,index) => {
            buttonsTemp.push(item)
            cont += 1
            if (cont === 3) {
                ROWS.push(
                    <Row size={1} style={{height:400 , marginVertical: 30, width:'100%'}}>
                        {Object.assign([],buttonsTemp)}
                    </Row>
                )
                // Restart variables
                cont = 0
                buttonsTemp = []
            }
        })
        return (
            <Grid style={[STYLES.grid , {height:500}]} >
               {ROWS}
            </Grid>
        )
    }

    return (
        <View
        style={[
        STYLES.containerComponent, 
        // props.styleContainer,
        ]}
        >
            <Container
            everyButtonComponents={EveryButtons()}
                />
                
        </View>
    )
}

export default Component