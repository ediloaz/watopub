import * as React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from "../../consts/colors";

const FONT_SIZE_2 = 18
const FONT_SIZE_3 = 16
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    container : {
        width:'100%',
        flex: 1,
        flexDirection: 'column'
    },
    containerReloadNextPosts : {
        width:'100%',
        height:200,
        
    },
    row:{
        height:50,
        // backgroundColor:'#500',
        // marginVertical:0,
        // width:'100%',
        // flex: 1,
        // flexDirection: 'row'
    },
    grid : {
        width:'100%'
    },

    containerForCategoriesButtons: {
        width:'100%',
        flex: 1,
        marginTop: 0,
        paddingTop: 30,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#FFF',
        marginBottom:80
        
    },
    titleCategoriesForCategoriesButtons:{
        width:'100%',
        paddingLeft:20,
        fontSize:FONT_SIZE_2,
        marginBottom:10,
        fontWeight:'bold',
    },
    // scrollViewForCategoriesButtons : {
    //     width:'100%',
    //     backgroundColor: '#FFF',
        
    //     // marginBottom:200,
    //     // paddingBottom:200,
    // },
    
    containerComponent : {
        color:'#F0F',
        // width:'100%',
        // height:800,
        // flexDirection:'column',
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        
    },
    textTitle : {
        fontWeight: 'bold',
        color:'#FFF',
        textAlign:"center",
        fontSize:FONT_SIZE_3,
    },
    backgroundWhite : {
        backgroundColor: '#FFF',
        borderColor: '#555'
    },
    textInputRoot : {
        borderBottomWidth: 0
    },
    separator : {
        marginBottom:20,
    },
    conditionsAndTerms : {
        marginTop:10,
        flexDirection:'row',
        flexWrap:'wrap',
    },
    textLink : {
        color:COLORS.yellow.default,
    },
    textConditionsAndTerms : {
        textAlign:'center',
        color:'#FFF',
        fontSize:FONT_SIZE_5,
    },
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        marginLeft:95,
        
    },
    textButtonLogin : {
        color:'#FFF',
    },
    viewForListEmpty:{
        
    },
    textForListEmpty:{
        textAlign:'center',
        fontSize:FONT_SIZE_3
    },

    // Products
    containerFlatListWithProducts: {
        width:'100%',
        marginTop:20,
    },
    textForFlatlistFooter : {
        fontSize:FONT_SIZE_3,
        textAlign:'center',
        marginTop:60,
        marginBottom:90,
        color:'#888'
    }
})

