// React
import React , { Fragment } from 'react';

// React native
import { SafeAreaView , ScrollView,  } from 'react-native';

// Styles
import { STYLES } from './styles'


function Component(props){

    function Static(){
        return (
            <SafeAreaView
            style={[ 
                STYLES.root, 
                STYLES.container,
                props.red && STYLES.bgRed,
            ]}
            > 
                {props.children}
            </SafeAreaView>
        )
    }

    function Scroll(){
        return (
            <SafeAreaView style={[
            STYLES.container,
            props.red && STYLES.bgRed,
            ]}>
            <ScrollView 
            style={[
            STYLES.scrollView,
            props.red && STYLES.bgRed,
            ]} 
            // onScroll={(e) => {
            //     let paddingToBottom = 10;
            //     paddingToBottom += e.nativeEvent.layoutMeasurement.height;
            //     if(e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
            //         console.log("Llegó al final")
            //         props.onReachEnd()
            //     }
            // }}
            // onScroll={_ => }
            // contentContainerStyle={[ 
            //     // STYLES.root, 
            //     STYLES.containerScroll,
            //     props.red && STYLES.bgRed,
            //     props.existsHeader && STYLES.existsHeader,
            // ]}
            > 
                {props.children}
            </ScrollView>
            </SafeAreaView>
        )
    }

    return (
        <Fragment>
            {props.scroll
            ? <Scroll />
            : <Static />
        }
        </Fragment>
    )
}

export default Component