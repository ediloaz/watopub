import * as React from 'react';
import { StyleSheet } from 'react-native';

export const STYLES = StyleSheet.create({
    root : {
          
    },
    container: {
        width:'100%',
        flex: 1,
        marginTop: 0,
        paddingTop: 30,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#FFF',
        // paddingBottom:60,
        
        
    },
    scrollView : {
        width:'100%',
        backgroundColor: '#FFF',
        // marginBottom:200,
        // paddingBottom:200,
    },
    containerScroll: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    existsHeader : {
        // backgroundColor: 'pink',
        // marginTop:50,
        paddingTop:120,
    },
    bgWhite : {
        backgroundColor: '#FFF',
    },
    bgRed : {
        color:'#FFF',
        backgroundColor: '#F00',
    },
    textInputRoot : {
        borderBottomWidth: 0
    }

})

