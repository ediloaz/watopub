// React
import React from 'react';

// React native
import { View } from 'react-native';

export default function Component(props) {
    return (
        <View style={{marginBottom: props.size || 50}} />
    )
}