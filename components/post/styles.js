import * as React from 'react';
import { StyleSheet } from 'react-native';
import { COLORS } from "../../consts/colors";
import { SIZES } from "../../consts/sizes";

const FONT_SIZE_3 = 16
const FONT_SIZE_4 = 14
const FONT_SIZE_5 = 12

export const STYLES = StyleSheet.create({
    container : {
        width:'100%',
        flex: 1,
        flexDirection: 'column'
    },
    row:{
        height:50,
        // backgroundColor:'#500',
        // marginVertical:0,
        // width:'100%',
        // flex: 1,
        // flexDirection: 'row'
    },
    grid : {
        width:'100%'
    },
    
    containerComponent : {
        backgroundColor:'#FFF',
        flexDirection:'column',
        marginBottom:50,
        
    },
    
    textTitle : {
        fontWeight: 'bold',
        color:'#FFF',
        textAlign:"center",
        fontSize:FONT_SIZE_3,
    },
    backgroundWhite : {
        backgroundColor: '#FFF',
        borderColor: '#555'
    },
    textInputRoot : {
        borderBottomWidth: 0
    },
    separator : {
        marginBottom:20,
    },
    conditionsAndTerms : {
        marginTop:10,
        flexDirection:'row',
        flexWrap:'wrap',
    },
    textLink : {
        color:COLORS.yellow.default,
    },
    textConditionsAndTerms : {
        textAlign:'center',
        color:'#FFF',
        fontSize:FONT_SIZE_5,
    },
    buttonLogin : {
        backgroundColor:COLORS.blue.default,
        marginTop:22,
        // alignSelf:'center',
        // alignContent:'center',
        // alignItems:'center',
        marginLeft:95,
        
    },
    textButtonLogin : {
        color:'#FFF',
    },



    containerForTitle : {
        backgroundColor:COLORS.red.default,
        width:'100%',
        height:50,
    } ,
    colForTitle:{
        justifyContent:'center'
    },
    textForTitle : {
        textAlign:'center',
        color:'white',
        fontWeight:'bold',
        // marginVertical:20,
    } ,

   
    containerForBody : {
        color:'black',
        width:'100%',
        // borderStyle:'solid',
        // borderWidth:2,
        // borderColor:'green',
        
    } ,
    gridForBody:{
        height:'auto',
        flex:0,
        resizeMode:'contain',
        // height:800,
        // borderStyle:'solid',
        // borderBottomWidth:1,
        // borderBottomColor:'purple',
    },
    rowForBody:{
        // borderStyle:'solid',
        // borderWidth:2,
        // borderColor:'black',
    },
    rowForBodyImage:{
        // backgroundColor:'#F'
        // borderBottomWidth:1,
        // borderBottomColor:'#CCC',
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation:1,
    },
    rowForBodyDate:{
        marginTop:10,
        marginHorizontal:'3%',
        height:SIZES.post.date.height
    },
    rowForBodyText:{
        marginHorizontal:'3%',
        flex:0,
        height:'auto'
    },
    
    textForDate : {
        width:'100%',
        color:'#555',
        fontWeight:'normal',
        textAlign:'left',
        fontSize:FONT_SIZE_5,
        height:'auto',
        flex: 0
        // height:'100%',
        // resizeMode:'contain'
    } ,
    textForBody : {
        width:'100%',
        color:'black',
        fontWeight:'normal',
        textAlign:'justify',
        fontSize:FONT_SIZE_4,
        flex:0,
        height:'auto'
    } ,

    containerForInteractionButtons : {
        // backgroundColor:'pink',
        width:'100%',
        height:70,
        justifyContent:'center'
    } ,
    colForInteractionButtons : {
        justifyContent:'center'
    },
    containerButtonInteraction:{
        backgroundColor:COLORS.red.default,
        // padding:20
    },

    // Posts for Results
    containerForBodyOfResult : {
        backgroundColor:'#DDD',
        width:SIZES.resultsContainers.width,
        // height:SIZES.resultsContainers.height,    
        paddingHorizontal:SIZES.resultsContainers.marginHorizontal,
        paddingVertical:15,
        marginHorizontal:'1%',
        marginBottom:20,
        borderRadius:20,
    },
    rowForBodyDateOfResult:{
        // marginTop:-10,
        marginHorizontal:'3%',
        height:SIZES.post.date.height,
        textAlign:'center',
    },
    rowForBodyMainOfResult:{
        marginTop:-20,
    },
    textForDateOfResult : {
        textAlign:'right',
    },
    colOfResult:{
        justifyContent:'center'
    },
    textOfResultForBusinessName : {
        fontWeight:'bold',
    },
    textOfResultForDescription : {
        fontWeight:'normal',
    },
})

