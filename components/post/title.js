// React
import React , {  } from 'react';

// React native
import { TouchableHighlight, Text, View,  } from 'react-native';

// React Native Easy Grid
import { Col, Grid } from "react-native-easy-grid";

// Watopub components
import CircleButton from '../button/circle'

// Styles
import { STYLES } from './styles'

const IMAGE_MENU = require('../../assets/icons/posts/menu.png')

function Component(props){

    const { navigation } = props


    const GoToDeletePost = _ => {
        navigation.navigate("DeletePost", { type:'publicación' , keyPost:props.keyPost });
    }
    
    const GoToBusinessProfile = _ => {
        navigation.navigate("ProfileIndex" , { onlyView:true , businessEmail:props.businessEmail });
    }

    function ProfilePictureBusiness(){
        return (
            <CircleButton
            source={props.image}
            size={20}
            // onPress={GoToBusinessProfile}
            />
        )
    }

    function MenuToDelete(){
        return (
            <CircleButton
            source={IMAGE_MENU}
            size={20}
            onPress={GoToDeletePost}
            />
        )
    }

    function GridComplete(){
        return (
            <Grid
            >
                <Col
                size={1}
                style={[
                STYLES.colForTitle, 
                ]}
                >
                    {
                    props.own
                    &&
                    <ProfilePictureBusiness />
                    }
                </Col>
                <Col
                size={5}
                style={[
                STYLES.colForTitle, 
                ]}  
                >
                    <Text
                    style={[
                    STYLES.textForTitle
                    ]}
                    >
                        {props.text}
                    </Text>
                </Col>
                <Col
                size={1}
                style={[
                STYLES.colForTitle, 
                ]}  
                >
                    {
                    props.own
                    ?
                    <MenuToDelete />
                    :
                    <ProfilePictureBusiness />
                    }
                </Col>
            </Grid>
        )
    }

    return (
            <View
            style={[
                STYLES.containerForTitle, 
                // props.styleContainer,
            ]}
            >
                {
                    props.own
                    ?
                    <GridComplete />
                    :
                    <TouchableHighlight
                    style={{height:50}}
                    onPress={_ => GoToBusinessProfile()}
                    >
                        <GridComplete />
                    </TouchableHighlight>
                }


               
            </View>
    )
}

export default Component