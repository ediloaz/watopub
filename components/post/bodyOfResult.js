// React
import React , { useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import Icon  from '../icon/circle'
import CircleButton  from '../button/circle'

// Styles
import { STYLES } from './styles'

// import { SIZES } from '../../consts/sizes'

// Resources
const IMAGE_BUTTON_ARROW = require('../../assets/icons/posts/arrow.png')
const LIMIT_LENGHT_DESCRIPTION = 50

function Component(props){


    return (
            <View
            style={[
                STYLES.containerForBodyOfResult, 
            ]}
            >
                <Grid
                style={[
                    STYLES.gridForBody, 
                ]}  
                >
                    
                    {/* Date */}
                    <Row
                    style={[
                        STYLES.rowForBody, 
                        STYLES.rowForBodyDateOfResult,
                    ]}  
                    >
                        <Text
                        style={[
                        STYLES.textForDate,
                        STYLES.textForDateOfResult,
                        ]}
                        >
                            {props.date}
                        </Text>
                    </Row>
                    
                    {/* Image or Video or Nothing */}
                    {props.media && 
                    <Row
                    style={[
                        STYLES.rowForBodyMainOfResult, 
                    ]}  
                    >
                        <Col
                        size={2}
                        style={STYLES.colOfResult}
                        >
                            <Icon 
                            // forView
                            source={props.media}
                            size={80}
                            // styleIcon={{borderRadius:200,display:'none'}}
                            // mediaType={props.mediaType}
                            // PressControlFromMedia={props.PressControlFromMedia}
                            />
                        </Col>
                        <Col
                        size={4}
                        style={STYLES.colOfResult}
                        >
                            <Text style={STYLES.textOfResultForBusinessName} >
                                {props.businessName}
                            </Text>
                            <Text style={STYLES.textOfResultForDescription} >
                                {props.text.substring(0,LIMIT_LENGHT_DESCRIPTION)} {props.text.length>LIMIT_LENGHT_DESCRIPTION && ' ...'}
                            </Text>
                        </Col>
                        <Col
                        size={1}
                        style={STYLES.colOfResult}
                        >
                            <CircleButton 
                            source={IMAGE_BUTTON_ARROW}
                            size={40}
                            // PressControlFromMedia={props.PressControlFromMedia}
                            />
                        </Col>
                        
                    </Row>
                    }

                    
                  
                </Grid>


               
            </View>
    )
}

export default Component