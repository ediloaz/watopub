// React
import React , { useState , useEffect } from 'react';

// React native
import {  View  } from 'react-native';

// Post Watopub components
import Title from './title'
import Body from './body'
import InteractionButtons from '../button/interactionButtons'

// Styles
import { STYLES } from './styles'

// Firestore
import { 
    GetImageFromPosts
} from '../../firebase/firestore/posts'


// const IMAGETEMP = require('../../assets/icons/businessCategories/BC_14.png')
const IMAGETEMP = require('../../assets/images/tempLogoBusiness.png')


function Component(props){

    const { navigation } = props

    const [media,setMedia] = useState(null)
    // const [controlPressedIntermetent,setControlPressedIntermetent] = useState(false)

    useEffect( _ => {
        props.media && GetImageFromPosts(props.media)
        .then( r => {
            setMedia(r)
        })
    },[])

    // const PressControlFromMedia = _ => {
    //     setControlPressedIntermetent(!controlPressedIntermetent)
    // }

    return (
            <View
            style={[
                STYLES.containerComponent, 
                // props.styleContainer,
            ]} 
            >
                {!props.isForUserProfile
                &&
                <Title
                businessEmail={props.businessEmail}
                keyPost={props.keyPost}
                text={props.businessName} 
                image={IMAGETEMP}
                own={props.own}
                navigation = { navigation }
                />
                }
                <Body 
                text={props.description || "Sin texto"}
                media={ media ? {uri:media} : null }
                // mediaType={props.mediaType || 'imagen'}
                mediaType={props.mediaType}
                date={props.date || ""}
                // PressControlFromMedia={PressControlFromMedia}
                /> 
                <InteractionButtons
                // controlPressedIntermetent={controlPressedIntermetent}
                businessName={props.businessName} 
                description={props.description}
                controls={props.controls || 0}
                onShare={_ => alert("co")}
                onControl={_ => alert("co")}
                />

                
                
               
            </View>
    )
}

export default Component