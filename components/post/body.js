// React
import React , { useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import Media  from '../media/forPost'

// Styles
import { STYLES } from './styles'

import { SIZES } from '../../consts/sizes'


function Component(props){


    return (
            <View
            style={[
                STYLES.containerForBody, 
            ]}
            >
                <Grid
                style={[
                    STYLES.gridForBody, 
                ]}  
                >
                    {/* Image or Video or Nothing */}
                    {props.media && 
                    <Row
                    style={[
                        STYLES.rowForBody, 
                        STYLES.rowForBodyImage, 
                        {height:SIZES.mediaForView.height}
                    ]}  
                    >
                        <Media 
                        forView
                        source={props.media}
                        mediaType={props.mediaType}
                        PressControlFromMedia={props.PressControlFromMedia}
                        />
                    </Row>
                    }

                    {/* Date */}
                    <Row
                    style={[
                        STYLES.rowForBody, 
                        STYLES.rowForBodyDate,
                    ]}  
                    >
                        <Text
                        style={[
                        STYLES.textForDate
                        ]}
                        >
                            {props.date}
                        </Text>
                    </Row>
                    
                    {/* Text */}
                    <View
                    style={[
                        STYLES.rowForBody, 
                        STYLES.rowForBodyText,
                    ]}  
                    >
                        <Text
                        style={[
                        STYLES.textForBody
                        ]}
                        >
                            {props.text}
                        </Text>
                    </View>
{/* 
                    <Row
                    style={[
                        STYLES.rowForBody, 
                    ]}  
                    >
                        
                    </Col>
                    <Col
                    size={1}
                    style={[
                        STYLES.colForTitle, 
                    ]}  
                    >
                        <Icon
                            source={props.image}
                            size={40}
                        />
                    </Col> */}
                </Grid>


               
            </View>
    )
}

export default Component