// React
import React , { useState } from 'react';

// React native
import { Platform, StyleSheet, Text, View, Alert,  } from 'react-native';

// React Native Easy Grid
import { Col, Row, Grid } from "react-native-easy-grid";

// Watopub components
import Icon from '../icon/circle'
import CircleButton from '../button/circle'
import Title from '../text/title'

// Styles
import { STYLES } from './styles'

// Images and resources
const IMAGE_GO_TO_BACK = require('../../assets/icons/navigation/goToBackWhite.png')
const IMAGE_MENU = require('../../assets/icons/posts/menu.png')

function Component(props){

    const { navigation } = props


    const GoToDeletePost = _ => {
        console.log(`1 : ${props.keyPost}`)
        navigation.navigate("DeletePost", { type:'producto' , keyPost:props.keyPost });
    }

    function ProfilePictureBusiness(){
        return (
            <Icon
            source={props.image}
            size={20}
            />
        )
    }

    function MenuToDelete(){
        return (
            <CircleButton
            source={IMAGE_MENU}
            size={20}
            onPress={GoToDeletePost}
            />
        )
    }

    function ButtonGoToBack(){
        return (
            <CircleButton 
            source={IMAGE_GO_TO_BACK}
            size={45}
            styleContainer={STYLES.containerCircleButtonGoToBack} 
            onPress={GoToBack}
            />
        )
    }

    const GoToBack = _ => {
        navigation.goBack();
    }

    return (
            <View
            style={[
                STYLES.containerForTitle, 
                // props.styleContainer,
            ]}
            >
                <Grid
                >
                    <Col
                    size={1}
                    style={[
                    STYLES.colForTitle, 
                    ]}
                    >
                        <ButtonGoToBack />
                        
                    </Col>
                    <Col
                    size={5}
                    style={[
                    STYLES.colForTitle, 
                    ]}  
                    >
                        {/* <Text
                        style={[
                        // STYLES.textForTitle
                        ]}
                        > */}
                        <Title
                        text={props.text}
                        colorInverted
                        />
                            
                        {/* </Text> */}
                    </Col>
                    <Col
                    size={1}
                    style={[
                    STYLES.colForTitle, 
                    ]}  
                    >
                        {
                        props.own
                        ?
                        <MenuToDelete />
                        :
                        <ProfilePictureBusiness />
                        }
                    </Col>
                </Grid>


               
            </View>
    )
}

export default Component