// React
import React , { useEffect , useState } from 'react';

// React native
import { View } from 'react-native';

// Watopub components
import SquareButton from '../button/square'

// Firestore
import { 
    GetImageFromProducts
} from '../../firebase/firestore/posts'

// Styles
import { STYLES } from './styles'

// Consts
import { SIZES } from '../../consts/sizes'



function Component(props){
    
    // Just navigation, separated by code order
    const { navigation } = props

    const [media,setMedia] = useState(null)

    useEffect( _ => {
        props.data.media && GetImageFromProducts(props.data.media)
        .then( r => {
            props.data.media = r
            setMedia(r)
        })
    },[])

    const GoTo = _ => {
        navigation.navigate( "ViewProduct" , { data:props.data });
    }



    return (
            <View
            style={[
                STYLES.containerComponentMiniature, 
            ]} 
            >
                <SquareButton
                sizeIcon={SIZES.miniatureProductInProfileScreen}
                source={{uri:media}}
                // text={props.data.keyPost}
                styleContainer={STYLES.containerButtonMiniature} 
                styleIcon={STYLES.squareButtonMiniature} 
                onPress={GoTo}
                />
            </View>
    )
}

export default Component