// React
import React , {  useState , useEffect } from 'react';

// React native
import { View } from 'react-native';

// Post Watopub components
import Title from './title'
import Body from './body'
import InteractionButtons from '../button/interactionButtons'

// Styles
import { STYLES } from './styles'




function Component(props){

    const {
        navigation,
        own,
        businessName,
        category,
        controls,
        date,
        description,
        id,
        keyPost,
        media,
        mediaType,
        title,
    } = props

    return (
            <View
            style={[
                STYLES.containerComponent, 
                // props.styleContainer,
            ]} 
            >
                <Title 
                keyPost={keyPost}
                text={businessName || "Negocio"}
                image={{uri:media}}
                own={own}
                navigation = { navigation }
                />

                <Body 
                title={props.title}
                description={description}
                media={ media ? {uri:media} : null }
                mediaType={mediaType}
                date={props.date || ""}
                /> 

                <InteractionButtons
                businessName={props.businessName} 
                description={props.description}
                controls={controls || 0}
                onShare={_ => alert("co")}
                onControl={_ => alert("controlaarsh")}
                />
               
            </View>
    )
}

export default Component